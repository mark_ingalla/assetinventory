USE [AssetInventory]
GO
/****** Object:  ForeignKey [ApprovingUser_Country]    Script Date: 02/02/2012 01:19:51 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers] DROP CONSTRAINT [ApprovingUser_Country]
GO
/****** Object:  ForeignKey [ApprovingUser_TransferType]    Script Date: 02/02/2012 01:19:51 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers] DROP CONSTRAINT [ApprovingUser_TransferType]
GO
/****** Object:  ForeignKey [ApprovingUser_TransferReasons_Source]    Script Date: 02/02/2012 01:19:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons] DROP CONSTRAINT [ApprovingUser_TransferReasons_Source]
GO
/****** Object:  ForeignKey [ApprovingUser_TransferReasons_Target]    Script Date: 02/02/2012 01:19:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons] DROP CONSTRAINT [ApprovingUser_TransferReasons_Target]
GO
/****** Object:  ForeignKey [Asset_Category]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Category]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_Category]
GO
/****** Object:  ForeignKey [Asset_ConditionEvaluation]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ConditionEvaluation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_ConditionEvaluation]
GO
/****** Object:  ForeignKey [Asset_Location]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_Location]
GO
/****** Object:  ForeignKey [Asset_Manufacturer]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Manufacturer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_Manufacturer]
GO
/****** Object:  ForeignKey [Asset_ModelType]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ModelType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_ModelType]
GO
/****** Object:  ForeignKey [Asset_PurchaseInfo]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PurchaseInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_PurchaseInfo]
GO
/****** Object:  ForeignKey [Asset_ShippingInfo]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ShippingInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_ShippingInfo]
GO
/****** Object:  ForeignKey [Asset_SubCategory]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_SubCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_SubCategory]
GO
/****** Object:  ForeignKey [Caution_MaterialTransfers_Source]    Script Date: 02/02/2012 01:19:58 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers] DROP CONSTRAINT [Caution_MaterialTransfers_Source]
GO
/****** Object:  ForeignKey [Caution_MaterialTransfers_Target]    Script Date: 02/02/2012 01:19:58 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers] DROP CONSTRAINT [Caution_MaterialTransfers_Target]
GO
/****** Object:  ForeignKey [ChangeRequest_Attachments]    Script Date: 02/02/2012 01:20:02 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Attachments]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]'))
ALTER TABLE [dbo].[ChangeRequestAttachments] DROP CONSTRAINT [ChangeRequest_Attachments]
GO
/****** Object:  ForeignKey [ChangeRequest_Admin]    Script Date: 02/02/2012 01:20:03 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Admin]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] DROP CONSTRAINT [ChangeRequest_Admin]
GO
/****** Object:  ForeignKey [ChangeRequest_Requestor]    Script Date: 02/02/2012 01:20:03 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Requestor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] DROP CONSTRAINT [ChangeRequest_Requestor]
GO
/****** Object:  ForeignKey [CountryManager_MaterialTransfer]    Script Date: 02/02/2012 01:20:08 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_MaterialTransfer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers] DROP CONSTRAINT [CountryManager_MaterialTransfer]
GO
/****** Object:  ForeignKey [CountryManager_User]    Script Date: 02/02/2012 01:20:08 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers] DROP CONSTRAINT [CountryManager_User]
GO
/****** Object:  ForeignKey [Document_Asset]    Script Date: 02/02/2012 01:20:11 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_Asset]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [Document_Asset]
GO
/****** Object:  ForeignKey [Document_DocumentType]    Script Date: 02/02/2012 01:20:11 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [Document_DocumentType]
GO
/****** Object:  ForeignKey [Location_Country]    Script Date: 02/02/2012 01:20:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Location_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[Locations]'))
ALTER TABLE [dbo].[Locations] DROP CONSTRAINT [Location_Country]
GO
/****** Object:  ForeignKey [ApprovingUser_MaterialTransferApprovals]    Script Date: 02/02/2012 01:20:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_MaterialTransferApprovals]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaterialTransferApprovals]'))
ALTER TABLE [dbo].[MaterialTransferApprovals] DROP CONSTRAINT [ApprovingUser_MaterialTransferApprovals]
GO
/****** Object:  ForeignKey [Asset_PhotoArchives]    Script Date: 02/02/2012 01:20:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PhotoArchives]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhotoArchives]'))
ALTER TABLE [dbo].[PhotoArchives] DROP CONSTRAINT [Asset_PhotoArchives]
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 02/02/2012 01:20:11 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_Asset]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [Document_Asset]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [Document_DocumentType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Documents]') AND type in (N'U'))
DROP TABLE [dbo].[Documents]
GO
/****** Object:  Table [dbo].[PhotoArchives]    Script Date: 02/02/2012 01:20:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PhotoArchives]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhotoArchives]'))
ALTER TABLE [dbo].[PhotoArchives] DROP CONSTRAINT [Asset_PhotoArchives]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PhotoArchives]') AND type in (N'U'))
DROP TABLE [dbo].[PhotoArchives]
GO
/****** Object:  Table [dbo].[MaterialTransferApprovals]    Script Date: 02/02/2012 01:20:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_MaterialTransferApprovals]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaterialTransferApprovals]'))
ALTER TABLE [dbo].[MaterialTransferApprovals] DROP CONSTRAINT [ApprovingUser_MaterialTransferApprovals]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaterialTransferApprovals]') AND type in (N'U'))
DROP TABLE [dbo].[MaterialTransferApprovals]
GO
/****** Object:  Table [dbo].[ApprovingUserTransferReasons]    Script Date: 02/02/2012 01:19:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons] DROP CONSTRAINT [ApprovingUser_TransferReasons_Source]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons] DROP CONSTRAINT [ApprovingUser_TransferReasons_Target]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]') AND type in (N'U'))
DROP TABLE [dbo].[ApprovingUserTransferReasons]
GO
/****** Object:  Table [dbo].[Assets]    Script Date: 02/02/2012 01:19:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Category]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_Category]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ConditionEvaluation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_ConditionEvaluation]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_Location]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Manufacturer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_Manufacturer]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ModelType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_ModelType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PurchaseInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_PurchaseInfo]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ShippingInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_ShippingInfo]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_SubCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] DROP CONSTRAINT [Asset_SubCategory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assets]') AND type in (N'U'))
DROP TABLE [dbo].[Assets]
GO
/****** Object:  Table [dbo].[ChangeRequestAttachments]    Script Date: 02/02/2012 01:20:02 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Attachments]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]'))
ALTER TABLE [dbo].[ChangeRequestAttachments] DROP CONSTRAINT [ChangeRequest_Attachments]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]') AND type in (N'U'))
DROP TABLE [dbo].[ChangeRequestAttachments]
GO
/****** Object:  Table [dbo].[ChangeRequests]    Script Date: 02/02/2012 01:20:03 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Admin]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] DROP CONSTRAINT [ChangeRequest_Admin]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Requestor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] DROP CONSTRAINT [ChangeRequest_Requestor]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequests]') AND type in (N'U'))
DROP TABLE [dbo].[ChangeRequests]
GO
/****** Object:  Table [dbo].[ApprovingUsers]    Script Date: 02/02/2012 01:19:51 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers] DROP CONSTRAINT [ApprovingUser_Country]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers] DROP CONSTRAINT [ApprovingUser_TransferType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]') AND type in (N'U'))
DROP TABLE [dbo].[ApprovingUsers]
GO
/****** Object:  Table [dbo].[CautionMaterialTransfers]    Script Date: 02/02/2012 01:19:58 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers] DROP CONSTRAINT [Caution_MaterialTransfers_Source]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers] DROP CONSTRAINT [Caution_MaterialTransfers_Target]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]') AND type in (N'U'))
DROP TABLE [dbo].[CautionMaterialTransfers]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 02/02/2012 01:20:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Location_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[Locations]'))
ALTER TABLE [dbo].[Locations] DROP CONSTRAINT [Location_Country]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
DROP TABLE [dbo].[Locations]
GO
/****** Object:  Table [dbo].[CountryManagers]    Script Date: 02/02/2012 01:20:08 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_MaterialTransfer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers] DROP CONSTRAINT [CountryManager_MaterialTransfer]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers] DROP CONSTRAINT [CountryManager_User]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CountryManagers]') AND type in (N'U'))
DROP TABLE [dbo].[CountryManagers]
GO
/****** Object:  Table [dbo].[DimensionUOMs]    Script Date: 02/02/2012 01:20:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimensionUOMs]') AND type in (N'U'))
DROP TABLE [dbo].[DimensionUOMs]
GO
/****** Object:  Table [dbo].[PurchaseCurrencies]    Script Date: 02/02/2012 01:20:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurchaseCurrencies]') AND type in (N'U'))
DROP TABLE [dbo].[PurchaseCurrencies]
GO
/****** Object:  Table [dbo].[PurchaseInfoes]    Script Date: 02/02/2012 01:20:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurchaseInfoes]') AND type in (N'U'))
DROP TABLE [dbo].[PurchaseInfoes]
GO
/****** Object:  Table [dbo].[ShippingInfoes]    Script Date: 02/02/2012 01:20:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShippingInfoes]') AND type in (N'U'))
DROP TABLE [dbo].[ShippingInfoes]
GO
/****** Object:  Table [dbo].[SubCategories]    Script Date: 02/02/2012 01:20:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SubCategories]') AND type in (N'U'))
DROP TABLE [dbo].[SubCategories]
GO
/****** Object:  Table [dbo].[TransferReasons]    Script Date: 02/02/2012 01:20:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransferReasons]') AND type in (N'U'))
DROP TABLE [dbo].[TransferReasons]
GO
/****** Object:  Table [dbo].[TransferTypes]    Script Date: 02/02/2012 01:20:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransferTypes]') AND type in (N'U'))
DROP TABLE [dbo].[TransferTypes]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 02/02/2012 01:20:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
DROP TABLE [dbo].[Users]
GO
/****** Object:  Table [dbo].[WeightUOMs]    Script Date: 02/02/2012 01:20:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WeightUOMs]') AND type in (N'U'))
DROP TABLE [dbo].[WeightUOMs]
GO
/****** Object:  Table [dbo].[Manufacturers]    Script Date: 02/02/2012 01:20:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Manufacturers]') AND type in (N'U'))
DROP TABLE [dbo].[Manufacturers]
GO
/****** Object:  Table [dbo].[DocumentTypes]    Script Date: 02/02/2012 01:20:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentTypes]') AND type in (N'U'))
DROP TABLE [dbo].[DocumentTypes]
GO
/****** Object:  Table [dbo].[EdmMetadata]    Script Date: 02/02/2012 01:20:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EdmMetadata]') AND type in (N'U'))
DROP TABLE [dbo].[EdmMetadata]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 02/02/2012 01:20:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Languages]') AND type in (N'U'))
DROP TABLE [dbo].[Languages]
GO
/****** Object:  Table [dbo].[MaterialTransferAssets]    Script Date: 02/02/2012 01:20:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaterialTransferAssets]') AND type in (N'U'))
DROP TABLE [dbo].[MaterialTransferAssets]
GO
/****** Object:  Table [dbo].[MaterialTransfers]    Script Date: 02/02/2012 01:20:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaterialTransfers]') AND type in (N'U'))
DROP TABLE [dbo].[MaterialTransfers]
GO
/****** Object:  Table [dbo].[ModelTypes]    Script Date: 02/02/2012 01:20:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModelTypes]') AND type in (N'U'))
DROP TABLE [dbo].[ModelTypes]
GO
/****** Object:  Table [dbo].[NotificationUsers]    Script Date: 02/02/2012 01:20:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationUsers]') AND type in (N'U'))
DROP TABLE [dbo].[NotificationUsers]
GO
/****** Object:  Table [dbo].[NotificationUserTransferReasons]    Script Date: 02/02/2012 01:20:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationUserTransferReasons]') AND type in (N'U'))
DROP TABLE [dbo].[NotificationUserTransferReasons]
GO
/****** Object:  Table [dbo].[Cautions]    Script Date: 02/02/2012 01:20:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cautions]') AND type in (N'U'))
DROP TABLE [dbo].[Cautions]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 02/02/2012 01:19:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Categories]') AND type in (N'U'))
DROP TABLE [dbo].[Categories]
GO
/****** Object:  Table [dbo].[ConditionEvaluations]    Script Date: 02/02/2012 01:20:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConditionEvaluations]') AND type in (N'U'))
DROP TABLE [dbo].[ConditionEvaluations]
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 02/02/2012 01:20:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Countries]') AND type in (N'U'))
DROP TABLE [dbo].[Countries]
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 02/02/2012 01:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Countries]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Countries](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](max) NOT NULL,
	[CountryCode] [nvarchar](max) NOT NULL,
	[ISOCode] [nvarchar](max) NULL,
	[SortCode] [int] NOT NULL,
	[IsShown] [bit] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ConditionEvaluations]    Script Date: 02/02/2012 01:20:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConditionEvaluations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ConditionEvaluations](
	[ConditionEvaluationId] [int] IDENTITY(1,1) NOT NULL,
	[ConditionEvalDescription] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConditionEvaluationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_ConditionEvalDescription] UNIQUE NONCLUSTERED 
(
	[ConditionEvalDescription] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 02/02/2012 01:19:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Categories]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Categories](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](max) NOT NULL,
	[Prefix] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_CategoryPrefix] UNIQUE NONCLUSTERED 
(
	[Prefix] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Cautions]    Script Date: 02/02/2012 01:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cautions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Cautions](
	[CautionId] [int] IDENTITY(1,1) NOT NULL,
	[CautionName] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CautionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_CautionName] UNIQUE NONCLUSTERED 
(
	[CautionName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[NotificationUserTransferReasons]    Script Date: 02/02/2012 01:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationUserTransferReasons]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NotificationUserTransferReasons](
	[NotificationUser_NotificationUserId] [int] NOT NULL,
	[TransferReason_TransferReasonId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NotificationUser_NotificationUserId] ASC,
	[TransferReason_TransferReasonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[NotificationUsers]    Script Date: 02/02/2012 01:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NotificationUsers](
	[NotificationUserId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TransferTypeId] [int] NOT NULL,
	[CountryId] [int] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[NotificationUserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ModelTypes]    Script Date: 02/02/2012 01:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModelTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ModelTypes](
	[ModelTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ModelTypeName] [nvarchar](max) NOT NULL,
	[ModelTypeDescription] [nvarchar](max) NOT NULL,
	[ManufacturerId] [int] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ModelTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MaterialTransfers]    Script Date: 02/02/2012 01:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaterialTransfers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaterialTransfers](
	[MaterialTransferId] [int] IDENTITY(1,1) NOT NULL,
	[TransferTypeId] [int] NOT NULL,
	[TransferReasonId] [int] NOT NULL,
	[MaterialTransferNumber] [nvarchar](450) NULL,
	[ManifestNumber] [nvarchar](max) NULL,
	[FromCountryId] [int] NOT NULL,
	[CurrentLocationId] [int] NOT NULL,
	[AFENumber] [nvarchar](max) NULL,
	[Transporter] [nvarchar](max) NULL,
	[ToCountryId] [int] NOT NULL,
	[NewLocationId] [int] NOT NULL,
	[RepairWorkOrder] [nvarchar](max) NULL,
	[TruckNumber] [nvarchar](max) NULL,
	[AttnReceiver] [nvarchar](max) NULL,
	[ReceiverId] [int] NOT NULL,
	[ReceiverRef] [nvarchar](max) NULL,
	[ShipToAddress] [nvarchar](max) NULL,
	[TransferComment] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL,
	[OriginatorId] [int] NOT NULL,
	[ManifestoInfo] [nvarchar](max) NULL,
	[ApprovalStatus] [nvarchar](max) NULL,
	[ApprovalDate] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
	[User_UserId] [int] NULL,
	[User_UserId1] [int] NULL,
	[FromCountry_CountryId] [int] NULL,
	[ToCountry_CountryId] [int] NULL,
	[Receiver_UserId] [int] NULL,
	[Originator_UserId] [int] NULL,
	[CurrentLocation_LocationId] [int] NULL,
	[NewLocation_LocationId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaterialTransferId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_MaterialTransferNumber] UNIQUE NONCLUSTERED 
(
	[MaterialTransferNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MaterialTransferAssets]    Script Date: 02/02/2012 01:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaterialTransferAssets]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaterialTransferAssets](
	[MaterialTransferAssetId] [int] IDENTITY(1,1) NOT NULL,
	[MaterialTransferId] [int] NOT NULL,
	[AssetId] [int] NULL,
	[NonAssetDescription] [nvarchar](max) NULL,
	[NonAssetQuantity] [int] NOT NULL,
	[IsShipped] [bit] NOT NULL,
	[ETADays] [int] NOT NULL,
	[ConditionEvaluationId] [int] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaterialTransferAssetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 02/02/2012 01:20:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Languages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Languages](
	[LanguageId] [int] IDENTITY(1,1) NOT NULL,
	[LanguageName] [nvarchar](max) NOT NULL,
	[LanguageShortName] [nvarchar](max) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EdmMetadata]    Script Date: 02/02/2012 01:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EdmMetadata]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EdmMetadata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModelHash] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DocumentTypes]    Script Date: 02/02/2012 01:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DocumentTypes](
	[DocumentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentTypeName] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DocumentTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_DocumentTypeName] UNIQUE NONCLUSTERED 
(
	[DocumentTypeName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Manufacturers]    Script Date: 02/02/2012 01:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Manufacturers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Manufacturers](
	[ManufacturerId] [int] IDENTITY(1,1) NOT NULL,
	[ManufacturerName] [nvarchar](max) NOT NULL,
	[ManufacturerCode] [nvarchar](3) NOT NULL,
	[OtherManufacturerRef] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ManufacturerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_ManufacturerCode] UNIQUE NONCLUSTERED 
(
	[ManufacturerCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[WeightUOMs]    Script Date: 02/02/2012 01:20:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WeightUOMs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WeightUOMs](
	[WeightUOMId] [int] IDENTITY(1,1) NOT NULL,
	[WeightUOMDescription] [nvarchar](max) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[WeightUOMId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Users]    Script Date: 02/02/2012 01:20:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](450) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](450) NOT NULL,
	[UserTypeId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Position] [nvarchar](max) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_UserEmail] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_UserUserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TransferTypes]    Script Date: 02/02/2012 01:20:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransferTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransferTypes](
	[TransferTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TransferTypeName] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TransferTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_TransferTypeName] UNIQUE NONCLUSTERED 
(
	[TransferTypeName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TransferReasons]    Script Date: 02/02/2012 01:20:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransferReasons]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransferReasons](
	[TransferReasonId] [int] IDENTITY(1,1) NOT NULL,
	[TransferReasonName] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TransferReasonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_TransferReasonName] UNIQUE NONCLUSTERED 
(
	[TransferReasonName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SubCategories]    Script Date: 02/02/2012 01:20:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SubCategories]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SubCategories](
	[SubCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[SubCategoryName] [nvarchar](max) NOT NULL,
	[MarkinginInstructions] [nvarchar](max) NULL,
	[CertOrDocRequirement] [nvarchar](max) NULL,
	[DescriptionTemplate] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SubCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ShippingInfoes]    Script Date: 02/02/2012 01:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShippingInfoes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ShippingInfoes](
	[ShippingInfoId] [int] IDENTITY(1,1) NOT NULL,
	[OperatingWeight] [float] NULL,
	[WeightUOMId] [int] NULL,
	[DryWeight] [float] NULL,
	[DryWeightUOMId] [int] NULL,
	[Height] [float] NULL,
	[HeightUOMId] [int] NULL,
	[Width] [float] NULL,
	[WidthUOMId] [int] NULL,
	[Diameter] [float] NULL,
	[DiameterUOMId] [int] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
	[OperatingWeightUOM_WeightUOMId] [int] NULL,
	[DryWeightUOM_WeightUOMId] [int] NULL,
	[HeightUOM_DimensionUOMId] [int] NULL,
	[WidthUOM_DimensionUOMId] [int] NULL,
	[DiameterUOM_DimensionUOMId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ShippingInfoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PurchaseInfoes]    Script Date: 02/02/2012 01:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurchaseInfoes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurchaseInfoes](
	[PurchaseInfoId] [int] IDENTITY(1,1) NOT NULL,
	[AFENumber] [nvarchar](max) NULL,
	[MaterialReqNumber] [nvarchar](max) NULL,
	[PONumber] [nvarchar](max) NULL,
	[Vendor] [nvarchar](max) NULL,
	[InvoiceNumber] [nvarchar](max) NULL,
	[PurchasePrice] [decimal](18, 2) NULL,
	[PurchaseCurrencyId] [int] NULL,
	[PurchaseDate] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PurchaseInfoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PurchaseCurrencies]    Script Date: 02/02/2012 01:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurchaseCurrencies]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurchaseCurrencies](
	[PurchaseCurrencyID] [int] IDENTITY(1,1) NOT NULL,
	[PurhcaseCurrencySymbol] [nvarchar](max) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PurchaseCurrencyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DimensionUOMs]    Script Date: 02/02/2012 01:20:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimensionUOMs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DimensionUOMs](
	[DimensionUOMId] [int] IDENTITY(1,1) NOT NULL,
	[DimesionDescription] [nvarchar](max) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DimensionUOMId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CountryManagers]    Script Date: 02/02/2012 01:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CountryManagers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CountryManagers](
	[CountryManagerId] [int] IDENTITY(1,1) NOT NULL,
	[MaterialTransferId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryManagerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 02/02/2012 01:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Locations](
	[LocationId] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [nvarchar](max) NOT NULL,
	[CountryId] [int] NOT NULL,
	[CostCenterCode] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_LocationCostCenterCode] UNIQUE NONCLUSTERED 
(
	[CostCenterCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CautionMaterialTransfers]    Script Date: 02/02/2012 01:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CautionMaterialTransfers](
	[Caution_CautionId] [int] NOT NULL,
	[MaterialTransfer_MaterialTransferId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Caution_CautionId] ASC,
	[MaterialTransfer_MaterialTransferId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ApprovingUsers]    Script Date: 02/02/2012 01:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ApprovingUsers](
	[ApprovingUserId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TransferTypeId] [int] NOT NULL,
	[CountryId] [int] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ApprovingUserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ChangeRequests]    Script Date: 02/02/2012 01:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequests]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChangeRequests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChangeType] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NULL,
	[AdminId] [int] NOT NULL,
	[DateRequested] [datetime] NULL,
	[RequestorId] [int] NOT NULL,
	[Module] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Admin_UserId] [int] NULL,
	[Requestor_UserId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ChangeRequestAttachments]    Script Date: 02/02/2012 01:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChangeRequestAttachments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ChangeRequest_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Assets]    Script Date: 02/02/2012 01:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assets]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Assets](
	[AssetId] [int] IDENTITY(1,1) NOT NULL,
	[AssetUniqueId] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryId] [int] NOT NULL,
	[SubCategoryId] [int] NOT NULL,
	[ManufacturerId] [int] NOT NULL,
	[ModelTypeID] [int] NOT NULL,
	[SerialNumber] [nvarchar](max) NULL,
	[ArrangementNumber] [nvarchar](max) NULL,
	[LocationID] [int] NOT NULL,
	[PurchaseInfoID] [int] NULL,
	[MaintenanceRequestID] [int] NOT NULL,
	[ShippingInfoID] [int] NULL,
	[MaterialTransferNumber] [nvarchar](max) NULL,
	[ConditionEvaluationID] [int] NOT NULL,
	[AssetPurchasePrice] [decimal](18, 2) NULL,
	[NetbookValue] [decimal](18, 2) NULL,
	[FairMarketPrice] [decimal](18, 2) NULL,
	[Comment] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL,
	[IsPart] [bit] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AssetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ApprovingUserTransferReasons]    Script Date: 02/02/2012 01:19:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ApprovingUserTransferReasons](
	[ApprovingUser_ApprovingUserId] [int] NOT NULL,
	[TransferReason_TransferReasonId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ApprovingUser_ApprovingUserId] ASC,
	[TransferReason_TransferReasonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MaterialTransferApprovals]    Script Date: 02/02/2012 01:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaterialTransferApprovals]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaterialTransferApprovals](
	[MaterialTransferApprovalId] [int] IDENTITY(1,1) NOT NULL,
	[MaterialTransferId] [int] NOT NULL,
	[ApprovingUserId] [int] NOT NULL,
	[ApprovalStatus] [nvarchar](max) NULL,
	[ApprovalDateTime] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
	[ApprovingUser_ApprovingUserId] [int] NULL,
	[MaterialTransfer_MaterialTransferId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaterialTransferApprovalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PhotoArchives]    Script Date: 02/02/2012 01:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PhotoArchives]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PhotoArchives](
	[PhotoArchiveId] [int] IDENTITY(1,1) NOT NULL,
	[DateUploaded] [datetime] NOT NULL,
	[PhotoFileName] [nvarchar](max) NULL,
	[AssetId] [int] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PhotoArchiveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 02/02/2012 01:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Documents]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Documents](
	[DocumentId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentName] [nvarchar](max) NULL,
	[DocumentDescription] [nvarchar](max) NULL,
	[DocumentTypeId] [int] NULL,
	[AssetId] [int] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  ForeignKey [ApprovingUser_Country]    Script Date: 02/02/2012 01:19:51 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers]  WITH CHECK ADD  CONSTRAINT [ApprovingUser_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Countries] ([CountryId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers] CHECK CONSTRAINT [ApprovingUser_Country]
GO
/****** Object:  ForeignKey [ApprovingUser_TransferType]    Script Date: 02/02/2012 01:19:51 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers]  WITH CHECK ADD  CONSTRAINT [ApprovingUser_TransferType] FOREIGN KEY([TransferTypeId])
REFERENCES [dbo].[TransferTypes] ([TransferTypeId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUsers]'))
ALTER TABLE [dbo].[ApprovingUsers] CHECK CONSTRAINT [ApprovingUser_TransferType]
GO
/****** Object:  ForeignKey [ApprovingUser_TransferReasons_Source]    Script Date: 02/02/2012 01:19:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons]  WITH CHECK ADD  CONSTRAINT [ApprovingUser_TransferReasons_Source] FOREIGN KEY([ApprovingUser_ApprovingUserId])
REFERENCES [dbo].[ApprovingUsers] ([ApprovingUserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons] CHECK CONSTRAINT [ApprovingUser_TransferReasons_Source]
GO
/****** Object:  ForeignKey [ApprovingUser_TransferReasons_Target]    Script Date: 02/02/2012 01:19:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons]  WITH CHECK ADD  CONSTRAINT [ApprovingUser_TransferReasons_Target] FOREIGN KEY([TransferReason_TransferReasonId])
REFERENCES [dbo].[TransferReasons] ([TransferReasonId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_TransferReasons_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[ApprovingUserTransferReasons]'))
ALTER TABLE [dbo].[ApprovingUserTransferReasons] CHECK CONSTRAINT [ApprovingUser_TransferReasons_Target]
GO
/****** Object:  ForeignKey [Asset_Category]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Category]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Category]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_Category]
GO
/****** Object:  ForeignKey [Asset_ConditionEvaluation]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ConditionEvaluation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_ConditionEvaluation] FOREIGN KEY([ConditionEvaluationID])
REFERENCES [dbo].[ConditionEvaluations] ([ConditionEvaluationId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ConditionEvaluation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_ConditionEvaluation]
GO
/****** Object:  ForeignKey [Asset_Location]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_Location]
GO
/****** Object:  ForeignKey [Asset_Manufacturer]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Manufacturer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_Manufacturer] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[Manufacturers] ([ManufacturerId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_Manufacturer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_Manufacturer]
GO
/****** Object:  ForeignKey [Asset_ModelType]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ModelType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_ModelType] FOREIGN KEY([ModelTypeID])
REFERENCES [dbo].[ModelTypes] ([ModelTypeId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ModelType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_ModelType]
GO
/****** Object:  ForeignKey [Asset_PurchaseInfo]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PurchaseInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_PurchaseInfo] FOREIGN KEY([PurchaseInfoID])
REFERENCES [dbo].[PurchaseInfoes] ([PurchaseInfoId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PurchaseInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_PurchaseInfo]
GO
/****** Object:  ForeignKey [Asset_ShippingInfo]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ShippingInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_ShippingInfo] FOREIGN KEY([ShippingInfoID])
REFERENCES [dbo].[ShippingInfoes] ([ShippingInfoId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_ShippingInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_ShippingInfo]
GO
/****** Object:  ForeignKey [Asset_SubCategory]    Script Date: 02/02/2012 01:19:55 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_SubCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_SubCategory] FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[SubCategories] ([SubCategoryId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_SubCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assets]'))
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_SubCategory]
GO
/****** Object:  ForeignKey [Caution_MaterialTransfers_Source]    Script Date: 02/02/2012 01:19:58 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers]  WITH CHECK ADD  CONSTRAINT [Caution_MaterialTransfers_Source] FOREIGN KEY([Caution_CautionId])
REFERENCES [dbo].[Cautions] ([CautionId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Source]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers] CHECK CONSTRAINT [Caution_MaterialTransfers_Source]
GO
/****** Object:  ForeignKey [Caution_MaterialTransfers_Target]    Script Date: 02/02/2012 01:19:58 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers]  WITH CHECK ADD  CONSTRAINT [Caution_MaterialTransfers_Target] FOREIGN KEY([MaterialTransfer_MaterialTransferId])
REFERENCES [dbo].[MaterialTransfers] ([MaterialTransferId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Caution_MaterialTransfers_Target]') AND parent_object_id = OBJECT_ID(N'[dbo].[CautionMaterialTransfers]'))
ALTER TABLE [dbo].[CautionMaterialTransfers] CHECK CONSTRAINT [Caution_MaterialTransfers_Target]
GO
/****** Object:  ForeignKey [ChangeRequest_Attachments]    Script Date: 02/02/2012 01:20:02 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Attachments]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]'))
ALTER TABLE [dbo].[ChangeRequestAttachments]  WITH CHECK ADD  CONSTRAINT [ChangeRequest_Attachments] FOREIGN KEY([ChangeRequest_Id])
REFERENCES [dbo].[ChangeRequests] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Attachments]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]'))
ALTER TABLE [dbo].[ChangeRequestAttachments] CHECK CONSTRAINT [ChangeRequest_Attachments]
GO
/****** Object:  ForeignKey [ChangeRequest_Admin]    Script Date: 02/02/2012 01:20:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Admin]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests]  WITH CHECK ADD  CONSTRAINT [ChangeRequest_Admin] FOREIGN KEY([Admin_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Admin]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] CHECK CONSTRAINT [ChangeRequest_Admin]
GO
/****** Object:  ForeignKey [ChangeRequest_Requestor]    Script Date: 02/02/2012 01:20:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Requestor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests]  WITH CHECK ADD  CONSTRAINT [ChangeRequest_Requestor] FOREIGN KEY([Requestor_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Requestor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] CHECK CONSTRAINT [ChangeRequest_Requestor]
GO
/****** Object:  ForeignKey [CountryManager_MaterialTransfer]    Script Date: 02/02/2012 01:20:08 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_MaterialTransfer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers]  WITH CHECK ADD  CONSTRAINT [CountryManager_MaterialTransfer] FOREIGN KEY([MaterialTransferId])
REFERENCES [dbo].[MaterialTransfers] ([MaterialTransferId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_MaterialTransfer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers] CHECK CONSTRAINT [CountryManager_MaterialTransfer]
GO
/****** Object:  ForeignKey [CountryManager_User]    Script Date: 02/02/2012 01:20:08 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers]  WITH CHECK ADD  CONSTRAINT [CountryManager_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CountryManager_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[CountryManagers]'))
ALTER TABLE [dbo].[CountryManagers] CHECK CONSTRAINT [CountryManager_User]
GO
/****** Object:  ForeignKey [Document_Asset]    Script Date: 02/02/2012 01:20:11 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_Asset]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [Document_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Assets] ([AssetId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_Asset]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [Document_Asset]
GO
/****** Object:  ForeignKey [Document_DocumentType]    Script Date: 02/02/2012 01:20:11 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [Document_DocumentType] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[DocumentTypes] ([DocumentTypeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Document_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Documents]'))
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [Document_DocumentType]
GO
/****** Object:  ForeignKey [Location_Country]    Script Date: 02/02/2012 01:20:16 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Location_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[Locations]'))
ALTER TABLE [dbo].[Locations]  WITH CHECK ADD  CONSTRAINT [Location_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Countries] ([CountryId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Location_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[Locations]'))
ALTER TABLE [dbo].[Locations] CHECK CONSTRAINT [Location_Country]
GO
/****** Object:  ForeignKey [ApprovingUser_MaterialTransferApprovals]    Script Date: 02/02/2012 01:20:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_MaterialTransferApprovals]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaterialTransferApprovals]'))
ALTER TABLE [dbo].[MaterialTransferApprovals]  WITH CHECK ADD  CONSTRAINT [ApprovingUser_MaterialTransferApprovals] FOREIGN KEY([ApprovingUser_ApprovingUserId])
REFERENCES [dbo].[ApprovingUsers] ([ApprovingUserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ApprovingUser_MaterialTransferApprovals]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaterialTransferApprovals]'))
ALTER TABLE [dbo].[MaterialTransferApprovals] CHECK CONSTRAINT [ApprovingUser_MaterialTransferApprovals]
GO
/****** Object:  ForeignKey [Asset_PhotoArchives]    Script Date: 02/02/2012 01:20:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PhotoArchives]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhotoArchives]'))
ALTER TABLE [dbo].[PhotoArchives]  WITH CHECK ADD  CONSTRAINT [Asset_PhotoArchives] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Assets] ([AssetId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Asset_PhotoArchives]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhotoArchives]'))
ALTER TABLE [dbo].[PhotoArchives] CHECK CONSTRAINT [Asset_PhotoArchives]
GO
