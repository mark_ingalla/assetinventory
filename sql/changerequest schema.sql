USE [AssetInventory]
GO
/****** Object:  Table [dbo].[ChangeRequestAssets]    Script Date: 01/12/2012 01:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChangeRequestAssets](
	[AssetId] [int] IDENTITY(1,1) NOT NULL,
	[AssetUniqueId] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryId] [int] NULL,
	[SubCategoryId] [int] NULL,
	[ManufacturerId] [int] NULL,
	[ModelTypeID] [int] NULL,
	[SerialNumber] [nvarchar](max) NULL,
	[ArrangementNumber] [nvarchar](max) NULL,
	[LocationID] [int] NULL,
	[PurchaseInfoID] [int] NULL,
	[MaintenanceRequestID] [int] NOT NULL,
	[ShippingInfoID] [int] NULL,
	[MaterialTransferNumber] [nvarchar](max) NULL,
	[ConditionEvaluationID] [int] NULL,
	[AssetPurchasePrice] [decimal](18, 2) NULL,
	[NetbookValue] [decimal](18, 2) NULL,
	[FairMarketPrice] [decimal](18, 2) NULL,
	[Comment] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL,
	[IsPart] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AssetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ChangeRequests]    Script Date: 01/12/2012 01:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequests]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChangeRequests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChangeType] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NULL,
	[AdminId] [int] NOT NULL,
	[DateRequested] [datetime] NULL,
	[RequestorId] [int] NOT NULL,
	[Module] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[ChangedAssetId] [int] NULL,
	[Admin_UserId] [int] NULL,
	[Requestor_UserId] [int] NULL,
	[ChangedAsset_AssetId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ChangeRequestAttachments]    Script Date: 01/12/2012 01:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChangeRequestAttachments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ChangeRequest_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  ForeignKey [ChangeRequestAsset_Category]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_Category]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_Category]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_Category]
GO
/****** Object:  ForeignKey [ChangeRequestAsset_ConditionEvaluation]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_ConditionEvaluation]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_ConditionEvaluation] FOREIGN KEY([ConditionEvaluationID])
REFERENCES [dbo].[ConditionEvaluations] ([ConditionEvaluationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_ConditionEvaluation]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_ConditionEvaluation]
GO
/****** Object:  ForeignKey [ChangeRequestAsset_Location]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_Location]
GO
/****** Object:  ForeignKey [ChangeRequestAsset_Manufacturer]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_Manufacturer]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_Manufacturer] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[Manufacturers] ([ManufacturerId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_Manufacturer]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_Manufacturer]
GO
/****** Object:  ForeignKey [ChangeRequestAsset_ModelType]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_ModelType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_ModelType] FOREIGN KEY([ModelTypeID])
REFERENCES [dbo].[ModelTypes] ([ModelTypeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_ModelType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_ModelType]
GO
/****** Object:  ForeignKey [ChangeRequestAsset_PurchaseInfo]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_PurchaseInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_PurchaseInfo] FOREIGN KEY([PurchaseInfoID])
REFERENCES [dbo].[PurchaseInfoes] ([PurchaseInfoId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_PurchaseInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_PurchaseInfo]
GO
/****** Object:  ForeignKey [ChangeRequestAsset_ShippingInfo]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_ShippingInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_ShippingInfo] FOREIGN KEY([ShippingInfoID])
REFERENCES [dbo].[ShippingInfoes] ([ShippingInfoId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_ShippingInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_ShippingInfo]
GO
/****** Object:  ForeignKey [ChangeRequestAsset_SubCategory]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_SubCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets]  WITH CHECK ADD  CONSTRAINT [ChangeRequestAsset_SubCategory] FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[SubCategories] ([SubCategoryId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequestAsset_SubCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAssets]'))
ALTER TABLE [dbo].[ChangeRequestAssets] CHECK CONSTRAINT [ChangeRequestAsset_SubCategory]
GO
/****** Object:  ForeignKey [ChangeRequest_Attachments]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Attachments]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]'))
ALTER TABLE [dbo].[ChangeRequestAttachments]  WITH CHECK ADD  CONSTRAINT [ChangeRequest_Attachments] FOREIGN KEY([ChangeRequest_Id])
REFERENCES [dbo].[ChangeRequests] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Attachments]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequestAttachments]'))
ALTER TABLE [dbo].[ChangeRequestAttachments] CHECK CONSTRAINT [ChangeRequest_Attachments]
GO
/****** Object:  ForeignKey [ChangeRequest_Admin]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Admin]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests]  WITH CHECK ADD  CONSTRAINT [ChangeRequest_Admin] FOREIGN KEY([Admin_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Admin]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] CHECK CONSTRAINT [ChangeRequest_Admin]
GO
/****** Object:  ForeignKey [ChangeRequest_ChangedAsset]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_ChangedAsset]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests]  WITH CHECK ADD  CONSTRAINT [ChangeRequest_ChangedAsset] FOREIGN KEY([ChangedAsset_AssetId])
REFERENCES [dbo].[ChangeRequestAssets] ([AssetId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_ChangedAsset]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] CHECK CONSTRAINT [ChangeRequest_ChangedAsset]
GO
/****** Object:  ForeignKey [ChangeRequest_Requestor]    Script Date: 01/12/2012 01:17:06 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Requestor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests]  WITH CHECK ADD  CONSTRAINT [ChangeRequest_Requestor] FOREIGN KEY([Requestor_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ChangeRequest_Requestor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChangeRequests]'))
ALTER TABLE [dbo].[ChangeRequests] CHECK CONSTRAINT [ChangeRequest_Requestor]
GO
