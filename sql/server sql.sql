USE [AssetInventory]
GO
/****** Object:  Table [dbo].[DimensionUOMs]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DimensionUOMs](
	[DimensionUOMId] [int] IDENTITY(1,1) NOT NULL,
	[DimesionDescription] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DimensionUOMId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DimensionUOMs] ON
INSERT [dbo].[DimensionUOMs] ([DimensionUOMId], [DimesionDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'meters', NULL, NULL, NULL, NULL)
INSERT [dbo].[DimensionUOMs] ([DimensionUOMId], [DimesionDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'inches', NULL, NULL, NULL, NULL)
INSERT [dbo].[DimensionUOMs] ([DimensionUOMId], [DimesionDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, N'feet', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DimensionUOMs] OFF
/****** Object:  Table [dbo].[Countries]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](max) NULL,
	[CountryCode] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Countries] ON
INSERT [dbo].[Countries] ([CountryId], [CountryName], [CountryCode], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'United States', N'USA', NULL, NULL, NULL, NULL)
INSERT [dbo].[Countries] ([CountryId], [CountryName], [CountryCode], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'Canada', N'CAN', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Countries] OFF
/****** Object:  Table [dbo].[ConditionEvaluations]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConditionEvaluations](
	[ConditionEvaluationId] [int] IDENTITY(1,1) NOT NULL,
	[ConditionEvalDescription] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConditionEvaluationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ConditionEvaluations] ON
INSERT [dbo].[ConditionEvaluations] ([ConditionEvaluationId], [ConditionEvalDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'New', NULL, NULL, NULL, NULL)
INSERT [dbo].[ConditionEvaluations] ([ConditionEvaluationId], [ConditionEvalDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'Good', NULL, NULL, NULL, NULL)
INSERT [dbo].[ConditionEvaluations] ([ConditionEvaluationId], [ConditionEvalDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, N'Average', NULL, NULL, NULL, NULL)
INSERT [dbo].[ConditionEvaluations] ([ConditionEvaluationId], [ConditionEvalDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, N'Poor', NULL, NULL, NULL, NULL)
INSERT [dbo].[ConditionEvaluations] ([ConditionEvaluationId], [ConditionEvalDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (5, N'Unknown', NULL, NULL, NULL, NULL)
INSERT [dbo].[ConditionEvaluations] ([ConditionEvaluationId], [ConditionEvalDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (6, N'Fair', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ConditionEvaluations] OFF
/****** Object:  Table [dbo].[Categories]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](max) NULL,
	[Prefix] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'DRILL PIPE', N'01', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'DRILL COLLARS', N'02', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, N'OTHER DOWNHOLE TOOLS', N'03', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, N'KELLY', N'04', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (5, N'TOP DRIVE', N'05', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (6, N'AUTOMATED PIPE HANDLING SYSTEMS', N'06', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (7, N'RIG FLOOR TOOLS', N'07', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (8, N'CASING EQUIPMENT', N'08', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (9, N'AIR SYSTEMS', N'09', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (10, N'ENGINES', N'10', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (11, N'TRANSMISSIONS', N'11', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (12, N'ROTARY TABLES', N'12', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (13, N'DRAWWORKS', N'13', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (14, N'MUD PUMPS', N'14', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (15, N'PUMPS OTHER', N'15', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (16, N'HIGH PRESSURE MUD SYSTEM', N'16', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (17, N'CEMENTING UNITS', N'17', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (18, N'MAST', N'18', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (19, N'SUBSTRUCTURE', N'19', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (20, N'LOAD PATH EQUIPMENT', N'20', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (21, N'HOISTS', N'21', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (22, N'ELECTRICAL SYSTEMS', N'22', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (23, N'GENERATORS AND ELECTRICAL MOTORS', N'23', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (24, N'INSTRUMENTATION / CONTROL SYSTEMS / RECORDING EQUIPMENT', N'24', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (25, N'COMMUNICATIONS', N'25', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (26, N'MUD CONDITIONING EQUIPMENT', N'26', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (27, N'DRILLING FLUID TANKS', N'27', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (28, N'WATER TANKS', N'28', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (29, N'FUEL TANKS', N'29', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (30, N'WELL CONTROL EQUIPMENT', N'30', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (31, N'ACCUMULATORS AND CLOSING UNITS', N'31', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (32, N'OPERATIONAL SUPPORT EQUIPMENT', N'32', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (33, N'HYDRAULIC POWER UNITS', N'33', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (34, N'CLIMATE CONTROL', N'34', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (35, N'ENVIRONMENT', N'35', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (36, N'RIG BUILDINGS', N'36', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (37, N'RIG CAMPS', N'37', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (38, N'HAND TOOLS', N'38', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (39, N'SAFETY', N'39', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (40, N'MATERIAL HANDLING', N'40', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (41, N'VEHICLES', N'41', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (42, N'GENERAL DRILLING SUPPLIES', N'42', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (43, N'ALLOCATION COST', N'43', NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [Prefix], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (44, N'CROWN BLOCKS', N'44', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Categories] OFF
/****** Object:  Table [dbo].[Languages]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[LanguageId] [int] IDENTITY(1,1) NOT NULL,
	[LanguageName] [nvarchar](max) NULL,
	[LanguageShortName] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Languages] ON
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'English', N'en', NULL, NULL, NULL, NULL)
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'Francais', N'fr', NULL, NULL, NULL, NULL)
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, N'Espanyol', N'es', NULL, NULL, NULL, NULL)
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, N'Portuguese', N'pt-Br', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Languages] OFF
/****** Object:  Table [dbo].[DocumentTypes]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentTypes](
	[DocumentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentTypeName] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DocumentTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DocumentTypes] ON
INSERT [dbo].[DocumentTypes] ([DocumentTypeId], [DocumentTypeName], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'Certificates', NULL, NULL, NULL, NULL)
INSERT [dbo].[DocumentTypes] ([DocumentTypeId], [DocumentTypeName], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'User Manual', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DocumentTypes] OFF
/****** Object:  Table [dbo].[Manufacturers]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manufacturers](
	[ManufacturerId] [int] IDENTITY(1,1) NOT NULL,
	[ManufacturerName] [nvarchar](max) NULL,
	[ManufacturerCode] [nvarchar](max) NULL,
	[OtherManufacturerRef] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ManufacturerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Manufacturers] ON
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'Mitsubishi', N'MIT', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'DELL', N'DELL', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, N'Acer', N'ACER', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, N'LOADCRAFT ', N'LCFT', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (5, N'PARMAC', N'PAR', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (6, N'UNKNOWN ', N'UNKNOWN ', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (7, N'AMERICAN BLOCK ', N'AMERICAN BLOCK ', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (8, N'BRIDON', N'BRI', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (9, N'WARRIOR', N'WAR', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (10, N'WRAM', N'WRAM', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (11, N'5 STAR RIG AND EQUIPMENT', N'5 STAR RIG AND EQUIPMENT', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (12, N'HACKER', N'HAC', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (13, N'CHELSEA', N'CHEL', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (14, N'GARDNER DENVER', N'GARDNER DENVER', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (15, N'BADGER', N'BAD', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (16, N'DOUBLELIFE', N'DOUBLELIFE', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (17, N'DERRICK', N'DER', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (18, N'BALDOR', N'BAL', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (19, N'CW EURO DRIVE', N'CW EURO DRIVE', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (20, N'SWACO', N'SWA', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (21, N'CATERPILLAR', N'CATE', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (22, N'DEUTZAG', N'DEU', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (23, N'STAMFORD', N'STAM', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (24, N'CUTLAR HAMMER', N'CUTLAR HAMMER', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (25, N'STEWART STEVENSON', N'STEWART STEVENSON', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (26, N'GENERAL THERMO DYNAMICS', N'GENERAL THERMO DYNAMICS', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (27, N'MODINE', N'MOD', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (28, N'LINCOLN', N'LIN', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (29, N'SHOP BUILT', N'SHOP BUILT', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (30, N'SULLIVAN PALATEK', N'SULLIVAN PALATEK', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (31, N'WILDEN', N'WILD', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (32, N'GULF ELECTROQUIP', N'GULF ELECTROQUIP', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (33, N'5 STAR', N'5STR', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (34, N'SA', N'SA', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (35, N'FLOW CONTROL', N'FLWCONTL', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (36, N'SHAFFER', N'SHAF', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (37, N'OCO', N'OCO', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (38, N'OSECO', N'OSE', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (39, N'VALVE WORKS', N'VALVE WORKS', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (40, N'MIDWEST HOSE', N'MIDWEST HOSE', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (41, N'MCM OIL TOOLS', N'MCM OIL TOOLS', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (42, N'GRAY', N'GRAY', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (43, N'KAL-KELLY COCK', N'KAL-KELLY COCK', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (44, N'WTM', N'WTM', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (45, N'VARCO', N'VAR', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (46, N'ACCESS OIL TOOL', N'ACCESS OIL TOOL', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (47, N'CONS ENERGY', N'CONS ENERGY', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (48, N'BRADON', N'BRA', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (49, N'AOI', N'AOI', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (50, N'FORUM', N'FRM', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (51, N'HERCULES', N'HER', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (52, N'REDS SATILITE', N'REDS SATILITE', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (53, N'AOXICON', N'AOXI', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (54, N'SPERIAN', N'SPE', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (55, N'MSA', N'MSA', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (56, N'DBI SALA', N'DBI SALA', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName], [ManufacturerCode], [OtherManufacturerRef], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (57, N'ROLLGLISS', N'ROLL', NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Manufacturers] OFF
/****** Object:  Table [dbo].[PurchaseCurrencies]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseCurrencies](
	[PurchaseCurrencyID] [int] IDENTITY(1,1) NOT NULL,
	[PurhcaseCurrencySymbol] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PurchaseCurrencyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PurchaseCurrencies] ON
INSERT [dbo].[PurchaseCurrencies] ([PurchaseCurrencyID], [PurhcaseCurrencySymbol], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'$', NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseCurrencies] ([PurchaseCurrencyID], [PurhcaseCurrencySymbol], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'€', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[PurchaseCurrencies] OFF
/****** Object:  Table [dbo].[WeightUOMs]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WeightUOMs](
	[WeightUOMId] [int] IDENTITY(1,1) NOT NULL,
	[WeightUOMDescription] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[WeightUOMId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WeightUOMs] ON
INSERT [dbo].[WeightUOMs] ([WeightUOMId], [WeightUOMDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'kg', NULL, NULL, NULL, NULL)
INSERT [dbo].[WeightUOMs] ([WeightUOMId], [WeightUOMDescription], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'lbs', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[WeightUOMs] OFF
/****** Object:  Table [dbo].[SubCategories]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategories](
	[SubCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[SubCategoryName] [nvarchar](max) NULL,
	[MarkinginInstructions] [nvarchar](max) NULL,
	[CertOrDocRequirement] [nvarchar](max) NULL,
	[DescriptionTemplate] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SubCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[SubCategories] ON
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, 1, N'DRILL PIPE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, 1, N'DRILL PIPE PUP JOINTS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, 2, N'DRILL COLLARS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, 3, N'STABILIZERS & REAMERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (5, 3, N'SUBS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (6, 3, N'JARS & SHOCK SUBS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (7, 3, N'FISHING TOOLS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (8, 4, N'KELLY', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (9, 4, N'KELLY BUSHINGS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (10, 4, N'KELLY SPINNER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (11, 5, N'TOP DRIVE AC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (12, 5, N'TOP DRIVE HYDRAULIC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (13, 5, N'POWER SWIVEL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (14, 6, N'EZ TORQUE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (15, 6, N'IRON ROUGHNECK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (16, 6, N'PIPE SPINNERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (17, 6, N'TUBING TONGS HYDRAULIC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (18, 6, N'POWER CAT WALK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (19, 6, N'PIPE DECK HANDLER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (20, 6, N'RACKING BOARD PIPE HANDLER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (21, 6, N'ROTATING MOUSHOLE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (22, 6, N'PIPE HANDLING MACHINES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (23, 6, N'CASING STABILIZING ARM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (24, 6, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (25, 7, N'DRILL PIPE TONGS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (26, 7, N'TUBING TONGS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (27, 7, N'DRILL COLLAR CLAMP', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (28, 7, N'DRILL PIPE SLIPS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (29, 7, N'DRILL COLLAR SLIPS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (30, 7, N'TUBING SLIPS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (31, 7, N'TUBING SPIDER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (32, 7, N'POWER SLIPS HYD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (33, 7, N'POWER SLIPS AIR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (34, 7, N'POWER SLIPS SPRING LOADED', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (35, 7, N'BIT BREAKER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (36, 7, N'MUD BUCKET', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (37, 7, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (38, 8, N'CASING ELEVATORS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (39, 8, N'CASING TONGS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (40, 8, N'CASING SLIPS MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (41, 8, N'CASING SPIDER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (42, 8, N'CASING ELEVATORS HYD/AIR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (43, 8, N'CASING TONGS HYD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (44, 8, N'CASING SLIPS (SPIDER) HYD/AIR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (45, 8, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (46, 9, N'AIR COMPRESSOR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (47, 9, N'AIR DRYER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (48, 9, N'AIR RECEIVERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (49, 10, N'ENGINES ', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (50, 10, N'RADIATORS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (51, 11, N'TRANSMISSIONS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (52, 11, N'COMPOUNDS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (53, 11, N'GEAR BOX (>350 HP)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (54, 11, N'TORQUE CONVERTER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (55, 12, N'ROTARY TABLES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (56, 12, N'ROTARY TABLE BUSHINGS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (57, 12, N'CHAINS AND BELTS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (58, 13, N'DRAWWORKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (59, 13, N'SPROKETS CHAINS  ', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (60, 13, N'AUX BRAKE HYDRO', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (61, 13, N'AUX BRAKE ELECTRIC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (62, 13, N'COOLING WATER HEAT EXCHANGER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (63, 13, N'EATON AIR BRAKE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (64, 13, N'CATHEAD FRICTION', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (65, 13, N'CATHEAD HYDRAULIC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (66, 14, N'MUD PUMPS (> 200 HP)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (67, 14, N'PULSATION DAMPNER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (68, 14, N'POPOFF VALVES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (69, 14, N'SPROCKETS, CHAINS, AND BELTS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (70, 15, N'SUBMERSIBLE PUMPS / CELLAR JET', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (71, 15, N'CENTRIFUGAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (72, 15, N'DIESEL POWERED RIVER PUMPS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (73, 16, N'CHICKSAN & LOW TORQUE VALVES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (74, 16, N'STANDPIPE MANIFOLD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (75, 16, N'MUD PUMP MANIFOLD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (76, 16, N'ROTARY HOSE ( > 2.5")', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (77, 16, N'VIBRATER HOSE ( > 2.5")', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (78, 16, N'INTEGRAL SHIPPING HOSE (< 2.5")', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (79, 16, N'ACCUMULATOR / BOP CONTROL HOSES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (80, 16, N'HIGH PRESSURE (>150 PSI) CONNECTIONS AND FITTINGS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (81, 16, N'STRUCTURAL PIPE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (82, 16, N'HOSE (>150 PSI)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (83, 17, N'CEMENT MANIFOLD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (84, 17, N'CEMENT UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (85, 17, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (86, 18, N'MAST', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (87, 18, N'MAST STRUCTURAL MATERIAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (88, 18, N'MAST PINS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (89, 18, N'MAST SHOE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (90, 18, N'MAST STAND', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (91, 18, N'CASING STABBING BOARD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (92, 18, N'TUBING BOARD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (93, 19, N'SUBSTRUCTURE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (94, 19, N'SUBSTRUCTURE STRUCTURAL MATERIAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (95, 19, N'SUBSTRUCTURE PINS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (96, 19, N'MUD BOATS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (97, 19, N'GRASSHOPPER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (98, 19, N'RIG WALKING SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (99, 19, N'RIG JACKING SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (100, 19, N'DRAG CHAIN', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (101, 20, N'DRILL LINE ANCHOR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (102, 20, N'DRILL LINE SPOOLER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (103, 20, N'DRILL LINE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (104, 20, N'CROWN BLOCKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (105, 20, N'TRAVELING BLOCKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (106, 20, N'HOOK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (107, 20, N'BLOCK TROLLEY', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (108, 20, N'SWIVEL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (109, 20, N'DRILL PIPE ELEVATORS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (110, 20, N'DRILL COLLAR ELEVATOR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (111, 20, N'TUBING ELEVATOR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (112, 20, N'ELEVATOR LINKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (113, 20, N'RAISING LINES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (114, 20, N'SLINGS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (115, 20, N'LIFT BEAM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (116, 20, N'PULLEYS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (117, 20, N'SHACKLES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (118, 20, N'PAD EYES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (119, 20, N'MISC LIFTING EQUIPMENT', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (120, 20, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (121, 21, N'BOP HOIST', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (122, 21, N'SUPER SACK TROLLEY', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (123, 21, N'AIR HOIST (> 2 Tons)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (124, 21, N'HYDRAULIC HOIST (> 2 Tons)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (125, 21, N'RACKING BOARD WINCHES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (126, 21, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (127, 22, N'SCR & VFD/AC SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (128, 22, N'HI LINE POWER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (129, 22, N'TRANSFORMERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (130, 22, N'ELECTRICAL LIGHTS, CABLE, PARTS, AND FITTINGS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (131, 23, N'GENERATORS (> 100 KVA)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (132, 23, N'TRACTION MOTORS AC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (133, 23, N'TRACTION MOTORS DC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (134, 23, N'ELECTRICAL MOTORS (> 50 HP)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (135, 23, N'PORTABLE LIGHT STANDS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (136, 23, N'WELDING UNIT ELECTRIC', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (137, 23, N'WELDING UNIT DIESEL ', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (138, 24, N'MARTIN DECKER (STAND ALONE)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (139, 24, N'INTEGRATED DRILLERS CONSOLES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (140, 24, N'DATA ACQUISITION SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (141, 24, N'PVT SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (142, 24, N'AUTO DRILLERS (STAND ALONE)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (143, 24, N'CROWN / FLOOR SAVIER SYSTEMS (STAND ALONE)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (144, 24, N'PIN RECORDERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (145, 24, N'ROTARY TORQUE SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (146, 24, N'DEVIATION SURVEY EQUIPMENT', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (147, 24, N'WIRE LINE MACHINE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (148, 24, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (149, 25, N'COMPUTERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (150, 25, N'NOTE PADS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (151, 25, N'PRINTERS / FAX / COPIERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (152, 25, N'MOBILE COMMUNICATIONS UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (153, 25, N'SATILLITE SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (154, 25, N'RIG INTERCOMS / GAITRONICS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (155, 25, N'HAND HELD UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (156, 25, N'CAMERA SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (157, 25, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (158, 26, N'AGITATOR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (159, 26, N'FLOW DIVIDER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (160, 26, N'CENTRIFUGE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (161, 26, N'DEGASSER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (162, 26, N'DESANDER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (163, 26, N'DESILTER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (164, 26, N'MUD CLEANER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (165, 26, N'MUD GAS SEPERATOR', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (166, 26, N'SHAKERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (167, 26, N'SCALPER SHAKERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (168, 26, N'HOPPERS (High Shear Systems Only)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (169, 26, N'AUGER SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (170, 27, N'BASE OIL STORAGE TANK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (171, 27, N'BRINE STORAGE TANK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (172, 27, N'CHEMICAL STORAGE TANK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (173, 27, N'MUD TANK RESERVE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (174, 27, N'MUD TANK SHAKER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (175, 27, N'MUD TANK SUCTION', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (176, 27, N'MUD TANK ACTIVE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (177, 27, N'MUD TANK PRE MIX', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (178, 27, N'MUD TANK SLUG', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (179, 27, N'STRIPPING TANK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (180, 27, N'LOW PRESSURE PIPING, VALVES AND FITTINGS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (181, 27, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (182, 28, N'DRILLING WATER TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (183, 28, N'POTABLE WATER TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (184, 28, N'BRAKE WATER TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (185, 29, N'DIESEL TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (186, 29, N'GASOLINE TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (187, 29, N'OIL STORAGE TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (188, 29, N'WASTE OIL TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (189, 30, N'ANNULAR PREVENTER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (190, 30, N'SINGLE RAM PREVENTER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (191, 30, N'DOUBLE RAM PREVENTER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (192, 30, N'BOP RAMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (193, 30, N'BOP ELASTOMERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (194, 30, N'BOP REPLACEMENT PARTS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (195, 30, N'DIVERTER VALVES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (196, 30, N'ROTATING HEAD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (197, 30, N'STRIPPER HEAD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (198, 30, N'BELL NIPPLES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (199, 30, N'RISERS, SPOOLS, XO SPOOLS, AND MUD CROSS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (200, 30, N'DSA', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (201, 30, N'MUD CROSS VALVES MANUAL', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (202, 30, N'HCR VALVES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (203, 30, N'SHOOTING NIPPLES / LUBRICATORS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (204, 30, N'TEST FLANGES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (205, 30, N'FLARE AND IGNITION SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (206, 30, N'FLARE PIPING', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (207, 30, N'HARD PIPE CHOKE LINE SPOOLS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (208, 30, N'HARD PIPE KILL LINES SPOOLS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (209, 30, N'CHOKE AND KILL HOSES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (210, 30, N'CHOKE MANIFOLDS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (211, 30, N'SUPER CHOKES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (212, 30, N'SUPER CHOKE CONTROL PANELS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (213, 30, N'CHOKE REPLACEMENT PARTS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (214, 30, N'CUP TESTER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (215, 30, N'BOP TEST PUMP', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (216, 31, N'ACCUMULATOR UNIT', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (217, 31, N'ACCUMULATOR MANIFOLD SECTIONS (SUIT CASES)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (218, 31, N'REMOTE CLOSING UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (219, 32, N'BOP TRANSPORT SKID', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (220, 32, N'PIPE RACKS ', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (221, 32, N'PIPE TUBS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (222, 32, N'SHIPPING CONTAINERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (223, 32, N'MISC EQUIP SKIDS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (224, 32, N'TRANSPORT SKIDS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (225, 32, N'TRANSPORT BINS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (226, 32, N'JUNK BASKETS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (227, 32, N'P-TANK & SURGE TANK', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (228, 32, N'DRY BULK STORAGE TANKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (229, 32, N'UTILITY MANIFOLD SUITCASES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (230, 32, N'BLOCK CRADLE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (231, 32, N'V-DOOR & TOOL SLIDES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (232, 32, N'CATWALKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (233, 32, N'PERSONNEL ELEVATORS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (234, 32, N'MATTING BOARDS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (235, 32, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (236, 33, N'DIESEL POWER UNITS ', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (237, 33, N'ELECTRIC POWER UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (238, 34, N'BOILERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (239, 34, N'STEAM PRESSURE VESSELS ', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (240, 34, N'HEATERS (ELECTRIC & STEAM)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (241, 34, N'FORCED AIR HEATERS (TIOGA)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (242, 34, N'AC UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (243, 34, N'BUG BLOWERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (244, 34, N'HVAC SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (245, 34, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (246, 35, N'WATER TREATMENT UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (247, 35, N'SEWAGE TREATMENT UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (248, 35, N'INCINERATORS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (249, 35, N'PRESSURE WASHERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (250, 35, N'RIG VAC SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (251, 35, N'RIG FLOOR MUD CONTAINMENT SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (252, 35, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (253, 36, N'DOG HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (254, 36, N'ENGINE HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (255, 36, N'GENERATOR HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (256, 36, N'MUD PUMP HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (257, 36, N'SCR HOUSE, VFD / AC HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (258, 36, N'MECHANICS SHOP', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (259, 36, N'ELECTRICIANS SHOP', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (260, 36, N'WELDERS SHOP', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (261, 36, N'TOOL HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (262, 36, N'ACCUMULATOR HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (263, 36, N'CHOKE MANIFOLD HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (264, 36, N'MUD HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (265, 36, N'BOILER HOUSE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (266, 36, N'MISC HOUSES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (267, 37, N'OFFICES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (268, 37, N'SLEEPERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (269, 37, N'REC ROOM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (270, 37, N'KITCHEN UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (271, 37, N'LAUNDRY', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (272, 37, N'BATH / SHOWER UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (273, 37, N'INFIRMERY', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (274, 37, N'MISC UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (275, 38, N'MECHANIC TOOLS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (276, 38, N'ELECTRICAL TOOLS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (277, 38, N'RIG FLOOR TOOLS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (278, 39, N'SAFETY OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (279, 39, N'CASCADE SYSTEMS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (280, 39, N'BREATHING AIR COMPRESSER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (281, 39, N'SCBA', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (282, 39, N'WHEELED FIRE EXTINGUISHERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (283, 39, N'FIRE EXTIGUISHERS HAND HELD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (284, 39, N'FIRE SUPRESSION SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (285, 39, N'GAS DETECTION SYSTEM', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (286, 39, N'GAS DETECTION SYSTEM HAND HELD', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (287, 39, N'MANLIFT', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (288, 40, N'FORKLIFT / LOADER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (289, 40, N'WHEELED CRANE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (290, 40, N'TRACK CRANE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (291, 40, N'PEDISTAL CRANE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (292, 41, N'CARRIERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (293, 41, N'HEAVY TRACTORS UNITS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (294, 41, N'TRAILERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (295, 41, N'VACCUM TRUCKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (296, 41, N'TANKERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (297, 41, N'BULL DOZIERS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (298, 41, N'BACK HOE', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (299, 41, N'GANG / DELIVERY TRUCKS (> 1.5 TON PAY LOAD)', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (300, 41, N'LIGHT TRUCKS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (301, 41, N'AMBULANCES', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (302, 41, N'CREW BUS', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
GO
print 'Processed 300 total records'
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (303, 41, N'SEDAN', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (304, 41, N'OTHER', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (305, 42, N'ROPE, DOPE, AND SOAP, ETC.,', NULL, NULL, N'Pump HorsePower XX, Flowrate YY, Discharge Rate: XX, Foot Lenght:XX', NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (307, 33, N'UNKNOWN', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (309, 27, N'MUD TANK ?', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (310, 27, N'MUD TANK TRIP', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (311, 22, N'AC SYSTEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (312, 23, N'WELDING UNIT ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (313, 15, N'CELLAR PUMP', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (314, 16, N'INTEGRAL UNION HOSE', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (315, 30, N'SPOOL', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (316, 30, N'RAM BLOCK', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (317, 30, N'CHOKE HOSE', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (318, 30, N'CHECK VALVE', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (319, 30, N'IBOP', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (320, 30, N'UPPER WELL CONTROL VALVE', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (321, 30, N'LOWER WELL CONTROL VALVE', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (322, 24, N'WEIGHT INDICATOR', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (323, 36, N'WAREHOUSE', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (324, 39, N'OTHER', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SubCategories] ([SubCategoryId], [CategoryId], [SubCategoryName], [MarkinginInstructions], [CertOrDocRequirement], [DescriptionTemplate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (325, 39, N'FALL PROTECTION DEVICES', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[SubCategories] OFF
/****** Object:  Table [dbo].[ShippingInfoes]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShippingInfoes](
	[ShippingInfoId] [int] IDENTITY(1,1) NOT NULL,
	[OperatingWeight] [float] NULL,
	[WeightUOMId] [int] NULL,
	[DryWeight] [float] NULL,
	[DryWeightUOMId] [int] NULL,
	[Height] [float] NULL,
	[HeightUOMId] [int] NULL,
	[Width] [float] NULL,
	[WidthUOMId] [int] NULL,
	[Diameter] [float] NULL,
	[DiameterUOMId] [int] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
	[OperatingWeightUOM_WeightUOMId] [int] NULL,
	[DryWeightUOM_WeightUOMId] [int] NULL,
	[HeightUOM_DimensionUOMId] [int] NULL,
	[WidthUOM_DimensionUOMId] [int] NULL,
	[DiameterUOM_DimensionUOMId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ShippingInfoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ShippingInfoes] ON
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (4, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (72, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (73, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (81, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (103, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (104, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (106, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (127, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (134, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (135, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (136, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (137, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (138, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (141, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (143, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (145, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (146, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (147, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (148, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (149, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (152, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (153, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (155, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (156, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (157, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (158, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (159, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (160, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (164, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (165, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (166, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (168, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (169, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (170, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (171, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (172, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (173, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (174, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (175, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (176, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (178, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (179, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (180, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (181, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ShippingInfoes] ([ShippingInfoId], [OperatingWeight], [WeightUOMId], [DryWeight], [DryWeightUOMId], [Height], [HeightUOMId], [Width], [WidthUOMId], [Diameter], [DiameterUOMId], [DateModified], [DateAdded], [AddedById], [ModifiedById], [OperatingWeightUOM_WeightUOMId], [DryWeightUOM_WeightUOMId], [HeightUOM_DimensionUOMId], [WidthUOM_DimensionUOMId], [DiameterUOM_DimensionUOMId]) VALUES (182, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ShippingInfoes] OFF
/****** Object:  Table [dbo].[PurchaseInfoes]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseInfoes](
	[PurchaseInfoId] [int] IDENTITY(1,1) NOT NULL,
	[AFENumber] [nvarchar](max) NULL,
	[MaterialReqNumber] [nvarchar](max) NULL,
	[PONumber] [nvarchar](max) NULL,
	[Vendor] [nvarchar](max) NULL,
	[InvoiceNumber] [nvarchar](max) NULL,
	[PurchasePrice] [decimal](18, 2) NULL,
	[PurchaseCurrencyId] [int] NULL,
	[PurchaseDate] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PurchaseInfoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PurchaseInfoes] ON
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (72, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (73, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (81, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (103, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (104, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (106, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (127, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (134, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (135, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (136, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (137, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (138, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (141, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (143, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (145, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (146, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (147, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (148, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (149, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (152, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (153, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (155, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (156, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (157, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (158, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (159, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (160, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (164, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (165, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (166, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (168, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (169, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (170, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (171, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (172, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (173, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (174, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (175, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (176, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (178, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (179, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (180, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (181, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PurchaseInfoes] ([PurchaseInfoId], [AFENumber], [MaterialReqNumber], [PONumber], [Vendor], [InvoiceNumber], [PurchasePrice], [PurchaseCurrencyId], [PurchaseDate], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (182, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[PurchaseInfoes] OFF
/****** Object:  Table [dbo].[ModelTypes]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModelTypes](
	[ModelTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ModelTypeName] [nvarchar](max) NULL,
	[ModelTypeDescription] [nvarchar](max) NULL,
	[ManufacturerId] [int] NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ModelTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ModelTypes] ON
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'Aspire', N'sample netbook model/type', 3, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'Lancer', N'car model/type', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, N'750 ', NULL, 4, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, N'202 ', NULL, 5, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (5, N'TELESCOPING DOUBLE ', N'TELESCOPING DOUBLE ', 4, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (6, N'UNKNOWN ', N'UNKNOWN ', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (7, N'114 X 410,000 ', N'114 X 410,000 ', 4, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (8, N'TELESCOPING HYDRAULIC ', N'TELESCOPING HYDRAULIC ', 4, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (9, N'D30E200 ', N'D30E200 ', 7, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (10, N'6X19 EIP IW RREG', N'6X19 EIP IW RREG', 8, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (11, N'250 H14', N'250 H14', 9, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (12, N'627086', NULL, 9, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (13, N'UNKNOWN', N'UNKNOWN', 9, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (14, N'A6C', N'A6C', 11, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (15, N'UNKNOWN', N'UNKNOWN', 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (16, N'22', NULL, 12, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (17, N'852XGAKP-F4XS', N'852XGAKP-F4XS', 13, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (18, N'PZH8', N'PZH8', 14, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (19, N'PZG7', N'PZG7', 14, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (20, N'B654HIS', N'B654HIS', 15, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (21, N'250', NULL, 16, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (22, N'UNKNOWN', N'UNKNOWN', 18, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (23, N'250', NULL, 17, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (24, N'NA', N'NA', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (25, N'UNKNOWN', N'UNKNOWN', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (26, N'UNKNOWN', N'UNKNOWN', 19, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (27, N'MEERKAT DUAL PT', N'MEERKAT DUAL PT', 20, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (28, N'3508', NULL, 21, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (29, N'C15', N'C15', 21, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (30, N'C18', N'C18', 21, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (31, N'BF 6M 1013 FC', N'BF 6M 1013 FC', 22, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (32, N'CLT 6061-4', N'CLT 6061-4', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (33, N'CLT 5860', N'CLT 5860', 25, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (34, N'SR4', N'SR4', 21, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (35, N'UC 1274H1L 61D', N'UC 1274H1L 61D', 23, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (36, N'FREEDOM 2100', N'FREEDOM 2100', 24, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (37, N'TMAX T6N 800', N'TMAX T6N 800', 25, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (38, N'3771H', N'3771H', 26, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (39, N'323-9136', NULL, 21, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (40, N'X814880', N'X814880', 21, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (41, N'6654172', NULL, 27, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (42, N'SAE 400', N'SAE 400', 28, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (43, N'25D4J-TE', N'25D4J-TE', 30, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (44, N'UNKNOWN', N'UNKNOWN', 21, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (45, N'UKNOWN', N'UKNOWN', 29, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (46, N'HSE-1002-0M', N'HSE-1002-0M', 34, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (47, N'GK', N'GK', 35, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (48, N'LXT', N'LXT', 36, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (49, N'SPACER SPOOL', N'SPACER SPOOL', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (50, N'NA', N'NA', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (51, N'SMX', N'SMX', 44, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (52, N'DBL', N'DBL', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (53, N'SLX', N'SLX', 46, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (54, N'SDXL', N'SDXL', 45, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (55, N'HT35', N'HT35', 45, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (56, N'700-BHS-3', N'700-BHS-3', 47, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (57, N'GEARMATIC', N'GEAR', 48, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (58, N'50', NULL, 49, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (59, N'55000', NULL, 50, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (60, N'129-HA', N'129-HA', 51, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (61, N'200', NULL, 49, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (62, N'300', NULL, 49, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (63, N'5000', NULL, 52, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (64, N'CONEX', N'CONEX', 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (65, N'PANTHER LP3', N'PANTHER LP3', 54, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (66, N'ALTAIR 4X', N'ALTAIR 4X', 55, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (67, N'3403600', NULL, 56, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (68, N'3400800', NULL, 56, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (69, N'3303019', NULL, 57, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (70, N'UNKNOWN', N'UNKNOWN', 31, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (71, N'UNKNOWN', N'UNKNOWN', 32, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (72, N'UNKNOWN', N'UNKNOWN', 33, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (73, N'UNKNOWN', N'UNKNOWN', 4, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (74, N'UNKNOWN', N'UNKNOWN', 37, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (75, N'UNKNOWN', N'UNKNOWN', 38, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (76, N'UNKNOWN', N'UNKNOWN', 39, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (77, N'UNKNOWN', N'UNKNOWN', 40, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (78, N'UNKNOWN', N'UNKNOWN', 41, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (79, N'UNKNOWN', N'UNKNOWN', 42, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (80, N'UNKNOWN', N'UNKNOWN', 43, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (81, N'UNKNOWN', N'UNKNOWN', 44, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (82, N'UNKNOWN', N'UNKNOWN', 45, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (83, N'UNKNOWN', N'UNKNOWN', 50, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (84, N'UNKNOWN', N'UNKNOWN', 53, NULL, NULL, NULL, NULL)
INSERT [dbo].[ModelTypes] ([ModelTypeId], [ModelTypeName], [ModelTypeDescription], [ManufacturerId], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (85, N'1100676', NULL, 56, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ModelTypes] OFF
/****** Object:  Table [dbo].[Locations]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locations](
	[LocationId] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [nvarchar](max) NULL,
	[CountryId] [int] NULL,
	[CostCenterCode] [nvarchar](450) NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_LocationCostCenterCode] UNIQUE NONCLUSTERED 
(
	[CostCenterCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Locations] ON
INSERT [dbo].[Locations] ([LocationId], [LocationName], [CountryId], [CostCenterCode], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, N'Rig 101', 2, N'04', NULL, NULL, NULL, NULL)
INSERT [dbo].[Locations] ([LocationId], [LocationName], [CountryId], [CostCenterCode], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (5, N'UNKNOWN', NULL, N'UNKNOWN', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Locations] OFF
/****** Object:  Table [dbo].[Assets]    Script Date: 01/03/2012 02:36:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assets](
	[AssetId] [int] IDENTITY(1,1) NOT NULL,
	[AssetUniqueId] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryId] [int] NULL,
	[SubCategoryId] [int] NULL,
	[ManufacturerId] [int] NULL,
	[ModelTypeID] [int] NULL,
	[SerialNumber] [nvarchar](max) NULL,
	[ArrangementNumber] [nvarchar](max) NULL,
	[LocationID] [int] NULL,
	[PurchaseInfoID] [int] NULL,
	[MaintenanceRequestID] [int] NOT NULL,
	[ShippingInfoID] [int] NULL,
	[MaterialTransferNumber] [nvarchar](max) NULL,
	[ConditionEvaluationID] [int] NULL,
	[AssetPurchasePrice] [decimal](18, 2) NOT NULL,
	[NetbookValue] [decimal](18, 2) NOT NULL,
	[FairMarketPrice] [decimal](18, 2) NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL,
	[IsPart] [bit] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AssetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assets] ON
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (1, N'13-00001', N'DESCRIPTION	DRAWWORKS 750 HP
HORSEPOWER	750
DRUM SIZE	42" WITH 1 1/8" LEBUS GROOVING
MAXIMUM HOISTING CAPACITY	365,000 LBS / 10 LINES
BRAKE BAND PART NUMBER	
BRAKE PAD PART NUMBER	
CROWN SAVER MAKE	
CROWN SAVER MODEL	
HIGH CLUTCH MAKE	
HIGH CLUTCH MODEL	
HIGH CLUTCH PART NUMBER	
LOW CLUTCH MAKE 	
LOW CLUTCH MODEL	
LOW CLUTCH PART NUMBER	
CHAIN ROLLER SIZE (FOR EACH CHAIN)	
CHAIN ROLLER LENGTH (FOR EACH CHAIN)	
OIL PUMP MAKE	
OIL PUMP MODEL	
', 13, 58, 4, 3, N'080076 ', N'NA', 4, 1, 0, 1, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (2, N'13-00002', N'FEED PUMP #1 MANUFACTURER	MOM
FEED PUMP #1 MODEL 	118
FEED PUMP #1 SIZE	1.5" X 2" X 8"
FEED PUMP #1 SERIAL NUMBER	4965
ELECTIC FEED #1 PUMP MOTOR MANUFACTURER	BALDOR
ELECTIC FEED #1 PUMP MOTOR MODEL / SIZE	0.5
ELECTIC FEED #1 PUMP MOTOR SERIAL NUMBER	F0902192559
FEED PUMP #2 MANUFACTURER	MOM
FEED PUMP #2 MODEL 	118
FEED PUMP #2 SIZE	1.5" X 2" X 8"
FEED PUMP #2 SERIAL NUMBER	4964
ELECTIC FEED #2 PUMP MOTOR MANUFACTURER	BALDOR
ELECTIC FEED #2 PUMP MOTOR MODEL / SIZE	0.5
ELECTIC FEED #2 PUMP MOTOR SERIAL NUMBER	F0902092579
', 13, 60, 5, 4, N'632071-X ', N'NA', 4, NULL, 0, NULL, NULL, 2, CAST(100000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (3, N'18-00003', N'DIMENSIONS	114'' X 10''
MAXIMUM RATED STATIC HOOK LOAD	410,000 LBS / 10 LINES
MAXIMUM RACKING CAPACITY	11,400'' OF 3 1/2" DRILL PIPE
TELESCOPING CYLINDER MANUFACTURER	UNKOWN
TELESCOPING CYLINDER MODEL	UNKOWN
TELESCOPING CYLINDER SERIAL NUMBER	UNKOWN
TELESCOPING CYLINDER DIAMETER	UNKOWN
TELESCOPING CYLINDER STROKE	UNKOWN
BELLY BOARD (YES / NO)	
CASING STABBING BOARD (YES / NO)	
WINTERIZATION (YES / NO)	
', 18, 86, 4, 5, N'080077', N'NA ', 4, 2, 0, 2, NULL, 2, CAST(1111.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1111.00 AS Decimal(18, 2)), NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (4, N'13-00004', N'DESCRIPTION	CATHEAD HYDRAULIC
MAXIMUM LINE PULL	20,000 LBS
STROKE	6''
', 13, 65, 6, 6, N'UNKNOWN ', N'NA ', 4, NULL, 0, NULL, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'BREAK OUT CAT HEAD
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (6, N'13-00005', N'DESCRIPTION	CATHEAD HYDRAULIC
MAXIMUM LINE PULL	20,000 LBS
STROKE	6''
', 13, 65, 6, 6, N'UNKNOWN', N'NA', 4, 4, 0, 4, NULL, 4, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (10, N'20-00006', N'DESCRIPTION	CROWN BLOCKS
MAXIMUM RATED CAPACITY	410,000 LBS
NUMBER OF CLUSTER SHEAVES	4
DIAMETER OF CLUSTER SHEAVES	30"
PIN DIAMETER OF CLUSTER SHEAVES	UNKNOWN
CLUSTER SHEAVE BEARING PART NUMBER	
NUMBER OF FAST LINE SHEAVES	1
DIAMETER OF FAST LINE SHEAVES	30"
PIN DIAMETER OF FAST LINE SHEAVES	UNKNOWN
FAST LINE SHEAVE BEARING PART NUMBER	
', 20, 104, 4, 7, N'080077', N'NA', 4, 8, 0, 8, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (11, N'19-00007', N'DESCRIPTION	SUBSTRUCTURE
NOMINAL HEIGHT	17''6"
CLEAR HEIGHT UNDER ROTARY BEAMS	14''
MAXIMUM SET BACK CAPACITY	250,000 LBS
MAXIMUM ROTARY CAPACITY	410,000 LBS
', 19, 93, 4, 8, N'080078', N'NA', 4, 9, 0, 9, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MISSING MANUFACTURER PLATE WITH SERIAL NUMBER
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (12, N'20-00008', N'DESCRIPTION	TRAVELING BLOCKS 200 TON
RATED CAPACITY	200 TON
SHEAVE NUMBER	5
SHEAVE SIZE	30"
SHEAVE GROOVE SIZE	1 1/4"
SHEAVE BEARING PART NUMBER	
', 20, 105, 7, 9, N'080420', N'NA', 4, 10, 0, 10, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (13, N'20-00009', N'DESCRIPTION	ELEVATOR LINKS 250 TON
RATED CAPACITY	250 TON
LINK DIAMETER	2 1/2"
LINK LENGTH	120"
', 20, 112, 6, 6, N'UNKNOWN', N'NA', 4, 11, 0, 11, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (14, N'20-00010', N'DESCRIPTION	DRILL LINE
LINE DIAMETER	1 1/8"
LINE LENGTH	5000''
', 20, 103, 8, 10, N'089164800', N'NA', 4, 12, 0, 12, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (15, N'05-00011', N'DESCRIPTION	TOP DRIVE HYDRAULIC 250 TON
RATED LOAD CAPACITY	250 TON
MOTOR MANUFACTURER	HAGGLUNDS
MOTOR MODEL NUMBER	CA10064SAON0022
MOTOR SERIAL NUMBER	UNKOWN
MAXIMUM RPM (CONTINUOUS)	200
TORQUE (HIGH GEAR)	12,500 TO 16,892 FT/LBS
TORQUE (LOW GEAR)	25,050 TO 33,794 FT/LBS
', 5, 12, 9, 11, N'WTD 090816', N'NA', 4, 13, 0, 13, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'WITH SHIPPING SKID
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (16, N'33-00012', N'DESCRIPTION	DIESEL POWERED UNITS
', 33, 236, 9, 12, N'A1311-1', N'NA', 4, 14, 0, 14, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (17, N'20-00013', N'DESCRIPTION	SWIVEL 250 TON
RATED LOAD CAPACITY	250 TON
QUICK CHANGE PACKING ASSEMBLY	UNKNOWN
', 20, 108, 10, 15, N'4318', N'NA', 4, 15, 0, 15, NULL, 4, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (18, N'04-00014', N'DESCRIPTION	KELLY SPINNER
LOAD RATED CAPACITY	250 TON
', 4, 10, 11, 14, N'08104', N'NA', 4, 16, 0, 16, NULL, 6, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (19, N'12-00015', N'DESCRIPTION	ROTARY TABLE 250 TON
RATED LOAD CAPACITY	250 TON
ROTARY OPENING DIAMETER	22"
DRIVE TYPE	CHAIN
DRIVE COUPLING TYPE AND PART NMBER	
ROTARY CHAIN SIZE	UNKNOWN
ROTARY CHAIN LENGTH	
MASTER BUSHING TYPE	SPLIT SQUARE SOLID
BOWL INSERT SIZES	UNKNOWN
', 12, 55, 12, 16, N'6956 B', N'NA', 4, 17, 0, 17, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (20, N'33-00016', N'DESCRIPTION	RIG HYDRAULIC POWER UNIT
', 33, 307, 13, 17, N'8093', N'NA', 4, 18, 0, 18, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (21, N'14-00017', N'DESCRIPTION	MUD PUMP 750 HP
MAXIMUM INPUT HORSEPOWER	750
MAXIMUM SPM	145
MAXIMUM WORKING PRESSURE (PSI)	5,000
PULSATION DAMPNER MANUFACTURER	SMITH
PULSATION DAMPNER MODEL	K20
PULSATION DAMPNER SERIAL NUMBER	4678 - 84
RELEIF VALVE MANUFACTURER	OTECO
RELEIF VALVE MODEL	UNKNOWN
RELEIF VALVE SERIAL NUMBER	55113
SUCTION STRAINER	YES
DISCHARGE STRAINER	YES
PUMP PRESSURE GAUGE MANUFACTURER	MF
PUMP PRESSURE GAUGE MODEL / TYPE	TYPE F
PUMP PRESSURE GAUGE SERIAL NUMBER	30115
DRIVE CHAIN SIZE (IF APPLICABLE)	
DRIVE CHAIN LENGTH (IF APPLICABLE)	
BELT SIZE (IF APPLICABLE)	
DRIVE SPROCKET OR HUB DETAILS	
FLUID MODULE MANUFACTURER	
', 14, 66, 14, 18, N'Q017266', N'NA', 4, 19, 0, 19, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (22, N'14-00018', N'DESCRIPTION	MUD PUMP 750 HP
MAXIMUM INPUT HORSEPOWER	750
MAXIMUM SPM	145
MAXIMUM WORKING PRESSURE (PSI)	5,000
PULSATION DAMPNER MANUFACTURER	SMITH
PULSATION DAMPNER MODEL	K20
PULSATION DAMPNER SERIAL NUMBER	4611-68
RELEIF VALVE MANUFACTURER	OTECO
RELEIF VALVE MODEL	UNKNOWN
RELEIF VALVE SERIAL NUMBER	5243
SUCTION STRAINER	YES
DISCHARGE STRAINER	YES
PUMP PRESSURE GAUGE MANUFACTURER	UNKNOWN
PUMP PRESSURE GAUGE MODEL / TYPE	UNKNOWN
PUMP PRESSURE GAUGE SERIAL NUMBER	UNKNOWN
DRIVE CHAIN SIZE (IF APPLICABLE)	
DRIVE CHAIN LENGTH (IF APPLICABLE)	
BELT SIZE (IF APPLICABLE)	
DRIVE SPROCKET OR HUB DETAILS	
FLUID MODULE MANUFACTURER	
', 14, 66, 14, 18, N'Q017267', N'NA', 4, 20, 0, 20, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (23, N'14-00019', N'DESCRIPTION	MUD PUMP 550 HP
MAXIMUM INPUT HORSEPOWER	550
MAXIMUM SPM	145
MAXIMUM WORKING PRESSURE (PSI)	4048
PULSATION DAMPNER MANUFACTURER	HYDRILL
PULSATION DAMPNER MODEL	K20
PULSATION DAMPNER SERIAL NUMBER	UNKNOWN
RELEIF VALVE MANUFACTURER	OTECO
RELEIF VALVE MODEL	UNKNOWN
RELEIF VALVE SERIAL NUMBER	5021
SUCTION STRAINER	YES
DISCHARGE STRAINER	YES
PUMP PRESSURE GAUGE MANUFACTURER	UNKNOWN
PUMP PRESSURE GAUGE MODEL / TYPE	TYPE F
PUMP PRESSURE GAUGE SERIAL NUMBER	25314
DRIVE CHAIN SIZE (IF APPLICABLE)	
DRIVE CHAIN LENGTH (IF APPLICABLE)	
BELT SIZE (IF APPLICABLE)	
DRIVE SPROCKET OR HUB DETAILS	
FLUID MODULE MANUFACTURER	
', 14, 66, 14, 19, N'Q011692', N'NA', 4, 21, 0, 21, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (24, N'15-00020', N'DESCRIPTION	CHARGE PUMP
BODY SIZE	UNKNOWN
IMPELLAR SIZE	UNKNOWN
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 15, 20, N'3563', N'NA', 4, 22, 0, 22, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (25, N'15-00021', N'DESCRIPTION	CHARGE PUMP
BODY SIZE	6" X 5"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'8891', N'NA', 4, 23, 0, 23, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (26, N'15-00022', N'DESCRIPTION	CHARGE PUMP
BODY SIZE	6" X 5"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 17, 23, N'MP 003307', N'NA', 4, 24, 0, 24, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (27, N'23-00023', N'DESCRIPTION	
RATED HORSEPOWER	50
VOLTAGE	UNKNOWN
AMPS	UNKNOWN
RPM	UNKNOWN
FRAME SIZE	UNKNOWN
	
', 23, 134, 18, 22, N'C0902211068', N'NA', 4, 25, 0, 25, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'CHARGE PUMP MOTOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (28, N'23-00024', N'DESCRIPTION	
RATED HORSEPOWER	50
VOLTAGE	UNKNOWN
AMPS	UNKNOWN
RPM	UNKNOWN
FRAME SIZE	UNKNOWN
	
', 23, 134, 18, 22, N'C0902211038', N'NA', 4, 26, 0, 26, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'CHARGE PUMP MOTOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (29, N'16-00025', N'DESCRIPTION	STANDPIPE MANIFOLD
RATED PRESSURE (PSI)	5,000
NOMINAL O.D.	4"
(1) VALVE SIZE	UNKNOWN
RATED PRESSURE (PSI)	UNKNOWN
VALVE MANUFACTURER	UNKNOWN
VALVE MODEL TYPE	UNKNOWN
VALVE SERIAL NUMBER	UNKNOWN
(2) VALVE SIZE	UNKNOWN
RATED PRESSURE (PSI)	UNKNOWN
VALVE MANUFACTURER	UNKNOWN
VALVE MODEL TYPE	UNKNOWN
VALVE SERIAL NUMBER	UNKNOWN
(3) VALVE SIZE	UNKNOWN
RATED PRESSURE (PSI)	UNKNOWN
VALVE MANUFACTURER	UNKNOWN
VALVE MODEL TYPE	UNKNOWN
VALVE SERIAL NUMBER	UNKNOWN
', 16, 74, 6, 6, N'UNKNOWN', N'NA', 4, 27, 0, 27, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (30, N'16-00026', N'DESCRIPTION	MUD PUMP MANIFOLD
RATED PRESSURE (PSI)	5,000
NOMINAL O.D.	4"
(1) VALVE SIZE	4"
RATED PRESSURE (PSI)	5,000
VALVE MANUFACTURER	UNKNOWN
VALVE MODEL TYPE	UNKNOWN
VALVE SERIAL NUMBER	UNKNOWN
(2) VALVE SIZE	4"
RATED PRESSURE (PSI)	5,000
VALVE MANUFACTURER	UNKNOWN
VALVE MODEL TYPE	UNKNOWN
VALVE SERIAL NUMBER	UNKNOWN
(3) VALVE SIZE	4"
RATED PRESSURE (PSI)	5,000
VALVE MANUFACTURER	UNKNOWN
VALVE MODEL TYPE	UNKNOWN
VALVE SERIAL NUMBER	UNKNOWN
', 16, 75, 6, 24, N'UNKNOWN', N'NA', 4, 28, 0, 28, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'SKID MOUNTED
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (31, N'16-00027', N'DESCRIPTION	ROTARY HOSE
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3 1/2"
LENGTH	55''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 76, 6, 6, N'UNKNOWN', N'NA', 4, 29, 0, 29, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (32, N'16-00028', N'DESCRIPTION	VIBRATOR HOSE
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3 1/2"
LENGTH	10''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 77, 6, 6, N'UNKNOWN', N'NA', 4, 30, 0, 30, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'CARRIER TO STANDPIPE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (33, N'16-00029', N'DESCRIPTION	VIBRATOR HOSE
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3 1/2"
LENGTH	20''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 77, 6, 6, N'UNKNOWN', N'NA', 4, 31, 0, 31, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'CARRIER TO MANIFOLD
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (34, N'16-00030', N'DESCRIPTION	VIBRATOR HOSE
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3 1/2"
LENGTH	20''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 77, 6, 6, N'UNKNOWN', N'NA', 4, 32, 0, 32, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MANIFOLD TO PZ8
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (35, N'16-00031', N'DESCRIPTION	VIBRATOR HOSE
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3 1/2"
LENGTH	20''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 77, 6, 6, N'UNKNOWN', N'NA', 4, 33, 0, 33, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MANIFOLD TO PZ8
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (36, N'16-00032', N'DESCRIPTION	VIBRATOR HOSE
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3 1/2"
LENGTH	15''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 77, 6, 6, N'UNKNOWN', N'NA', 4, 34, 0, 34, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MANIFOLD TO PZ7
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (37, N'16-00033', N'DESCRIPTION	VIBRATOR HOSE
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3 1/2"
LENGTH	10''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 77, 6, 6, N'UNKNOWN', N'NA', 4, 35, 0, 35, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'SPARE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (38, N'15-00034', N'DESCRIPTION	CENTRIFUGAL
BODY SIZE	6" X 5"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'9382', N'NA', 4, 36, 0, 36, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MIX PUMP
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (39, N'15-00035', N'DESCRIPTION	CENTRIFUGAL
BODY SIZE	6" X 5"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'9385', N'NA', 4, 37, 0, 37, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MIX PUMP
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (40, N'23-00036', N'DESCRIPTION	
RATED HORSEPOWER	50
VOLTAGE	UNKNOWN
AMPS	UNKNOWN
RPM	UNKNOWN
FRAME SIZE	UNKNOWN
	
', 23, 134, 18, 22, N'C0804290213', N'NA', 4, 38, 0, 38, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MIX PUMP MOTOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (41, N'23-00037', N'DESCRIPTION	
RATED HORSEPOWER	50
VOLTAGE	UNKNOWN
AMPS	UNKNOWN
RPM	UNKNOWN
FRAME SIZE	UNKNOWN
', 23, 134, 18, 22, N'C0902110058', N'NA', 4, 39, 0, 39, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MIX PUMP MOTOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (42, N'27-00038', N'DESCRIPTION	MUD TANK ?
TOTAL CAPACITY (BBL)	241
NUMBER OF COMPARTMENTS	
SIZE OF EACH COMPARTMENT (FT^2)	
CAPACITY OF EACH COMPARTMENT (BBLS)	
DUMP VALVE TYPE	
DUMP VALVE SIZE	
BUTTERFLY VALVE TYPE SIZE AND QUANTITY	
ROOF (YES / NO)	
FIRE SUPPRESSION SYSTEM (YES / NO)	
MISTING SYSTEM (YES / NO)	
', 27, 309, 6, 6, N'UNKNOWN', N'NA', 4, 40, 0, 40, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (43, N'27-00039', N'DESCRIPTION	MUD TANK ?
TOTAL CAPACITY (BBL)	260
NUMBER OF COMPARTMENTS	
SIZE OF EACH COMPARTMENT (FT^2)	
CAPACITY OF EACH COMPARTMENT (BBLS)	
DUMP VALVE TYPE	
DUMP VALVE SIZE	
BUTTERFLY VALVE TYPE SIZE AND QUANTITY	
ROOF (YES / NO)	
FIRE SUPPRESSION SYSTEM (YES / NO)	
MISTING SYSTEM (YES / NO)	
', 27, 309, 6, 6, N'UNKNOWN', N'NA', 4, 41, 0, 41, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (44, N'27-00040', N'DESCRIPTION	MUD TANK ?
TOTAL CAPACITY (BBL)	323
NUMBER OF COMPARTMENTS	
SIZE OF EACH COMPARTMENT (FT^2)	
CAPACITY OF EACH COMPARTMENT (BBLS)	
DUMP VALVE TYPE	
DUMP VALVE SIZE	
BUTTERFLY VALVE TYPE SIZE AND QUANTITY	
ROOF (YES / NO)	
FIRE SUPPRESSION SYSTEM (YES / NO)	
MISTING SYSTEM (YES / NO)	
', 27, 309, 6, 6, N'UNKNOWN', N'NA', 4, 42, 0, 42, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (45, N'26-00041', N'DESCRIPTION	
HORSEPOWER	5
IMPELLAR SIZE	UNKNOWN
SHAFT LENGTH	UNKNOWN
COUPLING TYPE SIZE AND PART NUMBER	
MOTOR MANUFCATURER	UNKNOWN
MOTOR MODEL	UNKNOWN
MOTOR SERIAL NUMBER	UNKNOWN
', 26, 158, 19, 26, N'88190150.05.06.001', N'NA', 4, 43, 0, 43, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'SUCTION TANK
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (46, N'26-00042', N'DESCRIPTION	
HORSEPOWER	5
IMPELLAR SIZE	UNKNOWN
SHAFT LENGTH	UNKNOWN
COUPLING TYPE SIZE AND PART NUMBER	UNKNOWN
MOTOR MANUFCATURER	UNKNOWN
MOTOR MODEL	UNKNOWN
MOTOR SERIAL NUMBER	

', 26, 158, 19, 26, N'880175385.06.06.002', N'NA', 4, 44, 0, 44, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'SUCTION TANK
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (47, N'26-00043', N'DESCRIPTION	
HORSEPOWER	5
IMPELLAR SIZE	UNKNOWN
SHAFT LENGTH	UNKNOWN
COUPLING TYPE SIZE AND PART NUMBER	UNKNOWN
MOTOR MANUFCATURER	UNKNOWN
MOTOR MODEL	UNKNOWN
MOTOR SERIAL NUMBER	
', 26, 158, 19, 26, N'880096409.08.08.002', N'NA', 4, 45, 0, 45, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'SUCTION TANK
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (48, N'26-00044', N'DESCRIPTION	
HORSEPOWER	5
IMPELLAR SIZE	UNKNOWN
SHAFT LENGTH	UNKNOWN
COUPLING TYPE SIZE AND PART NUMBER	UNKNOWN
MOTOR MANUFCATURER	UNKNOWN
MOTOR MODEL	UNKNOWN
MOTOR SERIAL NUMBER	
', 26, 158, 19, 26, N'88096409.08.08.001', N'NA', 4, 46, 0, 46, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'SUCTION TANK
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (49, N'26-00045', N'DESCRIPTION	
HORSEPOWER	10
IMPELLAR SIZE	UNKNOWN
SHAFT LENGTH	UNKNOWN
COUPLING TYPE SIZE AND PART NUMBER	UNKNOWN
MOTOR MANUFCATURER	UNKNOWN
MOTOR MODEL	UNKNOWN
MOTOR SERIAL NUMBER	
', 26, 158, 19, 26, N'880155318.09.09.014', N'NA', 4, 47, 0, 47, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'INTERMEDIATE TANK
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (50, N'26-00046', N'DESCRIPTION	
HORSEPOWER	10
IMPELLAR SIZE	UNKNOWN
SHAFT LENGTH	UNKNOWN
COUPLING TYPE SIZE AND PART NUMBER	UNKNOWN
MOTOR MANUFCATURER	UNKNOWN
MOTOR MODEL	UNKNOWN
MOTOR SERIAL NUMBER	
', 26, 158, 19, 26, N'880155318.09.09.013', N'NA', 4, 48, 0, 48, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'INTERMEDIATE TANK
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (51, N'26-00047', N'DESCRIPTION	
HORSEPOWER	10
IMPELLAR SIZE	UNKNOWN
SHAFT LENGTH	UNKNOWN
COUPLING TYPE SIZE AND PART NUMBER	UNKNOWN
MOTOR MANUFCATURER	UNKNOWN
MOTOR MODEL	UNKNOWN
MOTOR SERIAL NUMBER	
', 26, 158, 19, 26, N'880155318.09.09.012', N'NA', 4, 49, 0, 49, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'INTERMEDIATE TANK
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (52, N'27-00048', N'DESCRIPTION	MUD TANK TRIP
TOTAL CAPACITY (BBL)	60
NUMBER OF FILL PUMPS	2
SIZE OF FILL PUMPS	2" X 3" X 11"
HORSEPOWER OF FILL PUMPS	20
', 27, 310, 6, 6, N'UNKNOWN', N'NA', 4, 50, 0, 50, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (53, N'15-00049', N'DESCRIPTION	CENTRIFUGAL
BODY SIZE	3" X 2"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'9359', N'NA', 4, 51, 0, 51, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'TRIP TANK PUMP
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (54, N'15-00050', N'DESCRIPTION	CENTRIFUGAL
BODY SIZE	3" X 2"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'9358', N'NA', 4, 52, 0, 52, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'TRIP TANK PUMP
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (55, N'26-00051', N'DESCRIPTION	
MAXIMUM FLOW RATE (GPM)	800
ELECTRIC MOTOR VOLTAGE	
ELECTRIC MOTOR MAKE	
ELECTRIC MOTOR MODEL	
ELECTRIC MOTOR SERIAL NUMBER	
', 26, 166, 20, 27, N'MKATD-006', N'NA', 4, 53, 0, 53, NULL, 4, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (56, N'26-00052', N'DESCRIPTION	
MAXIMUM FLOW RATE (GPM)	UNKNOWN
CONE NUMBER 	
CONE SIZE	
', 26, 162, 6, 6, N'UNKNOWN', N'NA', 5, 54, 0, 54, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'STORED IN OPERATOR''S YARD
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (57, N'26-00053', N'DESCRIPTION	
MAXIMUM FLOW RATE (GPM)	UNKNOWN
CONE NUMBER	
CONE SIZE	
', 26, 163, 6, 6, N'UNKNOWN', N'NA', 5, 55, 0, 55, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'STORED IN OPERATOR''S YARD
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (58, N'26-00054', N'DESCRIPTION	
MAXIMUM FLOW RATE (GPM)	UNKNOWN
ELECTRIC MOTOR INFORMATION	
COMPRESSOR INFORMATION	
', 26, 161, 6, 6, N'UNKNOWN', N'NA', 5, 56, 0, 56, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'STORED IN OPERATOR''S YARD
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (59, N'15-00055', N'DESCRIPTION	CENTRIFUGAL
BODY SIZE	6" X 5"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'9383', N'NA', 4, 57, 0, 57, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DESANDER PUMP
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (60, N'15-00056', N'DESCRIPTION	CENTRIFUGAL
BODY SIZE	6" X 5"
IMPELLAR SIZE	11"
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'9384', N'NA', 4, 58, 0, 58, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DESILTER PUMP
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (61, N'15-00057', N'DESCRIPTION	CENTRIFUGAL
BODY SIZE	UNKNOWN
IMPELLAR SIZE	UNKNOWN
PACKING TYPE	UNKNOWN
PARKING PART NUMBER	UNKNOWN
DRIVE TYPE	UNKNOWN
BELT SIZE	UNKNOWN
COUPLING SIZE	UNKNOWN
', 15, 71, 16, 21, N'8887', N'NA', 4, 59, 0, 59, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DEGASSER PUMP
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (62, N'23-00058', N'DESCRIPTION	
RATED HORSEPOWER	75
VOLTAGE	UNKNOWN
AMPS	UNKNOWN
RPM	UNKNOWN
FRAME SIZE	UNKNOWN
	
', 23, 134, 18, 22, N'0710290054', N'NA', 4, 60, 0, 60, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DESANDER PUMP MOTOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (63, N'23-00059', N'DESCRIPTION	
RATED HORSEPOWER	50
VOLTAGE	UNKNOWN
AMPS	UNKNOWN
RPM	UNKNOWN
FRAME SIZE	UNKNOWN
', 23, 134, 18, 22, N'0902110053', N'NA', 4, 61, 0, 61, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (64, N'23-00060', N'DESCRIPTION	
RATED HORSEPOWER	50
VOLTAGE	UNKNOWN
AMPS	UNKNOWN
RPM	UNKNOWN
FRAME SIZE	UNKNOWN
', 23, 134, 18, 22, N'0901220343', N'NA', 4, 62, 0, 62, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DEGASSER PUMP MOTOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (65, N'10-00061', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, 28, N'LLE 00228', N'264-4644', 4, 63, 0, 63, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'PUMP #1 ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (66, N'10-00062', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, 28, N'LLE 00238', N'264-4644', 4, 64, 0, 64, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'PUMP #2 ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (67, N'10-00063', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, NULL, N'JRE 01395', N'254-3835', 4, 65, 0, 65, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'PUMP #3 ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (68, N'10-00064', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, 29, N'P1Z 00234', N'296-2943', 4, 66, 0, 66, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'GENERATOR #1 ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (69, N'10-00065', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, 29, N'P1Z 00233', N'296-2943', 4, 67, 0, 67, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'GENERATOR #2 ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (70, N'10-00066', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, 29, N'JRE 08321', N'241-0020', 4, 68, 0, 68, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DRAWWORKS #1 ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (71, N'10-00067', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, 29, N'JRE 08348', N'241-0020', 4, 69, 0, 69, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DRAWWORKS #2 ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (72, N'10-00068', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 21, 30, N'WJH 06310', N'237-1955', 4, 70, 0, 70, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'TOP DRIVE HPU ENGINE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (73, N'10-00069', N'DESCRIPTION	
RATED BRAKE HORSEPOWER	UNKNOWN
TURBO PART NUMBER	
STARTER PART NUMBER	
OIL PUMP PART NUMBER	
WATER PUMP PART NUMBER	
FUEL PUMP PART NUMBER	
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
FUEL FILTER PART NUMBER	
', 10, 49, 22, 31, N'10855308', N'UNKNOWN', 4, 71, 0, 71, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'RIG CAMP GENERATOR MOTOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (74, N'11-00070', N'DESCRIPTION	
TYPE	OUTPUT DRIVE
', 11, 53, 6, 6, N'UNKNOWN', N'NA', 4, 72, 0, 72, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MUD PUMP #1 GEAR BOX
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (75, N'11-00071', N'DESCRIPTION	
TYPE	OUTPUT DRIVE
', 11, 53, 6, 6, N'UNKNOWN', N'NA', 4, 73, 0, 73, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MUD PUMP #2 GEAR BOX
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (76, N'11-00072', N'DESCRIPTION	
TYPE	OUTPUT DRIVE
', 11, 51, 6, 32, N'3110038800', N'NA', 4, 74, 0, 74, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'PZG7 PUMP TRANSMISSION
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (77, N'11-00073', N'DESCRIPTION	
TYPE	OUTPUT DRIVE
', 11, 51, 25, 33, N'15342', N'NA', 4, 75, 0, 75, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DRAWWORKS TRANSMISSION
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (79, N'11-00074', N'DESCRIPTION	
TYPE	OUTPUT DRIVE
', 11, 51, 25, 33, N'84227', N'NA', 4, 77, 0, 77, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DRAWWORKS TRANSMISSION
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (80, N'23-00075', NULL, 23, 131, 21, 34, N'72L01665', N'NA', 4, 78, 0, 78, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'RIG GENERATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (81, N'23-00076', NULL, 23, 131, 21, 34, N'72L01664', N'NA', 4, 79, 0, 79, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'RIG GENERATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (82, N'23-00077', NULL, 23, 131, 23, 35, N'M09343613', N'NA', 4, 80, 0, 80, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'RIG CAMP GENERATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (83, N'22-00078', N'DESCRIPTION	
RATING (AMPS)	800
INPUT VOLTAGE	480
', 22, 311, 24, 36, N'SLU0089316 IT.001-8UC', N'NA', 4, 81, 0, 81, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'RIG AC SYSTEM
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (84, N'22-00079', N'DESCRIPTION	
RATING (AMPS)	UNKNOWN
INPUT VOLTAGE	UNKNOWN
', 22, NULL, 1, NULL, N'AMB1144825', N'NA', 4, 82, 0, 82, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'RIG CAMP AC SYSTEM
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (85, N'10-00080', NULL, 10, 50, 26, 38, N'UNKNOWN', N'NA', 4, 83, 0, 83, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'PZH8 MUD PUMP ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (86, N'10-00081', NULL, 10, 50, 26, 38, N'UNKNOWN', N'NA', 4, 84, 0, 84, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'PZH8 MUD PUMP ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (87, N'10-00082', NULL, 10, 50, 21, 39, N'2969893', N'NA', 4, 85, 0, 85, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'GENERATOR #1 ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (88, N'10-00083', NULL, 10, 50, 21, 39, N'2969921', N'NA', 4, 86, 0, 86, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'GENERATOR #2 ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (89, N'10-00084', NULL, 10, 50, 21, 44, N'UNKNOWN', N'NA', 4, 87, 0, 87, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DRAWWORKS #1 ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (90, N'10-00085', NULL, 10, 50, 21, 44, N'UNKNOWN', N'NA', 4, 88, 0, 88, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DRAWWORKS #2 ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (91, N'10-00086', NULL, 10, 50, 21, 40, N'228-9900-02', N'NA', 4, 89, 0, 89, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'TOP DRIVE HPU ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (92, N'10-00087', NULL, 10, 50, 27, 41, N'10017883-02', N'NA', 4, 90, 0, 90, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'RIG CAMP GENERATOR ENGINE RADIATOR
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (93, N'23-00088', NULL, 23, 312, 28, 42, N'C1081000346', N'NA', 4, 91, 0, 91, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (94, N'36-00089', N'DESCRIPTION	
	
	
	
SHIPPING INFORMATION	
OPERATING WEIGHT	UNKNOWN
DRY WEIGHT	UNKNOWN
HEIGHT 	8''0"
LENGTH	18''6"
WIDTH	8''0"
', 36, 260, 29, 45, N'UNKNOWN', N'NA', 4, 92, 0, 92, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (95, N'09-00090', N'DESCRIPTION	
CAPACITY (CFM)	UNKNOWN
PRESSURE RATING (PSI)	200
ELECTRIC MOTOR MANUFACTURER	WEG
ELECTRIC MOTOR MODEL	UNKNOWN
ELECTRIC MOTOR SERIAL NUMBER	MO8G-37705
ELECTRIC MOTOR HORSEPOWER	25
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
CHECK VALVE TYPE	
CHECK VALVE PART NUMBER	
', 9, 46, 30, 43, N'09C044', N'NA', 4, 93, 0, 93, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (96, N'09-00091', N'DESCRIPTION	
CAPACITY (CFM)	UNKNOWN
PRESSURE RATING (PSI)	200
ELECTRIC MOTOR MANUFACTURER	LINCOLN
ELECTRIC MOTOR MODEL	UNKNOWN
ELECTRIC MOTOR SERIAL NUMBER	M27440231-02/19-01
ELECTRIC MOTOR HORSEPOWER	25
AIR FILTER PART NUMBER	
OIL FILTER PART NUMBER	
CHECK VALVE TYPE	
CHECK VALVE PART NUMBER	
', 9, 46, 30, 43, N'08M040', N'NA', 4, 94, 0, 94, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (97, N'09-00092', N'DESCRIPTION	
CAPACITY (GAL)	120
PRESSURE RATING (PSI)	UNKNOWN
PRESSURE RELIEF VALVE TYPE	
PRESSURE RELIEF VALVE PART NUMBER	
', 9, 46, 6, 6, N'UNKNOWN', N'NA', 4, 95, 0, 95, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MOUNTED WITH AIR COMPRESSOR #1
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (98, N'09-00093', N'DESCRIPTION	
CAPACITY (GAL)	120
PRESSURE RATING (PSI)	UNKNOWN
PRESSURE RELIEF VALVE TYPE	
PRESSURE RELIEF VALVE PART NUMBER	
	
', 9, 48, 6, 6, N'UNKNOWN', N'NA', 4, 96, 0, 96, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'MOUNTED WITH AIR COMPRESSOR #2
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (99, N'15-00094', NULL, 15, 313, 31, 70, N'08-5080-01', N'NA', 4, 97, 0, 97, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (100, N'34-00095', N'DESCRIPTION	
FAN DAIMETER	48"
', 34, 243, 32, 71, N'UNKNOWN', N'NA', 4, 98, 0, 98, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (101, N'24-00096', N'DESCRIPTION	
WIRE LINE CAPACITY	15,000''
WIRE LINE DIAMETER	.092"
ELECTRIC MOTOR MANUFACTURER	UNKNOWN
ELECTRIC MOTOR MODEL 	UNKNOWN
ELECTRIC MOTOR SERIAL NUMBER	UNKNOWN
ELECTRIC MOTOR HORSEPOWER	UNKNOWN
', 24, 147, 33, 72, N'1403', N'NA', 4, 99, 0, 99, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (102, N'16-00097', N'DESCRIPTION	
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	2"
LENGTH	10''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 314, 6, 6, N'UNKNOWN', N'NA', 4, 100, 0, 100, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'CHICKSAN HOSE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (103, N'16-00098', N'DESCRIPTION	
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	2"
LENGTH	10''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 314, 6, 6, N'UNKNOWN', N'NA', 4, 101, 0, 101, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'CHICKSAN HOSE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (104, N'16-00099', N'DESCRIPTION	
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	2"
LENGTH	10''
INTEGRAL UNION	UNKNOWN
UNION MAKE AND MODEL	UNKNOWN
UNION SIZE	UNKNOWN
', 16, 314, 6, 6, N'UNKNOWN', N'NA', 4, 102, 0, 102, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'CHICKSAN HOSE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (105, N'36-00100', N'DESCRIPTION	
	
	
	
SHIPPING INFORMATION	
OPERATING WEIGHT	UNKNOWN
DRY WEIGHT	UNKNOWN
HEIGHT 	8''0"
LENGTH	20''0"
WIDTH	10''0"
', 36, 253, 4, 73, N'UNKNOWN', N'NA', 4, 103, 0, 103, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (106, N'36-00101', N'DESCRIPTION	
	
	
	
SHIPPING INFORMATION	
OPERATING WEIGHT	UNKNOWN
DRY WEIGHT	UNKNOWN
HEIGHT 	8''0"
LENGTH	31''7"
WIDTH	8''0"
', 36, 258, 29, 45, N'UNKNOWN', N'NA', 4, 104, 0, 104, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (107, N'35-00102', N'DESCRIPTION	
MAX BURNER TEMPERATURE (F)	225
MAX PRESSURE RATING (PSI)	1000
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
', 35, 249, 34, 46, N'15051526', N'NA', 4, 105, 0, 105, NULL, 4, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (109, N'32-00103', N'DESCRIPTION	
	
	
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	3''6"
LENGTH	45''0"
WIDTH	4''0"
', 32, 232, 4, 73, N'UNKNOWN', N'NA', 4, 107, 0, 107, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (110, N'32-00104', N'DESCRIPTION	V-DOOR 
	
	
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	1''0"
LENGTH	18''0"
WIDTH	8''0"
', 32, 231, 4, 73, N'UNKNOWN', N'NA', 4, 108, 0, 108, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (111, N'30-00105', N'DESCRIPTION	
SIZE (IN.)	11
RATED WORKING PRESSURE (PSI)	5,000
BOTTOM CONNECTION	FLANGED
BOTTOM CONNECTION RING GASKET	54
TOP CONNECTION	STUDDED
TOP CONNECTION RING GASKET	54
SOUR GAS SERVICE (YES/NO)	YES
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
', 30, 189, 35, 47, N'16A0024', N'NA', 4, 109, 0, 109, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (112, N'30-00106', N'DESCRIPTION	
SIZE (IN.)	11
RATED WORKING PRESSURE (PSI)	5,000
BOTTOM CONNECTION	STUDDED
BOTTOM CONNECTION RING GASKET	54
TOP CONNECTION	STUDDED
TOP CONNECTION RING GASKET	54
NUMBER OF SIDE OUTLETS	2
OUTLET SIZE (IN.)	3
OUTLET CONNECTION	FLANGED
OUTLET RING GASKET	35
SOUR GAS SERVICE (YES/NO)	UNKNOWN
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
', 30, 191, 36, 48, N'200221310-539', N'NA', 4, 110, 0, 110, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (113, N'30-00107', N'DESCRIPTION	
STACK UP HEIGHT (IN)	24
SIZE (IN.)	11
RATED WORKING PRESSURE (PSI)	5,000
BOTTOM CONNECTION	FLANGED
BOTTOM CONNECTION RING GASKET	54
TOP CONNECTION	FLANGED
TOP CONNECTION RING GASKET	54
NUMBER OF SIDE OUTLETS	0
OUTLET SIZE (IN.)	NA
OUTLET CONNECTION	NA
OUTLET RING GASKET	NA
SOUR GAS SERVICE (YES/NO)	UNKNOWN
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
', 30, NULL, 6, 6, N'UNKNOWN', N'NA', NULL, 111, 0, 111, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (114, N'30-00108', N'DESCRIPTION	
STACK UP HEIGHT (IN.)	24
SIZE (IN.)	11
RATED WORKING PRESSURE (PSI)	5,000
BOTTOM CONNECTION	FLANGED
BOTTOM CONNECTION RING GASKET	54
TOP CONNECTION	FLANGED
TOP CONNECTION RING GASKET	54
NUMBER OF SIDE OUTLETS	0
OUTLET SIZE (IN.)	NA
OUTLET CONNECTION	NA
OUTLET RING GASKET	NA
SOUR GAS SERVICE (YES/NO)	UNKNOWN
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
', 30, 315, 6, 49, N'UNKNOWN', N'NA', 4, 112, 0, 112, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (115, N'30-00109', N'DESCRIPTION	
STACK UP HEIGHT (IN.)	0''4 1/2"
BOTTOM CONNECTION SIZE (IN.)	11
BOTTOM CONNECTION RATED WORKING PRESSURE (PSI)	3,000
BOTTOM CONNECTION	STUDDED
BOTTOM CONNECTION RING GASKET	UNKNOWN
TOP CONNECTION SIZE (IN.)	11
TOP CONNECTION RATED WORKING PRESSURE (PSI)	5,000
TOP CONNECTION	STUDDED
TOP CONNECTION RING GASKET	54
NUMBER OF SIDE OUTLETS	NA
OUTLET SIZE (IN.)	NA
OUTLET CONNECTION	NA
OUTLET RING GASKET	NA
SOUR GAS SERVICE (YES/NO)	UNKNOWN
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
', NULL, NULL, NULL, NULL, N'UNKNOWN', N'NA', 4, 113, 0, 113, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (116, N'30-00110', N'DESCRIPTION	
STACK UP HEIGHT (IN.)	0''4 1/2"
BOTTOM CONNECTION SIZE (IN.)	11
BOTTOM CONNECTION RATED WORKING PRESSURE (PSI)	3,000
BOTTOM CONNECTION	STUDDED
BOTTOM CONNECTION RING GASKET	UNKNOWN
TOP CONNECTION SIZE (IN.)	11
TOP CONNECTION RATED WORKING PRESSURE (PSI)	5,000
TOP CONNECTION	STUDDED
TOP CONNECTION RING GASKET	54
NUMBER OF SIDE OUTLETS	NA
OUTLET SIZE (IN.)	NA
OUTLET CONNECTION	NA
OUTLET RING GASKET	NA
SOUR GAS SERVICE (YES/NO)	UNKNOWN
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
', 30, 200, 6, 50, N'UNKNOWN', N'NA', 4, 114, 0, 114, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (117, N'30-00111', N'DESCRIPTION	
SIZE (IN.)	11
RATED WORKING PRESSURE (PSI)	5,000
RAM TYPE	PIPE
RAM PIPE SIZE (IN.)	4 1/2
RAM GASKET PART NUMBER	
', 30, 316, 36, 48, N'UNKNOWN', N'NA', 4, 115, 0, 115, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (118, N'30-00112', N'DESCRIPTION	
SIZE (IN.)	11
RATED WORKING PRESSURE (PSI)	5,000
RAM TYPE	PIPE
RAM PIPE SIZE (IN.)	3 1/2
RAM GASKET PART NUMBER	
', 30, 316, 36, 48, N'UNKNOWN', N'NA', 4, 116, 0, 116, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (119, N'30-00113', N'DESCRIPTION	
SIZE (IN.)	11
RATED WORKING PRESSURE (PSI)	5,000
RAM TYPE	BLIND
RAM PIPE SIZE (IN.)	NA
RAM GASKET PART NUMBER	UNKNOWN
', 30, 316, 36, 48, N'UNKNOWN', N'NA', 4, 117, 0, 117, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (120, N'31-00114', N'DESCRIPTION	
STATIONS	5
FLUID CAPACITY (GAL)	120 
ACCUMULATOR PRESSURE	3,000
NUMBER OF ACCUMULATOR BOTTLES	12
NUMBER OF ELECTRIC PUMPS	1
REGULATOR PART NUMBER	
STRIPPING VALVE PART NUMBER	
PUMP MANUFACTURER	UNKNOWN
PUMP MODEL	1518XP3E254T
PUMP SERIAL NUMBER	UNKNOWN
ELECTRIC MOTOR MANUFACTURER	WEG
ELECTRIC MOTOR MODEL	1518XP3E254T
ELECTRIC MOTOR SERIAL NUMBER	CF63009
ELECTRIC MOTOR HORSEPOWER	15
NUMBER OF AIR PUMPS	2
AIR PUMP MANUFACTURER	UNKNOWN
AIR PUMP MODEL	UNKNOWN
AIR PUMP SERIAL NUMBER	UNKNOWN
', 31, 216, 37, 74, N'1102-279', N'NA', 4, 118, 0, 118, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (121, N'31-00115', N'DESCRIPTION	
STATIONS	5
UMBELICLE LENGTH (FT)	
', 31, 218, 37, 74, N'UNKNOWN', N'NA', 4, 119, 0, 119, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'LOCATED IN TOP DOG HOUSE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (122, N'30-00116', N'DESCRIPTION	
TUBULAR CONNECTION SIZE	UNKNOWN
TUBULAR CONNECTION TYPE	UNKNOWN
APPLICABLE CASING SIZE (IN.)	13 3/8
APPLICABLE CASING WEIGHTS (LBS/FT)	68 AND 72
GASKET PART NUMBER	UNKNOWN
', 30, 214, 6, NULL, N'UNKNOWN', N'NA', 4, 120, 0, 120, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (123, N'30-00117', N'DESCRIPTION	
POWER SUPPLY	AIR
MAX TEST PRESSURE (PSI)	14,601
CHART RECORDER (YES / NO)	
', 30, 215, 38, 75, N'19757-1-1', N'NA', 4, 121, 0, 121, NULL, 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (124, N'30-00118', N'DESCRIPTION	
MAX RATED PRESSURE (PSI)	5,000
NUMBER OF MANUAL CHOKES	1
CHOKE SIZE (IN.)	2 1/16
CHOKE MANUFACTURER	VALVE WORKS
CHOKE MODEL	
CHOKE SERIAL NUMBER	1009001
VALVE SIZE (IN.)	4  1/16
VALVE MANUFACTURER	
VALVE MODEL	
VALVE SERIAL NUMBER	
VALVE SIZE (IN.)	2  1/16
VALVE MANUFACTURER	
VALVE MODEL	
VALVE SERIAL NUMBER	
GAUGE MANUFACTURER	
GAUGE MODEL	TYPE F
GAUGE SERIAL NUMBER	30081
', 30, 210, 39, 76, N'UNKNOWN', N'NA', 4, 122, 0, 122, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'4 1/16" X 2 1/16"
(9) 2 1/16" 5M VALVES
(2) 4 1/16" 5M VALVES
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (125, N'30-00119', N'DESCRIPTION	
RATED WORKING PRESSURE (PSI)	5,000
NOMINAL I.D.	3
LENGTH	30''
CONNECTION TYPE	FLANGED
CONNECTION SIZE	UNKNOWN
CONNECTION PRESSURE RATING	UNKNOWN
RING GASKET	UNKNOWN
', 30, 317, 40, 77, N'42360-2', N'NA', 4, 123, 0, 123, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (127, N'30-00120', N'DESCRIPTION	
SIZE (IN.)	4 1/16
MAX WORKING PRESSURE (PSI)	5,000
CONNECTION TYPE	UNKNOWN
CONNECTION SIZE	UNKNOWN
CONNECTION RATED PRESSURE	UNKNOWN
RIG GASKET	UNKNOWN
', 30, 202, 41, 78, N'017864', N'NA', 4, 124, 0, 124, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (128, N'30-00121', N'DESCRIPTION	
SIZE (IN.)	2" X 3"
MAX WORKING PRESSURE (PSI)	5,000
CONNECTION TYPE	UNKNOWN
CONNECTION SIZE	UNKNOWN
CONNECTION RATED PRESSURE	UNKNOWN
RIG GASKET	UNKNOWN
', 30, 211, 37, 74, N'UNKNOWN', N'NA', 4, 125, 0, 125, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (129, N'30-00122', NULL, 30, 212, 41, 78, N'CP 93', N'NA', 4, 126, 0, 126, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'SINGLE SWACO STYLE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (130, N'30-00123', N'DESCRIPTION	
SIZE (IN.)	2 1/16
MAXIMUM RATED PRESSURE (PSI)	5,000
CONNECTION TYPE	
CONNECTION SIZE	
CONNECTION PRESSURE RATING (PSI)	
CONNECTION RIG GASKET	
', 30, 318, 39, 76, N'2008001', N'NA', 4, 127, 0, 127, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (131, N'30-00124', N'DESCRIPTION	
	
MAXIMUM RATED PRESSURE (PSI)	5,000
CONNECTION TYPE	XH
CONNECTION SIZE (IN.)	4 1/2
', 30, 319, 42, 79, N'D64626', N'NA', 4, 128, 0, 128, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'STAB IN TYPE FLOAT VALVE
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (132, N'30-00125', N'DESCRIPTION	
	
MAXIMUM RATED PRESSURE (PSI)	5,000
CONNECTION TYPE	IF
CONNECTION SIZE (IN.)	4 1/2
', 30, 320, 43, 80, N'4177-002-IF', N'NA', 4, 129, 0, 129, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (133, N'30-00126', N'DESCRIPTION	
	
MAXIMUM RATED PRESSURE (PSI)	UKNOWN
CONNECTION TYPE	XH
CONNECTION SIZE (IN.)	4 1/2
', 30, 321, 6, NULL, N'7959', N'NA', 4, 130, 0, 130, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (134, N'26-00127', N'DESCRIPTION	
	
MAXIMUM RATED PRESSURE (PSI)	UKNOWN
DIAMETER (IN.)	XH
LENGTH (IN.)	4 1/2
INLET CONNECTION TYPE	
INLET CONNECTION SIZE (IN.)	4
INLET CONNECTION PRESSURE RATING (PSI)	
OUTLET CONNECTION TYPE	
OUTLET CONNECTION SIZE (IN.)	4
OUTLET CONNECTION PRESSURE RATING (PSI)	
', 26, 165, 6, 25, N'UNKNOWN', N'NA', 4, 131, 0, 131, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (135, N'20-00128', N'DESCRIPTION	
SIZE (IN.)	4 1/2"
RATED LOAD CAPACITY (TONS)	250
', 20, 109, 44, 81, N'MS-043FX1', N'NA', 4, 132, 0, 132, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (136, N'20-00129', N'DESCRIPTION	
SIZE (IN.)	4 1/2 EU - 5 EIU
RATED LOAD CAPACITY (TONS)	350
', 20, 109, 44, NULL, N'NLS3431', N'NA', 4, 133, 0, 133, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (137, N'20-00130', N'DESCRIPTION	
SIZE (IN.)	4 1/2
RATED LOAD CAPACITY (TONS)	150
', 20, 109, 44, 51, N'NL84016', N'NA', 4, 134, 0, 134, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (138, N'20-00131', N'DESCRIPTION	
SIZE (IN.)	4 EU - 4 1/2 IEU
RATED LOAD CAPACITY (TONS)	350
', 20, 110, 44, 81, N'NL79625', N'NA', 4, 135, 0, 135, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (139, N'20-00132', N'DESCRIPTION	
SIZE (IN.)	3 1/2
RATED LOAD CAPACITY (TONS)	250
', 20, 109, 6, 52, N'UNKNOWN', N'NA', 4, NULL, 0, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (140, N'20-00133', N'DESCRIPTION	
EU SIZE (IN.)	4 1/2 IUE
RATED LOAD CAPACITY (TONS)	250
', 20, 110, 45, 82, N'7559', N'NA', 4, 136, 0, 136, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (141, N'20-00134', N'DESCRIPTION	
SIZE (IN.)	8
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 20, 110, 6, 6, N'UNKNOWN', N'NA', 4, 137, 0, 137, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (142, N'20-00135', N'DESCRIPTION	
SIZE (IN.)	6 1/2
RATED LOAD CAPACITY (TONS)	150
', 20, 110, 46, 53, N'E31239-373', N'NA', 4, 138, 0, 138, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (143, N'20-00136', N'DESCRIPTION	
SIZE (IN.)	4 3/4
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 20, 110, 6, 6, N'UNKNOWN', N'NA', 4, 139, 0, 139, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (144, N'08-00137', N'DESCRIPTION	
SIZE (IN.)	20
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 38, 6, 6, N'UNKNOWN', N'NA', 4, 140, 0, 140, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (145, N'08-00138', N'DESCRIPTION	
SIZE (IN.)	16
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 38, 6, 6, N'UNKNOWN', N'NA', 4, 141, 0, 141, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (146, N'08-00139', N'DESCRIPTION	
SIZE (IN.)	13 3/8
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 38, 6, 6, N'UNKNOWN', N'NA', 4, 142, 0, 142, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (147, N'08-00140', N'DESCRIPTION	
SIZE (IN.)	10 3/4
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 38, 6, 6, N'UNKNOWN', N'NA', 4, 143, 0, 143, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (148, N'08-00141', N'DESCRIPTION	
SIZE (IN.)	9 5/8
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 38, 6, 6, N'UNKNOWN', N'NA', 4, 144, 0, 144, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (149, N'08-00142', N'DESCRIPTION	
SIZE (IN.)	7 5/8
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 38, 6, 6, N'UNKNOWN', N'NA', 4, 145, 0, 145, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (150, N'08-00143', N'DESCRIPTION	
SIZE (IN.)	7
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 38, 6, 6, N'UNKNOWN', N'NA', 4, 146, 0, 146, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (151, N'20-00144', N'DESCRIPTION	
SIZE (IN.)	3 1/2 - 5
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 20, 109, 6, 6, N'UNKNOWN', N'NA', 4, 147, 0, 147, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (152, N'07-00145', N'DESCRIPTION	
SIZE (IN.)	4 1/2
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 7, 28, 6, 6, N'UNKNOWN', N'NA', 4, 148, 0, 148, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (153, N'07-00146', N'DESCRIPTION	
SIZE (IN.)	3 1/2
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 7, 28, 45, 54, N'UNKNOWN', N'NA', 4, 149, 0, 149, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (154, N'07-00147', N'DESCRIPTION	
SIZE (IN.)	3 1/2
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 7, 28, 45, 54, N'UNKNOWN', N'NA', 4, 150, 0, 150, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (155, N'07-00148', N'DESCRIPTION	
SIZE (IN.)	8
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 7, 29, 6, 6, N'UNKNOWN', N'NA', 4, 151, 0, 151, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (156, N'07-00149', N'DESCRIPTION	
SIZE (IN.)	6 1/2
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 7, 29, 6, 6, N'UNKNOWN', N'NA', 4, 152, 0, 152, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (157, N'07-00150', N'DESCRIPTION	
SIZE (IN.)	4 3/4
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 7, 29, 6, 6, N'UNKNOWN', N'NA', 4, 153, 0, 153, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (158, N'08-00151', N'DESCRIPTION	
SIZE (IN.)	20
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 40, 6, 6, N'UNKNOWN', N'NA', 4, 154, 0, 154, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (159, N'08-00152', N'DESCRIPTION	
SIZE (IN.)	16
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 40, 6, 6, N'UNKNOWN', N'NA', 4, 155, 0, 155, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (160, N'08-00153', N'DESCRIPTION	
SIZE (IN.)	13 3/8
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 40, 6, 6, N'UNKNOWN', N'NA', 4, 156, 0, 156, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (161, N'08-00154', N'DESCRIPTION	
SIZE (IN.)	10 3/4
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 40, 6, 6, N'UNKNOWN', N'NA', 4, 157, 0, 157, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (162, N'08-00155', N'DESCRIPTION	
SIZE (IN.)	9 5/8
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 40, 6, 6, N'UNKNOWN', N'NA', 4, 158, 0, 158, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (163, N'08-00156', N'DESCRIPTION	
SIZE (IN.)	7
RATED LOAD CAPACITY (TONS)	UNKNOWN
', 8, 40, 6, 6, N'UNKNOWN', N'NA', 4, 159, 0, 159, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (164, N'07-00157', N'DESCRIPTION	
RATED LOAD CAPACITY (IBS)	35,000
DRILL PIPE JAW SIZES (IN.)	4 1/2
	3 1/2
DRILL COLLAR JAW SIZES (IN.)	8
	6 3/4
	4 3/4
CASING JAW SIZES (IN.)	20
	16
	13 3/8
	9 5/8
	7
', 7, 25, 45, 55, N'UNKNOWN', N'NA', 4, 160, 0, 160, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (165, N'06-00158', N'DESCRIPTION	
POWER SUPPLY	HYDRAULIC
DRIVE TYPE (CHAIN OR ROLLER)	UNKNOWN
LARGEST PIPE SIZE (IN.)	7
SMALLEST PIPE SIZE (IN.)	2 7/8
', 6, 16, 47, 56, N'060410', N'NA', 4, 161, 0, 161, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (166, N'07-00159', N'DESCRIPTION	
LARGEST PIPE SIZE (IN.)	8 1/2
SMALLEST PIPE SIZE (IN.)	4 3/4
', 7, 27, 6, 6, N'UNKNOWN', N'NA', 4, 162, 0, 162, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (167, N'21-00160', N'DESCRIPTION	
MAXIMUM LOAD CAPACITY (LBS)	8,000
LINE SIZE (IN.)	5/8
WINCH MOTOR MANUFACTURER	PARKER
WINCH MOTOR MODEL NUMBER	25897
WINCH MOTOR SERIAL NUMBER	Y606-1873
', 21, 124, 48, 57, N'662444', N'NA', 4, 163, 0, 163, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'LOCATED ON THE DRILLER''S SIDE FRONT DRAWWORKS
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (168, N'21-00161', N'DESCRIPTION	
MAXIMUM LOAD CAPACITY (LBS)	8,000
LINE SIZE (IN.)	5/8
WINCH MOTOR MANUFACTURER	PARKER
WINCH MOTOR MODEL NUMBER	25897
WINCH MOTOR SERIAL NUMBER	Y0608-1866
', 21, 124, 48, 57, N'662443', N'NA', 4, 164, 0, 164, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (169, N'24-00162', NULL, 24, 322, 49, 58, N'7084', N'NA', 4, 165, 0, 165, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (170, N'24-00163', NULL, 24, 145, 50, 59, N'D30558', N'NA', 4, 166, 0, 166, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'LOCATED AT THE DRILLER''S INSTRUMENT CLUSTER
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (171, N'20-00164', N'DESCRIPTION	
MAXIMUM RATED CAPACITY 9(IBS)	UNKNOWN
MOUNT TYPE	UNKNOWN
DRILL LINE SIZE (IN.)	UNKNOWN
SENSOR MANUFACTURER	UNKNOWN
SENSOR SERIAL NUMBER	UNKNOWN
SENSOR PART NUMBER	UNKNOWN
', 20, 101, 51, 60, N'475', N'NA', 4, 167, 0, 167, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (172, N'24-00165', N'DESCRIPTION	PUMP STROKE COUNTER
', 24, 148, 49, 61, N'D-4202', N'NA', 4, 168, 0, 168, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'LOCATED AT THE DRILLER''S INSTRUMENT CLUSTER
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (173, N'24-00166', N'DESCRIPTION	ROTARY RPM 
', 24, 148, 49, 62, N'D-4706', N'NA', 4, 169, 0, 169, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'LOCATED AT THE DRILLER''S INSTRUMENT CLUSTER
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (174, N'24-00167', N'DESCRIPTION	DIGITALMUD PRESSURE GAUGE
', 24, 148, 52, 63, N'UNKNOWN', N'NA', 4, 170, 0, 170, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'LOCATED AT THE DRILLER''S INSTRUMENT CLUSTER
', N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (175, N'24-00168', N'DESCRIPTION	
CAMERA 0 TO 7 DEG. SERIAL NUMBER	1354
CAMERA 0 TO 14 DEG. SERIAL NUMBER	436
RETRIEVING TOOL (YES / NO)	YES
SINKER BARS (YES / NO)	UNKNOWN
NUMBER OF SINKER BARS	UNKNOWN
', 24, 146, 50, 83, N'UNKNOWN', N'NA', 4, 171, 0, 171, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (176, N'41-00169', N'DESCRIPTION	
NUMBER OF AXLES	7
MAXIMUM HIGHWAY LOAD CAPACITY (LBS)	1,000,000 
	
SHIPPING INFORMATION	
OPERATING WEIGHT	
DRY WEIGHT	
HEIGHT 	
LENGTH	
WIDTH	
DIAMETER	
', 41, 292, 4, 73, N'UNKNOWN', N'NA', 4, 172, 0, 172, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (177, N'29-00170', N'DESCRIPTION	
CAPACITY (BBLS)	233
EQUIPPED WITH FLOW METER (YES / NO)	YES
EQUIPPED WITH SIGHT GLASS (YES / NO)	YES
NUMBER OF DIESEL PUMPS	2
DIESEL PUMP #1 MANUFACTURER	
DIESEL PUMP #1 MODEL	
DIESEL PUMP #1 SERIAL NUMBER	
DIESEL PUMP MOTOR #1 HORSEPOWER	3
DIESEL PUMP MOTOR #1 MANUFACTURER	WEG
DIESEL PUMP MOTOR #1 MODEL	UKNOWN
DIESEL PUMP MOTOR #1 SERIAL NUMBER	1004207242
DIESEL PUMP #2 MANUFACTURER	
DIESEL PUMP #2 MODEL	
DIESEL PUMP #2 SERIAL NUMBER	
DIESEL PUMP #2 HORSEPOWER	3
DIESEL PUMP MOTOR #2 MANUFACTURER	WEG
DIESEL PUMP MOTOR #2 MODEL	UKNOWN
DIESEL PUMP MOTOR #2 SERIAL NUMBER	1004207242
', 29, 185, 4, 73, N'H900-90361', N'NA', 4, 173, 0, 173, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (178, N'28-00171', N'DESCRIPTION	
CAPACITY (BBLS)	491
EQUIPPED WITH FLOW METER (YES / NO)	NO
EQUIPPED WITH SIGHT GLASS (YES / NO)	UKNOWN
NUMBER OF WATER PUMPS	2
WATER PUMP #1 MANUFACTURER	DOUBLELIFE
WATER PUMP #1 MODEL 	178
WATER PUMP #1 SERIAL NUMBER	9388
WATER PUMP #1 TYPE	UKNOWN
WATER PUMP MOTOR #1 HORSEPOWER	20
WATER PUMP MOTOR #1 MANUFACTURER	BALDOR
WATER PUMP MOTOR #1 MODEL	UKNOWN
WATER PUMP MOTOR #1 SERIAL NUMBER	Z0902271316
WATER PUMP #2 MANUFACTURER	DOUBLELIFE
WATER PUMP #2 MODEL	178
WATER PUMP #2 SERIAL NUMBER	9389
WATER PUMP #2 TYPE	UKNOWN
WATER PUMP #2 HORSEPOWER	20
WATER PUMP MOTOR #2 MANUFACTURER	BALDOR
WATER PUMP MOTOR #2 MODEL	UKNOWN
WATER PUMP MOTOR #2 SERIAL NUMBER	Z0902271311
	
NUMBER OF CIRCULATING PUMPS	2
CIRCULATING PUMP #1 MANUFACTURER	MCM
CIRCULATING PUMP #1 MODEL NUMBER	118
CIRCULATING PUMP #1 SERIAL NUMBER	4964
CIRCULATING PUMP #2 MANUFACTURER	MCM
CIRCULATING PUMP #2 MODEL NUMBER	118
CIRCULATING PUMP #2 SERIAL NUMBER	4965
', 28, 182, 4, 73, N'H900-90356', N'NA', 4, 174, 0, 174, NULL, 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (179, N'36-00172', NULL, 36, 323, 6, 64, N'S-P41PX-D', N'NA', 4, 175, 0, 175, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (180, N'39-00173', N'DESCRIPTION	OXYGEN CYLINDER
', 39, NULL, 53, 84, N'CGA878', N'NA', 4, 176, 0, 176, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (181, N'39-00174', NULL, 39, 281, 54, 65, N'1007150146', N'NA', 4, 177, 0, 177, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (182, N'39-00175', NULL, 39, 286, 55, 66, N'UNKNOWN', N'NA', 4, 178, 0, 178, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (183, N'39-00176', N'DESCRIPTION	
SELF RETRACTING LIFE LINE LENGTH (FT)	130
', 39, 325, 56, 67, N'UNKNOWN', N'NA', 4, 179, 0, 179, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (184, N'39-00177', N'DESCRIPTION	
SELF RETRACTING LIFE LINE LENGTH (FT)	30
', 39, 325, 56, 68, N'195', N'NA', 4, 180, 0, 180, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (185, N'39-00178', N'DESCRIPTION	DECENT DEVICE
CABLE LENGTH (FT)	200
', 39, 325, 57, 69, N'121288', N'NA', 4, 181, 0, 181, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assets] ([AssetId], [AssetUniqueId], [Description], [CategoryId], [SubCategoryId], [ManufacturerId], [ModelTypeID], [SerialNumber], [ArrangementNumber], [LocationID], [PurchaseInfoID], [MaintenanceRequestID], [ShippingInfoID], [MaterialTransferNumber], [ConditionEvaluationID], [AssetPurchasePrice], [NetbookValue], [FairMarketPrice], [Comment], [Status], [IsPart], [DateModified], [DateAdded], [AddedById], [ModifiedById]) VALUES (186, N'39-00179', N'DESCRIPTION	HARNESS
', 39, 325, 56, 85, N'UNKNOWN', N'NA', 4, 182, 0, 182, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'InPlace', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Assets] OFF
/****** Object:  Table [dbo].[PhotoArchives]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoArchives](
	[PhotoArchiveId] [int] IDENTITY(1,1) NOT NULL,
	[DateUploaded] [datetime] NOT NULL,
	[PhotoFileName] [nvarchar](max) NULL,
	[AssetId] [int] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PhotoArchiveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 01/03/2012 02:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Documents](
	[DocumentId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentName] [nvarchar](max) NULL,
	[DocumentDescription] [nvarchar](max) NULL,
	[DocumentTypeId] [int] NULL,
	[AssetId] [int] NOT NULL,
	[DateModified] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[AddedById] [int] NULL,
	[ModifiedById] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [Asset_Category]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_Category]
GO
/****** Object:  ForeignKey [Asset_ConditionEvaluation]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_ConditionEvaluation] FOREIGN KEY([ConditionEvaluationID])
REFERENCES [dbo].[ConditionEvaluations] ([ConditionEvaluationId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_ConditionEvaluation]
GO
/****** Object:  ForeignKey [Asset_Location]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_Location]
GO
/****** Object:  ForeignKey [Asset_Manufacturer]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_Manufacturer] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[Manufacturers] ([ManufacturerId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_Manufacturer]
GO
/****** Object:  ForeignKey [Asset_ModelType]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_ModelType] FOREIGN KEY([ModelTypeID])
REFERENCES [dbo].[ModelTypes] ([ModelTypeId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_ModelType]
GO
/****** Object:  ForeignKey [Asset_PurchaseInfo]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_PurchaseInfo] FOREIGN KEY([PurchaseInfoID])
REFERENCES [dbo].[PurchaseInfoes] ([PurchaseInfoId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_PurchaseInfo]
GO
/****** Object:  ForeignKey [Asset_ShippingInfo]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_ShippingInfo] FOREIGN KEY([ShippingInfoID])
REFERENCES [dbo].[ShippingInfoes] ([ShippingInfoId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_ShippingInfo]
GO
/****** Object:  ForeignKey [Asset_SubCategory]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [Asset_SubCategory] FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[SubCategories] ([SubCategoryId])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [Asset_SubCategory]
GO
/****** Object:  ForeignKey [SubCategory_Category]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[SubCategories]  WITH CHECK ADD  CONSTRAINT [SubCategory_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[SubCategories] CHECK CONSTRAINT [SubCategory_Category]
GO
/****** Object:  ForeignKey [Manufacturer_ModelTypes]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[ModelTypes]  WITH CHECK ADD  CONSTRAINT [Manufacturer_ModelTypes] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[Manufacturers] ([ManufacturerId])
GO
ALTER TABLE [dbo].[ModelTypes] CHECK CONSTRAINT [Manufacturer_ModelTypes]
GO
/****** Object:  ForeignKey [Location_Country]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Locations]  WITH CHECK ADD  CONSTRAINT [Location_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Countries] ([CountryId])
GO
ALTER TABLE [dbo].[Locations] CHECK CONSTRAINT [Location_Country]
GO
/****** Object:  ForeignKey [PurchaseInfo_PurchaseCurrency]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[PurchaseInfoes]  WITH CHECK ADD  CONSTRAINT [PurchaseInfo_PurchaseCurrency] FOREIGN KEY([PurchaseCurrencyId])
REFERENCES [dbo].[PurchaseCurrencies] ([PurchaseCurrencyID])
GO
ALTER TABLE [dbo].[PurchaseInfoes] CHECK CONSTRAINT [PurchaseInfo_PurchaseCurrency]
GO
/****** Object:  ForeignKey [ShippingInfo_DiameterUOM]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[ShippingInfoes]  WITH CHECK ADD  CONSTRAINT [ShippingInfo_DiameterUOM] FOREIGN KEY([DiameterUOM_DimensionUOMId])
REFERENCES [dbo].[DimensionUOMs] ([DimensionUOMId])
GO
ALTER TABLE [dbo].[ShippingInfoes] CHECK CONSTRAINT [ShippingInfo_DiameterUOM]
GO
/****** Object:  ForeignKey [ShippingInfo_DryWeightUOM]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[ShippingInfoes]  WITH CHECK ADD  CONSTRAINT [ShippingInfo_DryWeightUOM] FOREIGN KEY([DryWeightUOM_WeightUOMId])
REFERENCES [dbo].[WeightUOMs] ([WeightUOMId])
GO
ALTER TABLE [dbo].[ShippingInfoes] CHECK CONSTRAINT [ShippingInfo_DryWeightUOM]
GO
/****** Object:  ForeignKey [ShippingInfo_HeightUOM]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[ShippingInfoes]  WITH CHECK ADD  CONSTRAINT [ShippingInfo_HeightUOM] FOREIGN KEY([HeightUOM_DimensionUOMId])
REFERENCES [dbo].[DimensionUOMs] ([DimensionUOMId])
GO
ALTER TABLE [dbo].[ShippingInfoes] CHECK CONSTRAINT [ShippingInfo_HeightUOM]
GO
/****** Object:  ForeignKey [ShippingInfo_OperatingWeightUOM]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[ShippingInfoes]  WITH CHECK ADD  CONSTRAINT [ShippingInfo_OperatingWeightUOM] FOREIGN KEY([OperatingWeightUOM_WeightUOMId])
REFERENCES [dbo].[WeightUOMs] ([WeightUOMId])
GO
ALTER TABLE [dbo].[ShippingInfoes] CHECK CONSTRAINT [ShippingInfo_OperatingWeightUOM]
GO
/****** Object:  ForeignKey [ShippingInfo_WidthUOM]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[ShippingInfoes]  WITH CHECK ADD  CONSTRAINT [ShippingInfo_WidthUOM] FOREIGN KEY([WidthUOM_DimensionUOMId])
REFERENCES [dbo].[DimensionUOMs] ([DimensionUOMId])
GO
ALTER TABLE [dbo].[ShippingInfoes] CHECK CONSTRAINT [ShippingInfo_WidthUOM]
GO
/****** Object:  ForeignKey [Asset_PhotoArchives]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[PhotoArchives]  WITH CHECK ADD  CONSTRAINT [Asset_PhotoArchives] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Assets] ([AssetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PhotoArchives] CHECK CONSTRAINT [Asset_PhotoArchives]
GO
/****** Object:  ForeignKey [Document_Asset]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [Document_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Assets] ([AssetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [Document_Asset]
GO
/****** Object:  ForeignKey [Document_DocumentType]    Script Date: 01/03/2012 02:36:16 ******/
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [Document_DocumentType] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[DocumentTypes] ([DocumentTypeId])
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [Document_DocumentType]
GO
