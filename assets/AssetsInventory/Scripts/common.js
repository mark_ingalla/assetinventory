﻿$(document).ready(function () {
    $('.date').datepicker({ dateFormat: "mm/dd/yy" });

    $(".create-btn").button({ icons: { primary: "ui-icon-newwin"} });

//    gridButtonaction();
    $(".save-btn").button({ icons: { primary: "ui-icon-circle-plus" } });
    $(".redirect-lnk").button({ icons: { primary: "ui-icon-arrow-1-w"} });
    $(".edit-lnk").button({ icons: { primary: "ui-icon-pencil"} });
    $(".approve-btn").button({ icons: { primary: "ui-icon-check"} });
});

function gridButtonaction() {
    $(".edit-lnk").button({ icons: { primary: "ui-icon-pencil" }, text: false });
    $(".details-lnk").button({ icons: { primary: "ui-icon-info" }, text: false });
    $(".delete-lnk").button({ icons: { primary: "ui-icon-trash" }, text: false });
}