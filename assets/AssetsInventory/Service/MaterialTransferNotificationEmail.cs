﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Service
{
    public class MaterialTransferNotificationEmail : MaterialTransferEmailService
    {
        public MaterialTransferNotificationEmail()
        {
            Message = AssetResource.MailTemplate.NotificationMaterialTransfer;
            Subject = "Material Transfer Notification";  
        }
    }
}