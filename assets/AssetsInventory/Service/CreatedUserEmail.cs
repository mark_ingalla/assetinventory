﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AssetsInventory.Utilities;

namespace AssetsInventory.Service
{
    public class CreatedUserEmail
    {
        private readonly EmailNotifier _notifier = new EmailNotifier();

        public string UserEmail { get; set; }
        public string AppUrl { get; set; }
        public string Password { get; set; }
        public string Subject { get; set; }
        public CultureInfo Culture { get; set; }

        public CreatedUserEmail()
        {
            Subject = "Account Successfully Created";
        }
        public void Send()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = Culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = Culture;
            var message = AssetResource.MailTemplate.UserCreateMailTemplate;

            //replace
            message = message.Replace("[Username]", UserEmail);
            message = message.Replace("[Password]", Password);
            message = message.Replace("[AppUrl]",AppUrl);

            _notifier.RecepientEmail = UserEmail;
            _notifier.Body = message;
            _notifier.Subject = Subject;
            _notifier.IsBodyHtml = true;
            _notifier.SendEmail();

        }
    }
}