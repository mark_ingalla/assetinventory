﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AssetsInventory.Models;
using AssetsInventory.Utilities;

namespace AssetsInventory.Service
{
    public abstract class MaterialTransferEmailService
    {
        private readonly EmailNotifier _notifier = new EmailNotifier();
        private readonly AssetContext db = new AssetContext();


        public string Subject { get; set; }
        public int MaterialTransferId { get; set; }
        public string UserEmail { get; set; }
        public string AppUrl { get; set; }
        public string Password { get; set; }
        public string MTRNumber { get; set; }
        public CultureInfo Culture { get; set; }

        public string InitiatedBy { get; set; }
        public string FromCountry { get; set; }
        public string TransferReason { get; set; }
        public string TransferType { get; set; }
        
        public string Status { get; set; }

        public string Message { get; set; }

        public void Send()
        {
            var materialtransfer = db.MaterialTransfers.FirstOrDefault(x => x.MaterialTransferNumber == MTRNumber);
            if (materialtransfer != null)
            {
                FromCountry = db.Countries.Find(materialtransfer.FromCountryId).CountryName;
                var initiated = db.Users.Find(materialtransfer.OriginatorId);
                InitiatedBy = initiated.FirstName + " " + initiated.LastName;
                TransferType = db.TransferTypes.Find(materialtransfer.TransferTypeId).TransferTypeName;
                TransferReason = db.TransferReasons.Find(materialtransfer.TransferReasonId).TransferReasonName;
                Status = materialtransfer.Status;
            }
            System.Threading.Thread.CurrentThread.CurrentCulture = Culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = Culture;

            
            //replace
            Message = Message.Replace("[Username]", UserEmail);
            Message = Message.Replace("[MaterialTransferNumber]", MTRNumber);
            Message = Message.Replace("[MTRNumber]", MTRNumber);

            Message = Message.Replace("[InitiatedBy]", InitiatedBy);
            Message = Message.Replace("[FromCountry]", FromCountry);
            Message = Message.Replace("[TransferReason]", TransferReason);
            Message = Message.Replace("[TransferType]", TransferType);
            Message = Message.Replace("[AppUrl]", AppUrl);
            Message = Message.Replace("[Status]", Status);
            _notifier.RecepientEmail = UserEmail;
            _notifier.Body = Message;
            _notifier.Subject = Subject;
            _notifier.IsBodyHtml = true;
            _notifier.SendEmail();
        }
    }
}