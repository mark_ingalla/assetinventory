﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AssetsInventory.Lookups;
using AssetsInventory.Models;
using System.Data.Entity;

namespace AssetsInventory.Service
{
    public class AssetWorkflowService
    {
        private AssetContext db = new AssetContext();

        public void MovetoApproved(int materialtransferid)
        {
            var materialtransfer = db.MaterialTransfers.Find(materialtransferid);

            //check approver list
            var approvedstatus = Enum.GetName(typeof(ApproveTransferStatusEnum), ApproveTransferStatusEnum.ApproveTransfer);
            var checkapprovers = materialtransfer.MaterialTransferApprovals.All(x => x.ApprovalStatus == approvedstatus);
            if (!checkapprovers) return;

            materialtransfer.Status = Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Approved);
            materialtransfer.ApprovalDate = DateTime.Now;
            db.SaveChanges();

            //send email
            var receiver = db.Users.Include(x => x.Language).FirstOrDefault(x => x.UserId == materialtransfer.OriginatorId);
            if (string.IsNullOrWhiteSpace(receiver.Email)) return;
            var email = new MaterialTransferApprovedEmail
                            {
                                MTRNumber = materialtransfer.MaterialTransferNumber,
                                UserEmail = receiver.Email,
                                Culture =
                                    new CultureInfo(receiver.Language == null
                                                        ? "en"
                                                        : receiver.Language.LanguageShortName)

                            };
            email.Send();

            var notificationusers =
                db.NotificationUsers.ToList().Where(
                    x =>
                    x.TransferTypeId == materialtransfer.TransferTypeId 
                    && x.CountryId == materialtransfer.FromCountryId
                    && x.TransferReasons.Any(y => y.TransferReasonId == materialtransfer.TransferReasonId));
            foreach (var approval in notificationusers)
            {
                
                var user = db.Users.Find(approval.UserId);
                if (string.IsNullOrWhiteSpace(user.Email)) continue;
                var notifier = new MaterialTransferNotificationEmail()
                {
                    //AppUrl = "Click this <a href='" + url + "'>Link</a>",
                    Culture =
                        new CultureInfo(user.Language != null
                                            ? user.Language.LanguageShortName
                                            : "en"),
                    MTRNumber = materialtransfer.MaterialTransferNumber,
                    UserEmail = user.Email
                };
                notifier.Send();
                

            }
        }

        public void MovetoTransit(int materialtransferid)
        {


            var materialtransfer = db.MaterialTransfers.Find(materialtransferid);

            //check if there is any shipped assets at all
            if (materialtransfer.TransferAssets.All(x=>!x.IsShipped)) return;

            var approvedstatus = Enum.GetName(typeof(ApproveTransferStatusEnum), ApproveTransferStatusEnum.ApproveTransfer);
            //check previous status

            var materialapprovedstatus = Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Approved);
            if (materialtransfer.Status != materialapprovedstatus) return;

            //check approver list
            var checkapprovers = materialtransfer.MaterialTransferApprovals.All(x => x.ApprovalStatus == approvedstatus);
            if (!checkapprovers) return;

            //check shipping info
            if (string.IsNullOrWhiteSpace(materialtransfer.ShipToAddress)
                && string.IsNullOrWhiteSpace(materialtransfer.Transporter)) return;

            
 
           
            //change status
            materialtransfer.Status = Enum.GetName(typeof (TransferStatusEnum), TransferStatusEnum.Transit);
            materialtransfer.ShippingDate = DateTime.Now;
            db.SaveChanges();
            foreach (var transferasset in materialtransfer.TransferAssets)
            {
                if (transferasset.AssetId.HasValue)
                {
                    var asset = db.Assets.Find(transferasset.AssetId);
                    asset.Status = Enum.GetName(typeof(AssetStatusEnum), !transferasset.IsShipped ? AssetStatusEnum.InPlace : AssetStatusEnum.Transit);

                    db.SaveChanges();
                }
            }

            //send email that mtf is on the way
            var receiver = db.Users.Find(materialtransfer.ReceiverId);
            if (string.IsNullOrWhiteSpace(receiver.Email)) return;
            
            var emailsender = new TransitEmail
            {
                Culture =
                    new CultureInfo(receiver.Language != null  ? receiver.Language.LanguageShortName : "en"),
                UserEmail = receiver.Email,
                MTRNumber = materialtransfer.MaterialTransferNumber
            };
            emailsender.Send();
            
        }

        public bool IsReadyForShipping(int materialtransferid)
        {
            var materialtransfer = db.MaterialTransfers.Find(materialtransferid);

            //check if there is any shipped assets at all
            if (materialtransfer.TransferAssets.All(x => !x.IsShipped)) return false;

            var approvedstatus = Enum.GetName(typeof(ApproveTransferStatusEnum), ApproveTransferStatusEnum.ApproveTransfer);
            //check previous status

            var materialapprovedstatus = Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Approved);
            if (materialtransfer.Status != materialapprovedstatus) return false;

            //check approver list
            var checkapprovers = materialtransfer.MaterialTransferApprovals.All(x => x.ApprovalStatus == approvedstatus);
            if (!checkapprovers) return false;

            //check shipping info
            if (string.IsNullOrWhiteSpace(materialtransfer.ShipToAddress)
                && string.IsNullOrWhiteSpace(materialtransfer.Transporter)) return false;


            return true;
        }


        public void MovetoDiscrepancy(int materialtransferid)
        {
            var materialtransfer = db.MaterialTransfers.Find(materialtransferid);
            materialtransfer.Status = Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Discrepancy);
            db.SaveChanges();
            //asset status will be selected by reciever
            //change assets
            //foreach (var asset in
            //    materialtransfer.TransferAssets.Where(item => item.AssetId.HasValue).Select(item => db.Assets.Find(item.AssetId)).Where(asset => asset != null))
            //{
            //    asset.Status = Enum.GetName(typeof (AssetStatusEnum), AssetStatusEnum.Discrepancy);
            //    db.SaveChanges();
            //}

            
            //notifies email to approvers

            foreach (var approvers in materialtransfer.MaterialTransferApprovals)
            {
                if (string.IsNullOrWhiteSpace(approvers.ApprovingUser.User.Email)) continue;
                var emailsender = new DiscrepancyEmail
                                      {
                                          Culture =
                                              new CultureInfo(
                                              approvers.ApprovingUser.User.Language.LanguageShortName),
                                          UserEmail = approvers.ApprovingUser.User.Email,
                                          MTRNumber = materialtransfer.MaterialTransferNumber
                                      };
                emailsender.Send();
            }

            var notificationusers =
                db.NotificationUsers.ToList().Where(
                    x =>
                    x.TransferTypeId == materialtransfer.TransferTypeId
                    && x.CountryId == materialtransfer.FromCountryId
                    && x.TransferReasons.Any(y => y.TransferReasonId == materialtransfer.TransferReasonId));
            foreach (var approval in notificationusers)
            {

                var user = db.Users.Find(approval.UserId);
                if (string.IsNullOrWhiteSpace(user.Email)) continue;
                var notifier = new MaterialTransferNotificationEmail()
                {
                    //AppUrl = "Click this <a href='" + url + "'>Link</a>",
                    Culture =
                        new CultureInfo(user.Language != null
                                            ? user.Language.LanguageShortName
                                            : "en"),
                    MTRNumber = materialtransfer.MaterialTransferNumber,
                    UserEmail = user.Email
                };
                notifier.Send();


            }

        }

        public void MovetoReceiverApproved(int materialtransferid)
        {
            var materialtransfer = db.MaterialTransfers.Find(materialtransferid);
            //materialtransfer.Status = Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Approved);
            materialtransfer.Status = Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Received);
            //materialtransfer.ApprovalDate = DateTime.Now;
            db.SaveChanges();

            //asset status will be selected by receiver
            //check each asset
            //asset status
            foreach (var transferasset in materialtransfer.TransferAssets.Where(x => x.IsShipped))
            {
                if (transferasset.AssetId.HasValue)
                {
                    var asset = db.Assets.Find(transferasset.AssetId);
                    asset.Status = asset.Status == Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.Received) ? Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.InPlace) : asset.Status;
                    asset.LocationID = materialtransfer.NewLocationId;
                  //  asset.ConditionEvaluationID = transferasset.ConditionEvaluationId;
                    db.SaveChanges();
                }
            }
        }


        public void CancelRejected(int materialtransferid)
        {
            var materialtransfer = db.MaterialTransfers.Find(materialtransferid);
            //change status for assets
            foreach (var asset in from transferasset in materialtransfer.TransferAssets
                                  where transferasset.AssetId.HasValue
                                  select db.Assets.Find(transferasset.AssetId))
            {
                asset.Status = Enum.GetName(typeof(AssetStatusEnum),AssetStatusEnum.InPlace);
                db.SaveChanges();
            }
        }
    }
}