﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AssetsInventory.Utilities;

namespace AssetsInventory.Service
{
    public class TransitEmail : MaterialTransferEmailService
    {
        public TransitEmail()
        {
            Message = AssetResource.MailTemplate.TransitMailTemplate;
            Subject = "Material Transfer on Transit";
        }
        //private readonly EmailNotifier _notifier = new EmailNotifier();

        //public string UserEmail { get; set; }
        //public string MaterialTransferNumber { get; set; }
        //public CultureInfo Culture { get; set; }

        //public void Send()
        //{
        //    System.Threading.Thread.CurrentThread.CurrentCulture = Culture;
        //    System.Threading.Thread.CurrentThread.CurrentUICulture = Culture;

        //    var message = AssetResource.MailTemplate.TransitMailTemplate;
        //    //replace
        //    message = message.Replace("[Username]", UserEmail);
        //    message = message.Replace("[MaterialTransferNumber]", MTRNumber);
        //    _notifier.RecepientEmail = UserEmail;
        //    _notifier.Body = message;
        //    _notifier.Subject = "Material Transfer on Transit";
        //    _notifier.SendEmail();
        //}
    }
}