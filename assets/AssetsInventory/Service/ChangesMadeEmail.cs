﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AssetsInventory.Utilities;

namespace AssetsInventory.Service
{
    public class ChangesMadeEmail
    {
        private readonly EmailNotifier _notifier = new EmailNotifier();

        public string UserEmail { get; set; }
        public string AppUrl { get; set; }
        public string Password { get; set; }
        public CultureInfo Culture { get; set; }

        public void Send()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = Culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = Culture;
            var message = AssetResource.MailTemplate.ChangeRequestMadeTemplate;

            //replace
            message = message.Replace("[Username]", UserEmail);
           

            _notifier.RecepientEmail = UserEmail;
            _notifier.Body = message;
            _notifier.Subject = "Change Request Has been Made";
            _notifier.IsBodyHtml = true;
            _notifier.SendEmail();

        }
    }
}