﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AssetsInventory.Utilities;

namespace AssetsInventory.Service
{
    public class MaterialTransferForApprovalEmail : MaterialTransferEmailService
    {

        public MaterialTransferForApprovalEmail()
        {
            Message = AssetResource.MailTemplate.ForApprovalMTFTemplate;
            Subject = "Material Transfer for Approval";
        }
        //private readonly EmailNotifier _notifier = new EmailNotifier();

        //public string UserEmail { get; set; }
        //public string AppUrl { get; set; }
        //public string Password { get; set; }
        //public string MTRNumber { get; set; }
        //public CultureInfo Culture { get; set; }

        //public string InitiatedBy { get; set; }
        //public string FromCountry { get; set; }
        //public string TransferReason { get; set; }
        //public string TransferType { get; set; }

        //public void Send()
        //{
        //    System.Threading.Thread.CurrentThread.CurrentCulture = Culture;
        //    System.Threading.Thread.CurrentThread.CurrentUICulture = Culture;
        //    var message = AssetResource.MailTemplate.ForApprovalMTFTemplate;

        //    //replace
        //    message = message.Replace("[Username]", UserEmail);
        //    message = message.Replace("[MTRNumber]", MTRNumber);
        //    message = message.Replace("[AppUrl]", AppUrl);

        //    message = message.Replace("[InitiatedBy]", InitiatedBy);
        //    message = message.Replace("[FromCountry]", FromCountry);
        //    message = message.Replace("[TransferReason]", TransferReason);
        //    message = message.Replace("[TransferType]", TransferType);

        //    _notifier.RecepientEmail = UserEmail;
        //    _notifier.Body = message;
        //    _notifier.Subject = "Material Transfer for Approval";
        //    _notifier.IsBodyHtml = true;
        //    _notifier.SendEmail();

        //}
    }
}