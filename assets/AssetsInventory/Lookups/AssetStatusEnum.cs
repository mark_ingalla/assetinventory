﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Lookups
{
    public enum AssetStatusEnum
    {
        InPlace,
        TransferRequested,
        Transit,
        Received,
        Lost,
        Discrepancy
        
    }

    public enum AssetReceiveStatusEnum
    {
        Received,
        Lost,
        Discrepancy
    }
}