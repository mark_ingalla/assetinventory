﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.Models;

namespace AssetsInventory.Lookups
{
    public class TransferStatusLookup
    {

        public static IList<string> StatusLookup()
        {
            var result =
                Enum.GetValues(typeof (TransferStatusEnum)).Cast<TransferStatusEnum>().ToList().Select(v => v.ToString())
                    .ToList();
            return result;
        }

        public static IList<string> ApproveStatusLookup()
        {
            var result =
                Enum.GetValues(typeof(ApproveTransferStatusEnum)).Cast<ApproveTransferStatusEnum>().ToList().Select(v => v.ToString())
                    .ToList();
            return result;
        }

        public static IList<string> ReceiveStatusLookup()
        {
            var result =
                Enum.GetValues(typeof(ReceiveTransferStatusEnum)).Cast<ReceiveTransferStatusEnum>().ToList().Select(v => v.ToString())
                    .ToList();
            return result;
        }


    }
}