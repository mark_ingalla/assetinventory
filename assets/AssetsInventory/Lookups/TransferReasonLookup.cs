﻿using System.Collections.Generic;

namespace AssetsInventory.Lookups
{
    public class TransferReasonLookup
    {
        

        public static IList<string> GetTransferReasons()
        {
            //return new List<TransferReason>()
            //           {
            //               new TransferReason{Id = 1,Name ="Country-Country"},
            //               new TransferReason{Id = 2,Name ="Disposal"},
            //               new TransferReason{Id = 3,Name ="Return for Repair"},
            //               new TransferReason{Id = 4,Name ="Return for Credit"},
            //               new TransferReason{Id = 5,Name ="Repair and Return"},
            //               new TransferReason{Id = 6,Name ="Sale"},
            //               new TransferReason{Id = 7,Name ="Stock"},
            //               new TransferReason{Id = 8,Name ="Subsidiary-Subsidiary"},
            //               new TransferReason{Id = 9,Name ="Trade"},
            //               new TransferReason{Id = 10,Name ="Warranty"},
            //               new TransferReason{Id = 11,Name ="Other"}
            //           };
            return new List<string>
                       {
                          "Country-Country",
                          "Disposal",
                           "Return for Repair",
                           "Return for Credit",
                           "Repair and Return",
                           "Sale",
                           "Stock",
                           "Subsidiary-Subsidiary",
                           "Trade",
                           "Warranty",
                           "Other"
                       };
        }
    }
}