﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Lookups
{
    public class Caution
    {
        public static IList<string> Cautions()
        {
            return new List<string>()
                       {
                           "Hazardous",
                           "Non-Hazardous",
                           "MSDS Sheet",
                           "PPE Required"
                       };
        }
    }
}