﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Lookups
{
    public class ChangeRequestStatusLookup
    {
        public static IList<string> ChangeRequestsStatusLookup()
        {
            var result =
                Enum.GetValues(typeof(ChangeRequestStatus)).Cast<ChangeRequestStatus>().ToList().Select(v => v.ToString())
                    .ToList();
            return result;
        }

    }

    public enum ChangeRequestStatus
    {
        ChangeRequested,
        ChangeMade
    }

}