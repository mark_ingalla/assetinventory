﻿using System.Collections.Generic;

namespace AssetsInventory.Lookups
{
    public class TransferTypeLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static IList<TransferTypeLookup> GetTransferTypes()
        {
            return new List<TransferTypeLookup>()
                       {
                           new TransferTypeLookup{Id = 1,Name="In Country"},
                           new TransferTypeLookup{Id = 2,Name = "Out of Country"}
                       };
        }
    }
}