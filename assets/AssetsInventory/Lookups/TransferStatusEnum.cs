﻿namespace AssetsInventory.Lookups
{
    public enum TransferStatusEnum
    {
        Draft,
        TransferRequested,
        Cancelled,
        Approved,
        Transit,
        Disapproved,
        Exception,
        Discrepancy,
        Received
    }

    public enum ApproveTransferStatusEnum
    {
        ApproveTransfer,
        DisapproveTransfer,
        Discrepancy
    }

    public enum ReceiveTransferStatusEnum
    {
        Discrepancy,
        Received
    }
}