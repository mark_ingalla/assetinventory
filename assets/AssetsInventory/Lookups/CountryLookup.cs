﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AssetsInventory.Lookups
{
    public class CountryLookup
    {
        public static IDictionary<int, string> GetCountries()
        {
            var dic = new Dictionary<int, string>();
            //var col = new List<string>();

            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures))
            {
                var ri = new RegionInfo(ci.LCID);
                if (!dic.ContainsKey(ri.GeoId))
                   // dic.Add(ri.EnglishName, ri.TwoLetterISORegionName.ToLowerInvariant());
                    dic.Add(ri.GeoId, ri.DisplayName);

                //if (!col.Contains(ri.EnglishName))
                //    col.Add(ri.EnglishName);
            }

            //col.Sort();
            return dic.OrderBy(x => x.Value).ToDictionary(ky => ky.Key, kv => kv.Value);

        }
    }
}