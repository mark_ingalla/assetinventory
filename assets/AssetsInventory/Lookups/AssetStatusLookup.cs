﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.Models;

namespace AssetsInventory.Lookups
{
    public class AssetStatusLookup
    {
        public static IList<string> StatusLookup()
        {
            var result =
                Enum.GetValues(typeof(AssetStatusEnum)).Cast<AssetStatusEnum>().ToList().Select(v => v.ToString())
                    .ToList();
            return result;
        }

        public static IList<string> ReceiveStatusLookup()
        {
            var result =
                Enum.GetValues(typeof(AssetReceiveStatusEnum)).Cast<AssetReceiveStatusEnum>().ToList().Select(v => v.ToString())
                    .ToList();
            return result;
        }
    }
}