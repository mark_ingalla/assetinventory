﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.ViewModels
{
    public class SubCategoryDropdownViewModel
    {
        public int? SubCategoryId { get; set; }
    }
}