﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AssetsInventory.DTO;
using AssetsInventory.Models;

namespace AssetsInventory.ViewModels
{
    
    public class EditAssetPageViewModel
    {

        private AssetContext db = new AssetContext();

        public int AssetId { get; set; }
        public string AssetUniqueId { get; set; }
        public string Description { get; set; }
        //[Required]
        public int? CategoryId { get; set; }
        //[Required]
        public int? SubCategoryId { get; set; }
        //[Required]
        public int? ManufacturerId { get; set; }
        //[Required]
        public int? ModelTypeID { get; set; }
        [Required]
        public string SerialNumber { get; set; }
        public string ArrangementNumber { get; set; }
        public int? LocationID { get; set; }
   //     public int? PurchaseInfoID { get; set; }
        public int MaintenanceRequestID { get; set; }
      //  public int? ShippingInfoID { get; set; }
        public string MaterialTransferNumber { get; set; }
        public IList<MaterialTransferDTO> MaterialTransfers { get; set; }
        [Required]
        public int? ConditionEvaluationID { get; set; }
        public string AssetPurchasePrice { get; set; }
        public decimal NetbookValue { get; set; }
        public string FairMarketPrice { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public bool IsPart { get; set; }


        //shipping info
        public int ShippingInfoId { get; set; }
        public double? OperatingWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public double? DryWeight { get; set; }
        public int? DryWeightUOMId { get; set; }
        public double? Height { get; set; }
        public int? HeightUOMId { get; set; }
        public double? Width { get; set; }
        public int? WidthUOMId { get; set; }
        public double? Diameter { get; set; }
        public int? DiameterUOMId { get; set; }

        //purchase info
        public int PurchaseInfoId { get; set; }
        public string AFENumber { get; set; }
        public string MaterialReqNumber { get; set; }
        public string PONumber { get; set; }
        public string Vendor { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal? PurchasePrice { get; set; }
        public int? PurchaseCurrencyId { get; set; }
        public DateTime? PurchaseDate { get; set; }

        public IList<DocumentDTO> Documents { get; set; }
        public IList<PhotoAssetDTO> Photos { get; set; }
        
        public EditAssetPageViewModel(){}

        public EditAssetPageViewModel(Asset asset)
        {
            AssetId = asset.AssetId;
            CategoryId = asset.CategoryId;
            SubCategoryId = asset.SubCategoryId;
            Description = asset.Description;
            ManufacturerId = asset.ManufacturerId;
            ModelTypeID = asset.ModelTypeID;
            SerialNumber = asset.SerialNumber;
            ArrangementNumber = asset.ArrangementNumber;
            LocationID = asset.LocationID;
            //MaterialTransferNumber = asset.MaterialTransferNumber;
            ConditionEvaluationID = asset.ConditionEvaluationID;
            AssetUniqueId = asset.AssetUniqueId;
            AssetPurchasePrice = asset.AssetPurchasePrice.ToString();
            FairMarketPrice = asset.FairMarketPrice.ToString();
            Comment = asset.Comment;
            Status = asset.Status;

            if (asset.ShippingInfo != null)
            {
                ShippingInfoId = asset.ShippingInfo.ShippingInfoId;
                Diameter = asset.ShippingInfo.Diameter;
                DiameterUOMId = asset.ShippingInfo.DiameterUOMId;
                DryWeight = asset.ShippingInfo.DryWeight;
                DryWeightUOMId = asset.ShippingInfo.DryWeightUOMId;
                Height = asset.ShippingInfo.Height;
                HeightUOMId = asset.ShippingInfo.HeightUOMId;
                OperatingWeight = asset.ShippingInfo.OperatingWeight;
                WeightUOMId = asset.ShippingInfo.WeightUOMId;
                Width = asset.ShippingInfo.Width;
                WidthUOMId = asset.ShippingInfo.WidthUOMId;

            }

            if (asset.PurchaseInfo != null)
            {
                PurchaseInfoId = asset.PurchaseInfo.PurchaseInfoId;
                AFENumber = asset.PurchaseInfo.AFENumber;
                MaterialReqNumber = asset.PurchaseInfo.MaterialReqNumber;
                PONumber = asset.PurchaseInfo.PONumber;
                Vendor = asset.PurchaseInfo.Vendor;
                InvoiceNumber = asset.PurchaseInfo.InvoiceNumber;
                PurchasePrice = asset.PurchaseInfo.PurchasePrice;

                if (asset.PurchaseInfo.PurchaseCurrency != null)
                    PurchaseCurrencyId = asset.PurchaseInfo.PurchaseCurrencyId;
                PurchaseDate = asset.PurchaseInfo.PurchaseDate;
            }

            Documents = asset.Documents.Select(x => new DocumentDTO
                                                        {
                                                            DocumentDescription = x.DocumentDescription,
                                                            DocumentId = x.DocumentId,
                                                            DocumentName = x.DocumentName
                                                        }).ToList();
            
            Photos = asset.PhotoArchives.Select(x => new PhotoAssetDTO
                                                         {
                                                             DateUploaded = x.DateUploaded,
                                                             PhotoArchiveId =  x.PhotoArchiveId,
                                                             PhotoFileName = x.PhotoFileName,
                                                             UploadedBy =  Uploader(x.AddedById ?? 0)
                                                         }).ToList();

            //MaterialTransfers = asset.TransferAssets.Select(x => new MaterialTransferDTO
            //                                                         {
            //                                                             MaterialTransferId =  x.MaterialTransferId,
            //                                                             MaterialTransferNumber = x.MaterialTransfer.MaterialTransferNumber
            //                                                         }).ToList();
        }

        public void UpdateModel(Asset asset)
        {
            asset.CategoryId = CategoryId;
            asset.SubCategoryId = SubCategoryId;
            asset.Description = Description;
            asset.ManufacturerId = ManufacturerId;
            asset.ModelTypeID = ModelTypeID;
            asset.SerialNumber = SerialNumber;
            asset.ArrangementNumber = ArrangementNumber;
            asset.LocationID = LocationID;
            asset.MaterialTransferNumber = MaterialTransferNumber;
            asset.ConditionEvaluationID = ConditionEvaluationID;
            asset.Status = Status;
            var shippinginfo = asset.ShippingInfo ?? new ShippingInfo();
            shippinginfo.Height = Height;
            shippinginfo.HeightUOMId = HeightUOMId;
            shippinginfo.Width = Width;
            shippinginfo.WidthUOMId = WidthUOMId;
            shippinginfo.Diameter = Diameter;
            shippinginfo.DiameterUOMId = DiameterUOMId;

            shippinginfo.OperatingWeight = OperatingWeight;
            shippinginfo.WeightUOMId = WeightUOMId;
            shippinginfo.DryWeight = DryWeight;
            shippinginfo.DryWeightUOMId = DryWeightUOMId;

            asset.ShippingInfo = shippinginfo;

            var purchaseinfo = asset.PurchaseInfo ?? new PurchaseInfo();
            purchaseinfo.AFENumber = AFENumber;
            purchaseinfo.InvoiceNumber = InvoiceNumber;
            purchaseinfo.MaterialReqNumber = MaterialReqNumber;
            purchaseinfo.PONumber = PONumber;
            purchaseinfo.PurchaseCurrencyId = PurchaseCurrencyId;
            purchaseinfo.PurchaseDate = PurchaseDate;
            purchaseinfo.PurchasePrice = PurchasePrice;
            purchaseinfo.Vendor = Vendor;

            asset.PurchaseInfo = purchaseinfo;

            decimal assetpurchase = 0;
            asset.AssetPurchasePrice = decimal.TryParse(AssetPurchasePrice,out assetpurchase) ? assetpurchase : 0;
            asset.NetbookValue = NetbookValue;
            decimal fairmarket = 0;
            asset.FairMarketPrice = decimal.TryParse(FairMarketPrice,out fairmarket) ? fairmarket : 0;

        }

        public void UserUpdateModel(Asset asset)
        {
            asset.ConditionEvaluationID = ConditionEvaluationID;
            asset.Comment = Comment;
        }

        private string Uploader(int id)
        {
            var uploader = db.Users.Find(id);
            return uploader != null ? uploader.UserName:string.Empty;
        }
        


    }
}