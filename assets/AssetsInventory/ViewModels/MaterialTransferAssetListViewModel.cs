﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.DTO;

namespace AssetsInventory.ViewModels
{
    public class MaterialTransferAssetListViewModel
    {
        public int StartCount { get; set; }
        public IList<MaterialTransferAssetDTO> Assets { get; set; }
    }
}