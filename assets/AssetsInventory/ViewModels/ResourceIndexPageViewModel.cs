﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.ViewModels
{
    public class ResourceIndexPageViewModel
    {
        public IList<ResourceFile> ResourceFiles { get; set; }

        public ResourceIndexPageViewModel()
        {
            ResourceFiles = new List<ResourceFile>
                                {
                                    new ResourceFile {ResourceName = "Common"},
                                    new ResourceFile {ResourceName = "AssetResource"},
                                    new ResourceFile {ResourceName = "MaterialTransfer"}
                                };
        }

        public class ResourceFile
        {
            public string ResourceName { get; set; }
            
        }
    }
}