﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.ViewModels
{
    public class MaterialTransferLocationViewModel
    {
        public int? CurrentLocationId { get; set; }
        public int? NewLocationId { get; set; }

        public MaterialTransferLocationViewModel()
        {
            
        }

        public MaterialTransferLocationViewModel(int? currentlocation,int? newlocation)
        {
            CurrentLocationId = currentlocation;
            NewLocationId = newlocation;
        }
    }
}