﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AssetsInventory.DTO;
using AssetsInventory.Lookups;
using AssetsInventory.Models;

namespace AssetsInventory.ViewModels
{
    public class EditMaterialTransferPageViewModel
    {
        public int MaterialTransferId { get; set; }
        //[Required]
        public int TransferTypeId { get; set; }
        //[Required]
        public int TransferReasonId { get; set; }
        // public int? CautionId { get; set; }
        public string MaterialTransferNumber { get; set; }
        public string ManifestNumber { get; set; }
        //[Required]
        public int FromCountryId { get; set; }
        //[Required]
        public int? CurrentLocationId { get; set; }
        public string AFENumber { get; set; }
        public string Transporter { get; set; }
        //[Required]
        public int ToCountryId { get; set; }
        //[Required]
        public int? NewLocationId { get; set; }
        public string RepairWorkOrder { get; set; }
        public string TruckNumber { get; set; }
        public string AttnReceiver { get; set; }
        //[Required]
        public int ReceiverId { get; set; }
        public string ReceiverRef { get; set; }
        public string ShipToAddress { get; set; }
        public string TransferComment { get; set; }
        public string Status { get; set; }

        public int OriginatorId { get; set; }

        public string ManifestoInfo { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime? ApprovalDate { get; set; }

        public IList<CautionDTO> Cautions { get; set; }
        public IList<MaterialTransferAssetDTO> Assets { get; set; }
        public IList<MaterialTransferApprovalDTO> ApprovingUsers { get; set; }
        public IList<MaterialTransferNotifyUserDTO> NotifiedUsers { get; set; }
        public IList<MaterialTransferCountryManagerDTO> CountryManagers { get; set; }

        public string OriginatorName { get; set; }
        public bool IsOwned { get; set; }
        public bool IsApprover { get; set; }
        public bool IsCancellable { get; set; }
        public bool IsShipReady { get; set; }
        public bool IsForApproval { get; set; }
        public int CurrentUserId { get; set; }
        
        private readonly AssetContext db = new AssetContext();

        public EditMaterialTransferPageViewModel(){}

        public EditMaterialTransferPageViewModel(MaterialTransfer materialTransfer,int currentuserid)
        {
            CurrentUserId = currentuserid;
            MaterialTransferId = materialTransfer.MaterialTransferId;
            TransferTypeId = materialTransfer.TransferTypeId;
            TransferReasonId = materialTransfer.TransferReasonId;
            MaterialTransferNumber  = materialTransfer.MaterialTransferNumber;
            ManifestNumber = materialTransfer.ManifestNumber;
            FromCountryId = materialTransfer.FromCountryId;
            CurrentLocationId  = materialTransfer.CurrentLocationId;
            AFENumber  = materialTransfer.AFENumber;
            Transporter = materialTransfer.Transporter;
            ToCountryId = materialTransfer.ToCountryId;
            NewLocationId = materialTransfer.NewLocationId;
            RepairWorkOrder = materialTransfer.RepairWorkOrder;
            TruckNumber = materialTransfer.TruckNumber;
            AttnReceiver = materialTransfer.AttnReceiver;
            ReceiverId  = materialTransfer.ReceiverId;
            ReceiverRef = materialTransfer.ReceiverRef;
            TransferComment = materialTransfer.TransferComment;
            Status = materialTransfer.Status == Enum.GetName(typeof(TransferStatusEnum),TransferStatusEnum.Transit) ? "In Transit" : materialTransfer.Status;
            OriginatorId = materialTransfer.OriginatorId;
            ManifestoInfo = materialTransfer.ManifestoInfo;
            ApprovalStatus = materialTransfer.ApprovalStatus;
            ApprovalDate = materialTransfer.ApprovalDate;
            ShipToAddress = materialTransfer.ShipToAddress;
            Cautions = materialTransfer.Cautions.Select(x => new CautionDTO
                                                                 {
                                                                     CautionId = x.CautionId,
                                                                     CautionName = x.CautionName
                                                                 }).ToList();
            Assets = materialTransfer.TransferAssets.Select(x => new MaterialTransferAssetDTO
                                                                     {
                                                                         MTA_AssetId = x.AssetId,
                                                                         MTA_AssetUniqueId = x.Asset != null ? x.Asset.AssetUniqueId : string.Empty,
                                                                         MaterialTransferAssetId = x.MaterialTransferAssetId,
                                                                         MaterialTransferId = x.MaterialTransferId,
                                                                         NonAssetDescription = x.NonAssetDescription,
                                                                         NonAssetQuantity = x.NonAssetQuantity,
                                                                         IsShipped = x.IsShipped,
                                                                         ETADays = x.ETADays,
                                                                         ConditionEvaluationId = x.ConditionEvaluationId ?? 0
                                                                     }).ToList();

            //ApprovingUsers = materialTransfer.MaterialTransferApprovals.Select(x => new MaterialTransferApprovalDTO
            //                                                                            {
            //                                                                                MaterialTransferApprovalId =
            //                                                                                    x.
            //                                                                                    MaterialTransferApprovalId,
            //                                                                                ApprovalDateTime = x.ApprovalDateTime,
            //                                                                                ApprovalStatus = x.ApprovalStatus,
            //                                                                                MaterialTransferId = x.MaterialTransferId,
            //                                                                                ApprovingUser = new UserDTO
            //                                                                                                    {
            //                                                                                                        UserId = x.ApprovingUser.UserId,
            //                                                                                                        FullName = x.ApprovingUser.User.FirstName + " " + x.ApprovingUser.User.LastName,
            //                                                                                                        Position = x.ApprovingUser.User.Position
            //                                                                                                    }

            //                                                                            }).ToList();
            //NotifiedUsers = db.NotificationUsers.ToList().Where(x => x.CountryId == FromCountryId
            //                                                && x.TransferTypeId == TransferTypeId && x.TransferReasons.Any(y=>y.TransferReasonId == TransferReasonId)).Select(
            //                                                    y => new MaterialTransferNotifyUserDTO
            //                                                             {
            //                                                                 NotifiedUser = new UserDTO
            //                                                                                    {
            //                                                                                        FullName =
            //                                                                                            y.User != null
            //                                                                                                ? string.
            //                                                                                                      Format
            //                                                                                                      ("{0} {1}",
            //                                                                                                       y.
            //                                                                                                           User
            //                                                                                                           .
            //                                                                                                           FirstName,
            //                                                                                                       y.
            //                                                                                                           User
            //                                                                                                           .
            //                                                                                                           LastName)
            //                                                                                                : string.
            //                                                                                                      Empty,
            //                                                                                        Position =
            //                                                                                            y.User != null
            //                                                                                                ? y.User.
            //                                                                                                      Position
            //                                                                                                : string.
            //                                                                                                      Empty
            //                                                                                    }
            //                                                             }).ToList();
            
            //if (Assets.Count == 0) Assets.Add(new MaterialTransferAssetDTO());
            
            IsOwned = materialTransfer.OriginatorId == currentuserid;
            IsApprover = materialTransfer.MaterialTransferApprovals.Any(x => x.ApprovingUser.UserId == currentuserid)
                && materialTransfer.Status == Enum.GetName(typeof(TransferStatusEnum),TransferStatusEnum.TransferRequested);

            IsCancellable = IsOwned &&
                            (materialTransfer.Status ==
                             Enum.GetName(typeof (TransferStatusEnum), TransferStatusEnum.Disapproved)
                             ||
                             materialTransfer.Status ==
                             Enum.GetName(typeof (TransferStatusEnum), TransferStatusEnum.Draft)
                              ||
                             materialTransfer.Status ==
                             Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.TransferRequested)
                             ||
                             materialTransfer.Status ==
                             Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Approved)
                            );
            IsForApproval = IsOwned && Status == "Draft" && Assets.Count > 0;
            IsShipReady = IsOwned && Status == Enum.GetName(typeof (TransferStatusEnum), TransferStatusEnum.Approved);
        }

        public void UpdateModel(MaterialTransfer materialTransfer)
        {
            materialTransfer.MaterialTransferId = MaterialTransferId;
            materialTransfer.TransferTypeId = TransferTypeId;
            materialTransfer.TransferReasonId = TransferReasonId;
            materialTransfer.MaterialTransferNumber = MaterialTransferNumber;
           
            //materialTransfer.FromCountryId = FromCountryId;
            //materialTransfer.CurrentLocationId = CurrentLocationId;
            materialTransfer.AFENumber = AFENumber;
          
            //materialTransfer.ToCountryId = ToCountryId;
            //fail safe since "to country is" disabled
            //if (materialTransfer.TransferTypeId == 1)
            //    materialTransfer.ToCountryId = materialTransfer.FromCountryId;
            //materialTransfer.NewLocationId = NewLocationId;
            materialTransfer.RepairWorkOrder = RepairWorkOrder;
       
            materialTransfer.AttnReceiver = AttnReceiver;
            materialTransfer.ReceiverId = ReceiverId;
            materialTransfer.ReceiverRef = ReceiverRef;

            materialTransfer.TransferComment = TransferComment;
            //materialTransfer.Status = Status;
            //materialTransfer.OriginatorId = OriginatorId;
            materialTransfer.ManifestoInfo = ManifestoInfo;
            //materialTransfer.ApprovalStatus = ApprovalStatus;
            //materialTransfer.ApprovalDate = ApprovalDate;
          
        }

        public void UpdateShippingModel(MaterialTransfer  materialTransfer)
        {
            materialTransfer.ManifestNumber = ManifestNumber;
            materialTransfer.Transporter = Transporter;
            materialTransfer.TruckNumber = TruckNumber;
            materialTransfer.TransferComment = TransferComment;
            materialTransfer.ManifestoInfo = ManifestoInfo;
            materialTransfer.ShipToAddress = ShipToAddress;

        }

    }
}