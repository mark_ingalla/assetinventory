﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.DTO;
using AssetsInventory.Models;

namespace AssetsInventory.ViewModels
{
    public class MaterialTransferReceivePageViewModel
    {
        private readonly AssetContext db = new AssetContext();

        public int MaterialTransferId { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }

        public IList<MaterialTransferAssetDTO> Assets { get; set; }

        public MaterialTransferReceivePageViewModel(){}

        public MaterialTransferReceivePageViewModel(MaterialTransfer materialTransfer)
        {
            MaterialTransferId = materialTransfer.MaterialTransferId;
            Assets = (from p in materialTransfer.TransferAssets.ToList()
                         join q in db.Assets.ToList() on p.AssetId equals q.AssetId into shipments
                         from q in shipments.DefaultIfEmpty()
                         where p.IsShipped
                         select new MaterialTransferAssetDTO
                                    {
                                        MaterialTransferAssetId = p.MaterialTransferAssetId,
                                        MTA_AssetUniqueId = q != null ? q.AssetUniqueId : p.MaterialTransferAssetId.ToString(),
                                        NonAssetDescription =
                                            string.IsNullOrWhiteSpace(p.NonAssetDescription)
                                                ? (q != null ? q.Description : string.Empty)
                                                : p.NonAssetDescription,
                                        NonAssetQuantity = q != null ? 1 : 0
                                    }).ToList();
            //Assets = materialTransfer.TransferAssets.Where(x=>x.IsShipped).Select(x => new MaterialTransferAssetDTO
            //                                                         {
            //                                                             MaterialTransferAssetId = x.MaterialTransferAssetId,
            //                                                             NonAssetDescription = x.NonAssetDescription,
            //                                                             NonAssetQuantity = x.NonAssetQuantity
            //                                                         }).ToList();
        }
    }
}