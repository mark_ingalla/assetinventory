﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AssetsInventory.Models;
using AssetsInventory.Service;
using AssetsInventory.Utilities;

namespace AssetsInventory.ViewModels
{
    public class UserEditPageViewModel
    {
        public int UserId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; } 
        [Required][Email]
        public string Email { get; set; }
        [Required]
        public int UserTypeId { get; set; }
        [Required]
        public int? LanguageId { get; set; }
        [Required]
        public string Position { get; set; }

        public UserEditPageViewModel(){}

        public UserEditPageViewModel(User model)
        {
            UserId = model.UserId;
            UserName = model.UserName;
            FirstName = model.FirstName;
            LastName = model.LastName;
            Email = model.Email;
            UserTypeId = model.UserTypeId;
            LanguageId = model.LanguageId;
            Position = model.Position;
        }

        public void UpdateModel(User model)
        {
            model.UserId = UserId;
            model.UserName = UserName;
            model.FirstName = FirstName;
            model.LastName = LastName;
            model.Email = Email;
            model.UserTypeId = UserTypeId;
            model.LanguageId = LanguageId;
            model.Position = Position;

            var hashpassword = EncryptService.Decrypt(model.Password, true);
            if (model.Password == hashpassword)
                model.Password = EncryptService.Encrypt(model.Password, true);
        }
    }
}