﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.Models;

namespace AssetsInventory.ViewModels
{
    public class SystemDisplayPageViewModel
    {
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string  CreatedBy { get; set; }
        public string  ModifiedBy { get; set; }

        private readonly AssetContext db = new AssetContext();

        public SystemDisplayPageViewModel()
        {
            
        }

        public SystemDisplayPageViewModel(BaseModel model)
        {
            CreatedOn = model.DateAdded;
            ModifiedOn = model.DateModified;
            if (model.AddedById.HasValue)
            {
                var createdby = db.Users.Find(model.AddedById);
                CreatedBy = string.Format("{0} {1}", createdby.FirstName, createdby.LastName);
            }
            if (model.ModifiedById.HasValue)
            {
                var modifiedby = db.Users.Find(model.ModifiedById);
                ModifiedBy = string.Format("{0} {1}", modifiedby.FirstName, modifiedby.LastName);
            }
        }
    }
}