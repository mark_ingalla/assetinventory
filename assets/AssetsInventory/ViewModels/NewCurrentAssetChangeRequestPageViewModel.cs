﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;

namespace AssetsInventory.ViewModels
{
    public class NewCurrentAssetChangeRequestPageViewModel : NewAssetChangeRequestPageViewModel
    {
        public string ModuleName { get; set; }
        public string Url { get; set; }
        public int AssetId { get; set; }
        public IEnumerable<AttachmentDTO> Attachments { get; set; }

        
    }

    public class EditAssetChangeRequestPageViewModel : NewCurrentAssetChangeRequestPageViewModel
    {
        public int RequestorId { get; set; }
        public IEnumerable<SelectListItem> Statuses { get; set; }
        public string SelectedStatus { get; set; }
    }
}