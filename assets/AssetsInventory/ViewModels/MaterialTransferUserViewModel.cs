﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.DTO;

namespace AssetsInventory.ViewModels
{
    public class MaterialTransferUserViewModel
    {

        public IEnumerable<MaterialTransferApprovalDTO> Approval { get; set; }
        public IEnumerable<MaterialTransferNotifyUserDTO> NotifyUser { get; set; }
        public int MaterialTransferApprovalId { get; set; }
    }
}