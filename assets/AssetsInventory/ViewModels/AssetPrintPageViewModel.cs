﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.DTO;
using AssetsInventory.Lookups;
using AssetsInventory.Models;

namespace AssetsInventory.ViewModels
{
    public class AssetPrintPageViewModel
    {
        public int AssetId { get; set; }
        public string AssetUniqueId { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string ManufacturerName { get; set; }
        public string ModelTypeName { get; set; }

        public string SerialNumber { get; set; }
        public string ArrangementNumber { get; set; }
        public string LocationName { get; set; }

        public int MaintenanceRequestID { get; set; }
        public string MaterialTransferNumber { get; set; }
        public string ConditionEvalDescription { get; set; }
        public decimal? AssetPurchasePrice { get; set; }
        public decimal? NetbookValue { get; set; }
        public decimal? FairMarketPrice { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public bool IsPart { get; set; }


        public int? ShippingInfo { get; set; }
        public double OperatingWeight { get; set; }
        public string WeightUOM { get; set; }
        public double DryWeight { get; set; }
        public string DryWeightUOM { get; set; }
        public double Height { get; set; }
        public string HeightUOM { get; set; }
        public double Width { get; set; }
        public string WidthUOM { get; set; }
        public double Diameter { get; set; }
        public string DiameterUOM { get; set; }

        public int? PurchaseInfoId { get; set; }
        public string AFENumber { get; set; }
        public string MaterialReqNumber { get; set; }
        public string PONumber { get; set; }
        public string Vendor { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal? PurchasePrice { get; set; }
        public string PurchaseCurrency { get; set; }
        public DateTime? PurchaseDate { get; set; }

        public IList<DocumentDTO> Documents { get; set; }
        public IList<PhotoAssetDTO> Photos { get; set; }

        public IEnumerable<MaterialTransferDTO> MaterialTransfers { get; set; }

        private readonly AssetContext db = new AssetContext();

        public AssetPrintPageViewModel()
        {
            
        }

        public AssetPrintPageViewModel(Asset asset)
        {
            AssetId = asset.AssetId;
            AssetUniqueId = asset.AssetUniqueId;
            Description = asset.Description;
            CategoryName = asset.Category.CategoryName;
            SubCategoryName = asset.SubCategory.SubCategoryName;
            ManufacturerName = asset.Manufacturer.ManufacturerName;
            ModelTypeName = asset.ModelType.ModelTypeName;
            SerialNumber = asset.SerialNumber;
            ArrangementNumber = asset.ArrangementNumber;
            LocationName = asset.Location.LocationName;
            Comment = asset.Comment;
            IsPart = asset.IsPart;
            switch (asset.Status)
            {
                case "Transit":
                    Status = "in Transit";
                    break;
                case "InPlace":
                case  "Received":
                    Status = "in Place";
                    break;
                default:
                    Status = asset.Status;
                    break;
            }
            ConditionEvalDescription = asset.ConditionEvaluation.ConditionEvalDescription;
            AssetPurchasePrice = asset.AssetPurchasePrice;
            FairMarketPrice = asset.FairMarketPrice;
            NetbookValue = asset.NetbookValue;
            ShippingInfo = asset.ShippingInfoID;

            if (asset.ShippingInfoID.HasValue)
            {
                var shippinginfo = (from shipinfo in db.ShippingInfos
                                   join weight in db.WeightUOMs on shipinfo.WeightUOMId equals weight.WeightUOMId into
                                       operatingweight
                                   from weight in operatingweight.DefaultIfEmpty()
                                   join dryweight in db.WeightUOMs on shipinfo.DryWeightUOMId equals
                                       dryweight.WeightUOMId into
                                       dryoperatingweight
                                   from dryweight in dryoperatingweight.DefaultIfEmpty()
                                   join height in db.DimensionUOMs on shipinfo.HeightUOMId equals height.DimensionUOMId
                                       into
                                       newheight
                                   from height in newheight.DefaultIfEmpty()
                                   join width in db.DimensionUOMs on shipinfo.WidthUOMId equals width.DimensionUOMId
                                       into
                                       newwidth
                                   from width in newwidth.DefaultIfEmpty()
                                   join diamter in db.DimensionUOMs on shipinfo.DiameterUOMId equals
                                       diamter.DimensionUOMId into
                                       newdiamter
                                   from diamter in newdiamter.DefaultIfEmpty()
                                   where shipinfo.ShippingInfoId == asset.ShippingInfoID
                                   select new { shipinfo,weight,dryweight,width,height,diamter}).FirstOrDefault();

                OperatingWeight = shippinginfo.shipinfo.OperatingWeight ?? 0;
                WeightUOM = shippinginfo.weight != null ? shippinginfo.weight.WeightUOMDescription : string.Empty;
                DryWeight = shippinginfo.shipinfo.DryWeight ?? 0;
                DryWeightUOM = shippinginfo.dryweight != null ? shippinginfo.dryweight.WeightUOMDescription : string.Empty;
                Width = shippinginfo.shipinfo.Width ?? 0;
                WidthUOM = shippinginfo.width != null ? shippinginfo.width.DimesionDescription : string.Empty;
                Height = shippinginfo.shipinfo.Height ?? 0;
                HeightUOM = shippinginfo.height != null ? shippinginfo.height.DimesionDescription : string.Empty;
                Diameter = shippinginfo.shipinfo.Diameter ?? 0;
                DiameterUOM = shippinginfo.diamter != null ? shippinginfo.diamter.DimesionDescription : string.Empty;

            }

            if (asset.PurchaseInfoID.HasValue)
            {
                var purchaseinfo = db.PurchaseInfos.Find(asset.PurchaseInfoID.Value);
                AFENumber = purchaseinfo.AFENumber;
                MaterialReqNumber = purchaseinfo.MaterialReqNumber;
                PONumber = purchaseinfo.PONumber;
                Vendor = purchaseinfo.Vendor;
                InvoiceNumber = purchaseinfo.InvoiceNumber;
                PurchasePrice = purchaseinfo.PurchasePrice;
                PurchaseCurrency = purchaseinfo.PurchaseCurrency != null ? purchaseinfo.PurchaseCurrency.PurhcaseCurrencySymbol: string.Empty;
                PurchaseDate = purchaseinfo.PurchaseDate;

            }

            MaterialTransfers = db.MaterialTransferAssets.Where(x=>x.AssetId == AssetId).Select(x => new MaterialTransferDTO
                                                                     {
                                                                         MaterialTransferNumber =
                                                                             x.MaterialTransfer.MaterialTransferNumber,
                                                                         DateApproved = x.MaterialTransfer.ApprovalDate,
                                                                         Status = x.MaterialTransfer.Status
                                                                     }).Distinct();

            Documents = asset.Documents.Select(x => new DocumentDTO
                                                        {
                                                            DocumentName = x.DocumentName,
                                                            DocumentDescription = x.DocumentDescription
                                                        }).ToList();

            Photos = asset.PhotoArchives.Select(x => new PhotoAssetDTO
                                                         {
                                                             PhotoFileName = x.PhotoFileName
                                                         }).ToList();

        }
    }
}