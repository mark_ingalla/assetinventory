﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;

namespace AssetsInventory.ViewModels
{
    public class NewAssetChangeRequestPageViewModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public int SelectedAdmin { get; set; }

        public IList<SelectListItem> Admins { get; set; }
    }
}