﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.ViewModels
{
    public class ManufacturerDropdownViewModel
    {
        public int? ManufacturerId { get; set; }
    }
}