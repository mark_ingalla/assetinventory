﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetsInventory.DTO;

namespace AssetsInventory.ViewModels
{
    public class ResourceEditPageViewModel
    {

        public string ResourceFileName { get; set; }
        public string LanguagePrefix { get; set; }
        public IList<ResourceDTO> Resources { get; set; }

        public ResourceEditPageViewModel(){}

        public ResourceEditPageViewModel(Dictionary<string,string> list)
        {
            Resources = list.Select(x => new ResourceDTO
                                             {
                                                 Key = x.Key,
                                                 Value = x.Value
                                             }).ToList();


        }


    }
}