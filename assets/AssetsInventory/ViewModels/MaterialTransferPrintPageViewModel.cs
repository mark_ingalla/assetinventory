﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;

namespace AssetsInventory.ViewModels
{
    public class MaterialTransferPrintPageViewModel
    {
        public IEnumerable<SelectListItem> TransferTypes { get; set; }
        public IEnumerable<SelectListItem> TransferReasons { get; set; }
        public IEnumerable<SelectListItem> Cautions { get; set; }

        public string MaterialTransferNumber { get; set; }
        public string MtfDate { get; set; }
        public string MaterialRequestNumber { get; set; }
        public string AfeReference { get; set; }
        public string RepairWorkOrder { get; set; }
        public string ShippingComments { get; set; }
        public string FromCountry { get; set; }
        public string ToCountry { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public string AttnReceiver { get; set; }
        public string ReceiverRef { get; set; }


        public string ShipToAddress { get; set; }
        public string ReceiverPhoneNo { get; set; }
        public string TransferComments { get; set; }
        public string Originator { get; set; }
        
        public string Status { get; set; }

        public IEnumerable<AssetDTO> Assets { get; set; }
        public IEnumerable<UserDTO> Approvers { get; set; }
        public IEnumerable<MaterialTransferApprovalDTO> Approvals { get; set; }

        public MaterialTransferPrintPageViewModel()
        {
        }

        //public MaterialTransferPrintPageViewModel(MaterialTransfer materialTransfer)
        //{
        //    MaterialTransferNumber = materialTransfer.MaterialTransferNumber;
        //    MtfDate = materialTransfer.DateAdded.HasValue ? materialTransfer.DateAdded.ToString() : string.Empty;
        //    AfeReference = materialTransfer.AFENumber;
        //    Originator = string.Format("{0} {1}", materialTransfer.Originator.FirstName,
        //                               materialTransfer.Originator.LastName);
        //    Status = materialTransfer.Status;
        //    ShipToAddress = materialTransfer.ShipToAddress;
        //    ShippingComments = materialTransfer.TransferComment;
        //    FromCountry = materialTransfer.FromCountry.CountryName;
        //    ToCountry = materialTransfer.ToCountry.CountryName;
        //    FromLocation = materialTransfer.CurrentLocation.LocationName;
        //    ToLocation = materialTransfer.NewLocation.LocationName;
        //    RepairWorkOrder = materialTransfer.RepairWorkOrder;


        //}


    }
}