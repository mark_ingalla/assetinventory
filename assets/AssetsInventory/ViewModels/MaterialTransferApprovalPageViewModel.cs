﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.ViewModels
{
    public class MaterialTransferApprovalPageViewModel
    {
        public int UserId { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
    }
}