﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.ViewModels
{
    public class LocationDropdownViewModel
    {
        public int? LocationID { get; set; }
        public string ControlName { get; set; }
        public int? CountryId { get; set; }
        public int? SelectedId { get; set; }

        public LocationDropdownViewModel()
        {
            if (string.IsNullOrWhiteSpace(ControlName))
                ControlName = "LocationID";
        }

        public LocationDropdownViewModel(string controlname)
        {
            ControlName = controlname;
            if (string.IsNullOrWhiteSpace(ControlName))
                ControlName = "LocationID";
        }

    }
}