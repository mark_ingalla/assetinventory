﻿using System;
using AssetsInventory.Models;

namespace AssetsInventory.ViewModels
{
    public class LogonPartialPageViewModel
    {
        

        public bool HasNoUser { get; set; }
        public string DisplayName { get; set; }
        public string Position { get; set; }
        
        public LogonPartialPageViewModel()
        {
           
        }
    }
}