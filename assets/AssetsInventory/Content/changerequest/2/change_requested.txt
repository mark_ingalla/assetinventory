how to request changes.

a regular user can not change assets and fields.

this can be done by submitting a change request to the admin.

an admin will have an extra tabs called Messages

there will be two types of changes, new records to be added, or changes to current records.

on the asset listing page, a regular user will have instead of Creat Asset button, a Request a new asset.

on this page, the user will have the option to add a title, message, attachments (multiple) and select which admin they want to send this to.

for available records, a regular user can request changes.

when viewing a record there should be a link at the bottom that say request a change. <-translatable buttons as well

when requesting a change, the same fields are also given with an additional the name of the module and link to the record where change was requested

module and record link to the record that we are asking to change.


when the admin receives the request under messages here is how the requests will be listed.


user name | date requested | module | status
---------------------------------------------


status could be Change Requested and Change Made

after the admin makes the change, he can come and change the status to Change Made

at that point the change requester will get notified