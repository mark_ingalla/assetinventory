﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class MaterialTransferAssetDTO
    {
        public int MaterialTransferAssetId { get; set; }
        public int MaterialTransferId { get; set; }
        public int?  MTA_AssetId { get; set; }
        public string MTA_AssetUniqueId { get; set; }
        public string NonAssetDescription { get; set; }
        public int NonAssetQuantity { get; set; }
        public int RowId { get; set; }
        public bool IsShipped { get; set; }
        public int ETADays { get; set; }
        public int ConditionEvaluationId { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
    }
}