﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class LookupDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}