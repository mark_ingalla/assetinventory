﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class AttachmentDTO
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}