﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class ApprovingUserDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string TransferType { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }
    }
}