﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class AssetDTO
    {
        public int AssetId { get; set; }
        public string AssetUniqueId { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string SerialNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public string ModelType { get; set; }
        public string Condition { get; set; }
    }
}