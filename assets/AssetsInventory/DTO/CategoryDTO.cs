﻿namespace AssetsInventory.DTO
{
    public class CategoryDTO
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Prefix { get; set; }
    }
}