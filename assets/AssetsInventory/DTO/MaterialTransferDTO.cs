﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class MaterialTransferDTO
    {
        public int MaterialTransferId { get; set; }
        public string MaterialTransferNumber { get; set; }
        public string TransferTypeName { get; set; }
        public string FromCountry { get; set; }
        public string ToCountry { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateApproved { get; set; }
        public string Status { get; set; }
        public string Initiator { get; set; }
        public string AFENumber { get; set; }
        
    }
}