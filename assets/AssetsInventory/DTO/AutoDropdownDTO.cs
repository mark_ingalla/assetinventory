﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class AutoDropdownDTO
    {
        public int id { get; set; }
        public string value { get; set; }
    }
}