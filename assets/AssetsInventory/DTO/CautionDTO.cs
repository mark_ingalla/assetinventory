﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class CautionDTO
    {
        public int CautionId { get; set; }
        public string CautionName { get; set; }
    }
}