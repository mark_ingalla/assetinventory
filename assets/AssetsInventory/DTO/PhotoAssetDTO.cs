﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class PhotoAssetDTO
    {
        public int PhotoArchiveId { get; set; }
        public DateTime DateUploaded { get; set; }
        public string PhotoFileName { get; set; }
        public string UploadedBy { get; set; }

        public string ImageUrl
        {
            get { return string.Format("~/Content/asset-photo/{0}", PhotoFileName); }
        }
    }
}