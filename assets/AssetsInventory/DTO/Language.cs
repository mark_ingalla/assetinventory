﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class Language
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}