﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class ChangeRequestDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string DateRequested { get; set; }
        public string Module { get; set; }
        public string Status { get; set; }

    }
}