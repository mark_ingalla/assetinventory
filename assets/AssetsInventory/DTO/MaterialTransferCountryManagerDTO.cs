﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class MaterialTransferCountryManagerDTO
    {
        public int CountryManagerId { get; set; }
        public int UserId { get; set; }
        public int MaterialTransferId { get; set; }

        public string FullName { get; set; }
    }
}