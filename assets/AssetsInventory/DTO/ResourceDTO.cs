﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class ResourceDTO
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}