﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.DTO
{
    public class MaterialTransferNotifyUserDTO
    {
        public int MaterialTransferApprovalId { get; set; }
        public int MaterialTransferId { get; set; }
        //public int ApprovingUserId { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime? ApprovalDateTime { get; set; }
        public UserDTO NotifiedUser { get; set; }
    }
}