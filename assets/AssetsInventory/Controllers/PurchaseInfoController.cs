﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class PurchaseInfoController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /PurchaseInfo/

        public ViewResult Index()
        {
            var purchaseinfos = db.PurchaseInfos.Include(p => p.PurchaseCurrency);
            return View(purchaseinfos.ToList());
        }

        //
        // GET: /PurchaseInfo/Details/5

        public ViewResult Details(int id)
        {
            PurchaseInfo purchaseinfo = db.PurchaseInfos.Find(id);
            return View(purchaseinfo);
        }

        //
        // GET: /PurchaseInfo/Create

        public ActionResult Create()
        {
            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol");
            return View();
        } 

        //
        // POST: /PurchaseInfo/Create

        [HttpPost]
        public ActionResult Create(PurchaseInfo purchaseinfo)
        {
            if (ModelState.IsValid)
            {
                db.PurchaseInfos.Add(purchaseinfo);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol", purchaseinfo.PurchaseCurrencyId);
            return View(purchaseinfo);
        }
        
        //
        // GET: /PurchaseInfo/Edit/5
 
        public ActionResult Edit(int id)
        {
            ViewData["AssetId"] = TempData["AssetId"];
            PurchaseInfo purchaseinfo = db.PurchaseInfos.Find(id);
            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol", purchaseinfo.PurchaseCurrencyId);
            return View(purchaseinfo);
        }

        //
        // POST: /PurchaseInfo/Edit/5

        [HttpPost]
        public ActionResult Edit(int id,PurchaseInfo purchaseinfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchaseinfo).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("Edit", "Asset", new { id = id });
            }
            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol", purchaseinfo.PurchaseCurrencyId);
            return View(purchaseinfo);
        }

        //
        // GET: /PurchaseInfo/Delete/5
 
        public ActionResult Delete(int id)
        {
            PurchaseInfo purchaseinfo = db.PurchaseInfos.Find(id);
            return View(purchaseinfo);
        }

        //
        // POST: /PurchaseInfo/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            PurchaseInfo purchaseinfo = db.PurchaseInfos.Find(id);
            db.PurchaseInfos.Remove(purchaseinfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}