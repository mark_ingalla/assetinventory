﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{ 
    public class ManufacturerController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /Manufacturer/

        public ViewResult Index()
        {
            return View(db.Manufacturers.ToList());
        }

        public JsonResult List()
        {
            var data = db.Manufacturers.ToList().Select(x => new ManufacturerDTO
                                                                 {
                                                                     Code = x.ManufacturerCode,
                                                                     Id = x.ManufacturerId,
                                                                     Name =x.ManufacturerName,
                                                                     Ref = x.OtherManufacturerRef
                                                                 });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Manufacturer/Details/5

        public ViewResult Details(int id)
        {
            Manufacturer manufacturer = db.Manufacturers.Find(id);
            return View(manufacturer);
        }

        //
        // GET: /Manufacturer/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Manufacturer/Create

        [HttpPost]
        public ActionResult Create(Manufacturer manufacturer)
        {
            if (ModelState.IsValid)
            {
                db.Manufacturers.Add(manufacturer);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(manufacturer);
        }
        
        //
        // GET: /Manufacturer/Edit/5
 
        public ActionResult Edit(int id)
        {
            Manufacturer manufacturer = db.Manufacturers.Find(id);
            return View(manufacturer);
        }

        //
        // POST: /Manufacturer/Edit/5

        [HttpPost]
        public ActionResult Edit(Manufacturer manufacturer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manufacturer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(manufacturer);
        }

        //
        // GET: /Manufacturer/Delete/5
 
        public ActionResult Delete(int id)
        {
            Manufacturer manufacturer = db.Manufacturers.Find(id);
            return View(manufacturer);
        }

        //
        // POST: /Manufacturer/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Manufacturer manufacturer = db.Manufacturers.Find(id);
            db.Manufacturers.Remove(manufacturer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult ManufacturerDropdown(int? id)
        {
         
            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName",id.HasValue ? id.ToString() : string.Empty);
            var model = new ManufacturerDropdownViewModel();
            return PartialView("ManufacturerDropdown", model);
        }

        public ActionResult AssetManufacturerDropdown(int catid,int? id)
        {

            ViewBag.ManufacturerId = new SelectList(CategoryManufacturerList(catid), "Id", "Name", id.HasValue ? id.ToString() : string.Empty);
            var model = new ManufacturerDropdownViewModel();
            return PartialView("ManufacturerDropdown", model);
        }


        private IList<ManufacturerDTO> CategoryManufacturerList(int id)
        {
            var assetmanufacturer = from manufacturer in db.Manufacturers.ToList()
                                    join asset in db.Assets on manufacturer.ManufacturerId equals asset.ManufacturerId
                                    where asset.CategoryId == id
                                    orderby manufacturer.ManufacturerName
                                    select manufacturer;
            var splitter = new List<Manufacturer>
                               {
                                   new Manufacturer
                                       {
                                           ManufacturerName = "----",
                                           ManufacturerId = 0,
                                           ManufacturerCode = "---"
                                       }
                               };

            var othermanufacturer = from manufacturer in db.Manufacturers.ToList()
                                    where !assetmanufacturer.Contains(manufacturer)
                                    orderby manufacturer.ManufacturerName
                                    select manufacturer;
            var data = assetmanufacturer.Union(splitter).Union(othermanufacturer)
                .Select(x => new ManufacturerDTO
                {
                    Code = x.ManufacturerCode,
                    Id = x.ManufacturerId,
                    Name = x.ManufacturerName
                });

            return data.ToList();
        }

        /// <summary>
        /// json for manufacturer list with category filter
        /// </summary>
        /// <param name="id">category id</param>
        /// <returns></returns>
        public JsonResult ManufacturerList(int id)
        {

            return Json(CategoryManufacturerList(id), JsonRequestBehavior.AllowGet);

        }
    }
}