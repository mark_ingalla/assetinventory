﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class ApprovingUserController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /ApprovingUser/

        public ViewResult Index()
        {
            var approvingusers = db.ApprovingUsers.Include(a => a.User).Include(a => a.TransferType).Include(a => a.Country);
            return View(approvingusers.ToList());
        }

        public JsonResult  List()
        {
            var data = db.ApprovingUsers.Include(a => a.User).Include(a => a.TransferType).Include(a => a.Country);

            var model = data.ToList().Select(x => new ApprovingUserDTO
                                                      {
                                                          Id = x.ApprovingUserId,
                                                          Country = x.Country.CountryName,
                                                          Name = x.User.FirstName + " " + x.User.LastName,
                                                          TransferType = x.TransferType.TransferTypeName,
                                                          UserName = x.User.UserName
                                                      });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /ApprovingUser/Details/5

        public ViewResult Details(int id)
        {
            ApprovingUser approvinguser = db.ApprovingUsers.Find(id);
            return View(approvinguser);
        }

        //
        // GET: /ApprovingUser/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users.Select(x => new UserDTO { UserId = x.UserId, FullName = x.FirstName + " " + x.LastName }), "UserId", "FullName",1);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName",1);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", 1);
            ViewBag.TransferReasonIds = GetReasons(new ApprovingUser(), db.TransferReasons);
            return View();
        }

        private IEnumerable<SelectListItem> GetReasons(ApprovingUser approvingUser, IEnumerable<TransferReason> reasons)
        {
            if (approvingUser.TransferReasons != null)
                return reasons.Select(reason => new SelectListItem
                {
                    Text = reason.TransferReasonName,
                    Value = reason.TransferReasonId.ToString(),
                    Selected = approvingUser.TransferReasons.Contains(reason)
                }).ToList();
            return reasons.Select(reason => new SelectListItem
            {
                Text = reason.TransferReasonName,
                Value = reason.TransferReasonId.ToString()
            }).ToList();
        }
        //
        // POST: /ApprovingUser/Create

        [HttpPost]
        public ActionResult Create(ApprovingUser approvinguser, IEnumerable<int> transferReasonIds)
        {
            if (ModelState.IsValid)
            {
                if (approvinguser.TransferReasons == null) approvinguser.TransferReasons = new List<TransferReason>();

                if (transferReasonIds != null) SetReasons(approvinguser, transferReasonIds.ToArray());
                db.ApprovingUsers.Add(approvinguser);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.UserId = new SelectList(db.Users.Select(x => new UserDTO { UserId = x.UserId, FullName = x.FirstName + " " + x.LastName }), "UserId", "FullName", approvinguser.UserId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", approvinguser.TransferTypeId);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", approvinguser.CountryId);
            ViewBag.TransferReasonIds = GetReasons(approvinguser, db.TransferReasons);
            return View(approvinguser);
        }

        private void SetReasons(ApprovingUser approvingUser, int[] cautionIds)
        {
            var selectedTags = GetReasonById(cautionIds);

            var cautions = approvingUser.TransferReasons.ToList();
            foreach (var caution in cautions)
                if (!selectedTags.Contains(caution))
                    approvingUser.TransferReasons.Remove(caution);

            foreach (var tag in selectedTags)
                if (!cautions.Contains(tag))
                    approvingUser.TransferReasons.Add(tag);
        }

        private IList<TransferReason> GetReasonById(int[] reasons)
        {
            var selectedreasons = new List<TransferReason>();
            foreach (var reasonid in reasons)
            {
                var dbcaution = db.TransferReasons.Single(x => x.TransferReasonId == reasonid);
                selectedreasons.Add(dbcaution);
            }
            return selectedreasons;
        }
        //
        // GET: /ApprovingUser/Edit/5
 
        public ActionResult Edit(int id)
        {
            ApprovingUser approvinguser = db.ApprovingUsers.Find(id);
            ViewBag.UserId = new SelectList(db.Users.Select(x=>new UserDTO{UserId = x.UserId,FullName = x.FirstName + " " + x.LastName}), "UserId", "FullName", approvinguser.UserId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", approvinguser.TransferTypeId);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", approvinguser.CountryId);
            ViewBag.TransferReasonIds = GetReasons(approvinguser, db.TransferReasons);
            return View(approvinguser);
        }

        //
        // POST: /ApprovingUser/Edit/5

        [HttpPost]
        public ActionResult Edit(ApprovingUser approvinguser, IEnumerable<int> transferReasonIds)
        {
            var approvinguserentity = db.ApprovingUsers.Find(approvinguser.ApprovingUserId);
            if (ModelState.IsValid)
            {
                approvinguserentity.TransferTypeId = approvinguser.TransferTypeId;
                approvinguserentity.UserId = approvinguser.UserId;
                approvinguserentity.CountryId = approvinguser.CountryId;
                

                if (approvinguserentity.TransferReasons == null) approvinguserentity.TransferReasons = new List<TransferReason>();

                

                //db.Entry(approvinguser).State = EntityState.Modified;
                if (transferReasonIds != null)
                    SetReasons(approvinguserentity, transferReasonIds.ToArray());
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users.Select(x => new UserDTO { UserId = x.UserId, FullName = x.FirstName + " " + x.LastName }), "UserId", "FullName", approvinguser.UserId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", approvinguser.TransferTypeId);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", approvinguser.CountryId);
            ViewBag.TransferReasonIds = GetReasons(approvinguser, db.TransferReasons);
            return View(approvinguser);
        }

        //
        // GET: /ApprovingUser/Delete/5
 
        public ActionResult Delete(int id)
        {
            ApprovingUser approvinguser = db.ApprovingUsers.Find(id);
            return View(approvinguser);
        }

        //
        // POST: /ApprovingUser/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            ApprovingUser approvinguser = db.ApprovingUsers.Find(id);


            var relatedapprovinguser = from p in db.MaterialTransferApprovals
                                       where p.ApprovingUserId == id
                                       select p;
            try
            {
                if (relatedapprovinguser.Count() > 0)
                    throw new Exception("Cannot Delete");
            }
            catch (Exception)
            {
                
                throw;
            }
           
            db.ApprovingUsers.Remove(approvinguser);
            db.SaveChanges();


            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}