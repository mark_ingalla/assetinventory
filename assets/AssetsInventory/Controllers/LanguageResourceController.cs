﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using AssetsInventory.DTO;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{
    public class LanguageResourceController : BaseAdminController
    {
        //
        // GET: /LanguageResource/

        public ActionResult Index()
        {
            var model = new ResourceIndexPageViewModel();
            return View(model);
        }

        public ActionResult Edit(string id)
        {
            return View();
        }

        [ActionName("Common")]
        public ActionResult CommonIndex(string id)
        {
            ViewBag.FormAction = "Common";
            var resourcefile = string.IsNullOrWhiteSpace(id) || id == "en" ? string.Empty : "." + id;

            var filename = Request.PhysicalApplicationPath +
                "Resource\\Common" + resourcefile + ".resx";
            var slist = LoadResources(filename);
            var model = new ResourceEditPageViewModel(slist)
                            {
                                ResourceFileName = "Common" + resourcefile + ".resx",
                                LanguagePrefix = id
                            };
            return View("Edit",model);
        }

        [ActionName("Common")]
        [HttpPost]
        public ActionResult CommonIndex(FormCollection formCollection)
        {
            var filename = formCollection["ResourceFileName"];
            
            filename = Request.PhysicalApplicationPath + "Resource\\" + filename;
            SaveResources(filename,formCollection);
            
           
            return RedirectToAction("Common","LanguageResource",new { id = formCollection["LanguagePrefix"]}) ;
        }

        [ActionName("Asset")]
        public ActionResult AssetIndex(string id)
        {
            ViewBag.FormAction = "Asset";
            var resourcefile = string.IsNullOrWhiteSpace(id) || id == "en" ? string.Empty : "." + id;

            var filename = Request.PhysicalApplicationPath +
                "Resource\\AssetResource" + resourcefile + ".resx";
            var slist = LoadResources(filename);
            var model = new ResourceEditPageViewModel(slist)
            {
                ResourceFileName = "AssetResource" + resourcefile + ".resx",
                LanguagePrefix = id
            };
            return View("Edit", model);
        }

        [ActionName("Asset")]
        [HttpPost]
        public ActionResult AssetIndex(FormCollection formCollection)
        {
            var filename = formCollection["ResourceFileName"];

            filename = Request.PhysicalApplicationPath + "Resource\\" + filename;
            SaveResources(filename, formCollection);


            return RedirectToAction("Asset", "LanguageResource", new { id = formCollection["LanguagePrefix"] });
        }

        [ActionName("MaterialTransfer")]
        public ActionResult MaterialTransferIndex(string id)
        {
            ViewBag.FormAction = "MaterialTransfer";
            var resourcefile = string.IsNullOrWhiteSpace(id) || id == "en" ? string.Empty : "." + id;

            var filename = Request.PhysicalApplicationPath +
                "Resource\\MaterialTransferResource" + resourcefile + ".resx";
            var slist = LoadResources(filename);
            var model = new ResourceEditPageViewModel(slist)
            {
                ResourceFileName = "MaterialTransferResource" + resourcefile + ".resx",
                LanguagePrefix = id
            };
            return View("Edit", model);
        }

        [ActionName("MaterialTransfer")]
        [HttpPost]
        public ActionResult MaterialTransferIndex(FormCollection formCollection)
        {
            var filename = formCollection["ResourceFileName"];

            filename = Request.PhysicalApplicationPath + "Resource\\" + filename;
            SaveResources(filename, formCollection);


            return RedirectToAction("MaterialTransfer", "LanguageResource", new { id = formCollection["LanguagePrefix"] });
        }


        [ActionName("EmailTemplates")]
        public ActionResult NewUserIndex(string id)
        {
            ViewBag.FormAction = "EmailTemplates";
            var resourcefile = string.IsNullOrWhiteSpace(id) || id == "en" ? string.Empty : "." + id;

            var filename = Request.PhysicalApplicationPath +
                "Resource\\MailTemplate" + resourcefile + ".resx";
            var slist = LoadResources(filename);
            var model = new ResourceEditPageViewModel(slist)
            {
                ResourceFileName = "MailTemplate" + resourcefile + ".resx",
                LanguagePrefix = id
            };
            return View("Mail", model);
        }

        [ActionName("EmailTemplates")]
        [HttpPost,ValidateInput(false)]
        public ActionResult NewUserIndex(FormCollection formCollection)
        {
            var filename = formCollection["ResourceFileName"];

            filename = Request.PhysicalApplicationPath + "Resource\\" + filename;
            SaveResources(filename, formCollection);


            return RedirectToAction("EmailTemplates", "LanguageResource", new { id = formCollection["LanguagePrefix"] });
        }

        public ActionResult RenderLanguageSelect(string id,string action)
        {
            
            var languagelist = new List<Language>
                                       {
                                           new Language {Code = "en", Name = "English"},
                                           new Language {Code = "fr", Name = "Francais"},
                                           new Language {Code = "es", Name = "Espanyol"},
                                           new Language {Code = "pt-Br", Name = "Portuguese"},
                                       };
            ViewBag.LanguageSelect = new SelectList(languagelist, "Code", "Name", id);
            return PartialView("LanguageSelector",action);
        }

        private void SaveResources(string filename,FormCollection formCollection)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);
            XmlNodeList nlist = xmlDoc.GetElementsByTagName("data");


            foreach (XmlNode childnode in nlist)
            {
                if (childnode.Attributes == null) continue;
                childnode.Attributes["xml:space"].Value = "default";
                xmlDoc.Save(filename);
                var lastnode = childnode.SelectSingleNode("value");
                if (childnode.Attributes != null && lastnode != null)
                    lastnode.InnerText = formCollection[childnode.Attributes["name"].Value];
                xmlDoc.Save(filename);
            }
        }

        private Dictionary<string, string> LoadResources(string filename)
        {
            Stream stream = new FileStream(filename, FileMode.Open,
                FileAccess.Read, FileShare.Read);
            var RrX = new ResXResourceReader(stream);
            var RrEn = RrX.GetEnumerator();
            //var slist = new SortedList();
            var slist = new Dictionary<string, string>();
            while (RrEn.MoveNext())
            {
                //slist.Add(RrEn.Key, RrEn.Value);
                slist.Add(RrEn.Key.ToString(), RrEn.Value.ToString());
            }

            RrX.Close();
            stream.Dispose();

            return slist;
        }

    }
}
