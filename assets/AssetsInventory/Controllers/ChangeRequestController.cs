﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Lookups;
using AssetsInventory.Models;
using AssetsInventory.Service;
using AssetsInventory.ViewModels;
using AssetsInventory.Utilities;

namespace AssetsInventory.Controllers
{
    public class ChangeRequestController : BaseAdminController
    {

        private readonly AssetContext db = new AssetContext();
        //
        // GET: /ChangeRequest/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            var currentadmin = db.Users.First(x => x.UserName == User.Identity.Name);
            var changerequests = db.ChangeRequests.Include(x => x.Admin).Include(x => x.Requestor).ToList()
                .Where(x => x.AdminId == currentadmin.UserId && x.Status == Enum.GetName(typeof(ChangeRequestStatus),ChangeRequestStatus.ChangeRequested));
            var data = from x in changerequests 
                       join users in db.Users on x.RequestorId equals users.UserId
                        select   new ChangeRequestDTO
                        {
                            DateRequested = x.DateRequested.HasValue ? x.DateRequested.ToString() : string.Empty ,
                            Id = x.Id,
                            Module = x.Module,
                            Status = x.Status,
                            UserName = users.UserName

                        };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ViewResult Change(int id)
        {
            var changerequest = db.ChangeRequests.Find(id);
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);

            if (changerequest.AdminId != currentuser.UserId) return View("Unauthorized");

            var model = new EditAssetChangeRequestPageViewModel()
                            {
                                Admins =
                                    db.Users.Where(x => x.UserTypeId == (int)UserTypeEnum.Admin).ToList().Select(
                                        x => new SelectListItem
                                                 {
                                                     Value = x.UserId.ToString(),
                                                     Text =
                                                         string.Format("{0} {1}", x.FirstName,
                                                                       x.LastName),
                                                     Selected = x.UserId == changerequest.AdminId
                                                 }).ToList(),
                                AssetId = changerequest.Id,
                                Message = changerequest.Message,
                                ModuleName = changerequest.Module,
                                SelectedAdmin = changerequest.AdminId,
                                Title = changerequest.Title,
                                Url = changerequest.Link,
                                RequestorId = changerequest.RequestorId,
                                Statuses = new SelectList(ChangeRequestStatusLookup.ChangeRequestsStatusLookup())
                                ,Attachments = changerequest.Attachments.Select(x=>new AttachmentDTO
                                                                                       {
                                                                                           FilePath = Url.Content("~/Content/changerequest/" + x.Id.ToString()+ "/" +  x.Name),
                                                                                           FileName = x.Name
                                                                                       })
                            };

            return View(model);

        }

        [HttpPost]
        public ActionResult Change(int id , EditAssetChangeRequestPageViewModel model)
        {
            var changemadestatus = Enum.GetName(typeof (ChangeRequestStatus), ChangeRequestStatus.ChangeMade);
            if (model.SelectedStatus == changemadestatus)
            {
                var changerequest = db.ChangeRequests.Find(id);
                changerequest.Status = changemadestatus;
                db.SaveChanges();

                //notify email
                var requestor = db.Users.Find(changerequest.RequestorId);
                var notification = new ChangesMadeEmail
                                       {
                                           UserEmail = requestor.Email,
                                           Culture = new CultureInfo(requestor.Language.LanguageShortName)
                                       };
                notification.Send();

                return RedirectToAction("Messages");
            }
            model.Statuses = new SelectList(ChangeRequestStatusLookup.ChangeRequestsStatusLookup());
            return View(model);
        }

        public ActionResult Messages()
        {
            return View();
        }

        public ViewResult NewChangeRequest()
        {
            var model = new NewAssetChangeRequestPageViewModel
                            {
                                Admins = db.Users.Where(x=>x.UserTypeId == (int)UserTypeEnum.Admin).ToList().Select(x => new SelectListItem
                                                                           {
                                                                               Value = x.UserId.ToString(),
                                                                               Text =
                                                                                   string.Format("{0} {1}", x.FirstName,
                                                                                                 x.LastName)
                                                                           }).ToList()
                            };

            return View(model);
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult NewChangeRequest(NewAssetChangeRequestPageViewModel model, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                var changerequest = new ChangeRequest
                                        {
                                            AdminId = model.SelectedAdmin,
                                            Title = model.Title,
                                            Message = model.Message,
                                            DateRequested = DateTime.Now,
                                            ChangeType = (int)ChangeTypeEnum.New,
                                            RequestorId = db.Users.FirstOrDefault(x=>x.UserName == User.Identity.Name).UserId,
                                            Module ="Asset",
                                            Status = Enum.GetName(typeof(ChangeRequestStatus), ChangeRequestStatus.ChangeRequested)
                                        };

                
                db.ChangeRequests.Add(changerequest);
                db.SaveChanges();

                var url = Url.AbsoluteAction("Change", "ChangeRequest", new { id = changerequest.Id });
                var adminuser = db.Users.Find(changerequest.AdminId);
                //notify email
                var notify = new ChangeRequestNotifyEmail()
                {
                    AppUrl = "<a href='" + url + "' >here</a>",
                    Culture =
                        new CultureInfo(adminuser.Language != null
                                            ? adminuser.Language.LanguageShortName
                                            : "en"),
                    UserEmail = adminuser.Email
                };
                notify.Send();

                if (attachments != null && attachments.Count() > 0)
                {
                    var savepath = Path.Combine(Server.MapPath("~/Content/changerequest/"),
                                                changerequest.Id.ToString());
                    if (!Directory.Exists(savepath))
                        Directory.CreateDirectory(savepath);

                    if (changerequest.Attachments == null) changerequest.Attachments = new List<ChangeRequestAttachment>();

                    foreach (var file in attachments)
                    {
                        if (file.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(file.FileName);
                            var path = Path.Combine(savepath, fileName);
                            file.SaveAs(path);
                            changerequest.Attachments.Add(new ChangeRequestAttachment {Name = fileName});
                        }
                    }
                    db.SaveChanges();

                   


                }
                return RedirectToAction("Index", "Asset");
            }
            model.Admins =
                db.Users.Where(x => x.UserTypeId == (int) UserTypeEnum.Admin).ToList().Select(x => new SelectListItem
                                                                                                       {
                                                                                                           Value =
                                                                                                               x.UserId.
                                                                                                               ToString(),
                                                                                                           Text =
                                                                                                               string.
                                                                                                               Format(
                                                                                                                   "{0} {1}",
                                                                                                                   x.
                                                                                                                       FirstName,
                                                                                                                   x.
                                                                                                                       LastName),
                                                                                                           Selected = (x.UserId == model.SelectedAdmin)
                                                                                                       }).ToList();
            return View(model);
        }

        public ViewResult NewCurrentChangeRequest(int id)
        {
            var model = new NewCurrentAssetChangeRequestPageViewModel()
            {
                Admins = db.Users.Where(x => x.UserTypeId == (int)UserTypeEnum.Admin).ToList().Select(x => new SelectListItem
                {
                    Value = x.UserId.ToString(),
                    Text =
                        string.Format("{0} {1}", x.FirstName,
                                      x.LastName)
                }).ToList(),
                ModuleName = "Asset",
                AssetId = id,
                Url = Url.AbsoluteAction("Edit", "Asset", new { id = id})
            };

            return View(model);
        }

        [HttpPost,ValidateInput(false)]
        public ActionResult NewCurrentChangeRequest(NewCurrentAssetChangeRequestPageViewModel model, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                var changerequest = new ChangeRequest
                {
                    AdminId = model.SelectedAdmin,
                    Title = model.Title,
                    Message = model.Message,
                    DateRequested = DateTime.Now,
                    ChangeType = (int)ChangeTypeEnum.Change,
                    RequestorId = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).UserId,
                    Module = "Asset",
                    Link = Url.AbsoluteAction("Edit","Asset",new { id = model.AssetId}),
                    Status = Enum.GetName(typeof(ChangeRequestStatus), ChangeRequestStatus.ChangeRequested)

                };


                db.ChangeRequests.Add(changerequest);
                db.SaveChanges();

                var url = Url.AbsoluteAction("Change", "ChangeRequest", new { id = changerequest.Id });
                var adminuser = db.Users.Find(changerequest.AdminId);
                //notify email
                var notify = new ChangeRequestNotifyEmail()
                {
                    AppUrl = "<a href='" + url + "' >here</a>",
                    Culture =
                        new CultureInfo(adminuser.Language != null
                                            ? adminuser.Language.LanguageShortName
                                            : "en"),
                    UserEmail = adminuser.Email
                };
                notify.Send();

                if (attachments != null && attachments.Count() > 0)
                {
                    var savepath = Path.Combine(Server.MapPath("~/Content/changerequest/"),
                                                changerequest.Id.ToString());
                    if (!Directory.Exists(savepath))
                        Directory.CreateDirectory(savepath);

                    if (changerequest.Attachments == null) changerequest.Attachments = new List<ChangeRequestAttachment>();

                    foreach (var file in attachments)
                    {
                        if (file.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(file.FileName);
                            var path = Path.Combine(savepath, fileName);
                            file.SaveAs(path);
                            changerequest.Attachments.Add(new ChangeRequestAttachment { Name = fileName });
                        }
                    }
                    db.SaveChanges();

                    
                }
                return RedirectToAction("Index", "Asset");
            }
            model.Admins = db.Users.Where(x => x.UserTypeId == (int)UserTypeEnum.Admin).ToList().Select(x => new SelectListItem
            {
                Value = x.UserId.ToString(),
                Text =
                    string.Format("{0} {1}", x.FirstName,
                                  x.LastName),
                Selected = (x.UserId == model.SelectedAdmin)
            }).ToList();
            return View(model);
        }


        //public ContentResult MessageCount()
        //{
        //    var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
        //    var query = from p in db.ChangeRequests
        //                where p.AdminId == currentuser.UserId
        //                && p.Status == Enum.GetName(typeof(ChangeTypeEnum),ChangeTypeEnum.New)
        //                select p;
        //    return Content(query.Count().ToString());


        //}

        public ActionResult RenderMessageTab()
        {
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            var query = from p in db.ChangeRequests.ToList()
                        where p.AdminId == currentuser.UserId
                        && p.Status == Enum.GetName(typeof(ChangeRequestStatus), ChangeRequestStatus.ChangeRequested)
                        select p;
            var model = new MessageTabViewModel
                            {
                                MessageCount = query.Count()
                            };
            return PartialView("MessageTab",model);
        }

        public class MessageTabViewModel
        {
            public int MessageCount { get; set; }
        }
    }
}
