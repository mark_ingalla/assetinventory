﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class MaterialTransferAssetController : Controller
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /MaterialTransferAsset/

        public ViewResult Index()
        {
            var materialtransferassets = db.MaterialTransferAssets.Include(m => m.MaterialTransfer).Include(m => m.Asset);
            return View(materialtransferassets.ToList());
        }

        //
        // GET: /MaterialTransferAsset/Details/5

        public ViewResult Details(int id)
        {
            MaterialTransferAsset materialtransferasset = db.MaterialTransferAssets.Find(id);
            return View(materialtransferasset);
        }

        //
        // GET: /MaterialTransferAsset/Create

        public ActionResult Create()
        {
            ViewBag.MaterialTransferId = new SelectList(db.MaterialTransfers, "MaterialTransferId", "MaterialTransferNumber");
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId");
            return View();
        } 

        //
        // POST: /MaterialTransferAsset/Create

        [HttpPost]
        public ActionResult Create(MaterialTransferAsset materialtransferasset)
        {
            if (ModelState.IsValid)
            {
                db.MaterialTransferAssets.Add(materialtransferasset);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.MaterialTransferId = new SelectList(db.MaterialTransfers, "MaterialTransferId", "MaterialTransferNumber", materialtransferasset.MaterialTransferId);
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId", materialtransferasset.AssetId);
            return View(materialtransferasset);
        }
        
        //
        // GET: /MaterialTransferAsset/Edit/5
 
        public ActionResult Edit(int id)
        {
            MaterialTransferAsset materialtransferasset = db.MaterialTransferAssets.Find(id);
            ViewBag.MaterialTransferId = new SelectList(db.MaterialTransfers, "MaterialTransferId", "MaterialTransferNumber", materialtransferasset.MaterialTransferId);
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId", materialtransferasset.AssetId);
            return View(materialtransferasset);
        }

        //
        // POST: /MaterialTransferAsset/Edit/5

        [HttpPost]
        public ActionResult Edit(MaterialTransferAsset materialtransferasset)
        {
            if (ModelState.IsValid)
            {
                db.Entry(materialtransferasset).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaterialTransferId = new SelectList(db.MaterialTransfers, "MaterialTransferId", "MaterialTransferNumber", materialtransferasset.MaterialTransferId);
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId", materialtransferasset.AssetId);
            return View(materialtransferasset);
        }

        //
        // GET: /MaterialTransferAsset/Delete/5
 
        public ActionResult Delete(int id)
        {
            MaterialTransferAsset materialtransferasset = db.MaterialTransferAssets.Find(id);
            return View(materialtransferasset);
        }

        //
        // POST: /MaterialTransferAsset/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            MaterialTransferAsset materialtransferasset = db.MaterialTransferAssets.Find(id);
            db.MaterialTransferAssets.Remove(materialtransferasset);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}