﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{ 
    public class LocationController : BaseAdminController
    {
        private readonly AssetContext db = new AssetContext();

        //
        // GET: /Location/

        public ViewResult Index()
        {
            return View(db.Locations.ToList());
        }

        //
        // GET: /Location/Details/5

        public ViewResult Details(int id)
        {
            Location location = db.Locations.Find(id);
            return View(location);
        }

        //
        // GET: /Location/Create
        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            //handle error for delete referential
            //if (filterContext.RouteData.Values["action"].ToString() == "Delete")
            //{
            //    filterContext.ExceptionHandled = true;

            //    var returnurl = filterContext.HttpContext.Request.RawUrl;
            //    //TempData["ErrorMsg"] = filterContext.Exception.InnerException.InnerException.Message;
            //    TempData["ErrorMsg"] = "Cannot Delete this Record Since It is being referenced by another Record";
            //    TempData["DeleteUrl"] = returnurl;
            //    //RedirectToAction("DeleteHandler");
            //    filterContext.Result = View("DeleteHandler");
            //}
            if (filterContext.RouteData.Values["action"].ToString() == "Edit" ||
                filterContext.RouteData.Values["action"].ToString() == "Create")
            {
                filterContext.ExceptionHandled = true;

                var returnurl = filterContext.HttpContext.Request.RawUrl;
                //TempData["ErrorMsg"] = filterContext.Exception.InnerException.InnerException.Message;
                TempData["ErrorMsg"] = "Cannot Update this Record. Location ID aready exists";
                TempData["DeleteUrl"] = returnurl;
                //RedirectToAction("DeleteHandler");
                filterContext.Result = Redirect(returnurl);
            }
        }

        public ActionResult Create()
        {
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x => x.SortCode).ThenBy(x => x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", 1);
            return View();
        } 

        //
        // POST: /Location/Create

        [HttpPost]
        public ActionResult Create(Location location)
        {
            if (ModelState.IsValid)
            {
                db.Locations.Add(location);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", location.CountryId);
            return View(location);
        }
        
        //
        // GET: /Location/Edit/5
 
        public ActionResult Edit(int id)
        {
            var location = db.Locations.Find(id);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", location.CountryId);
            return View(location);
        }

        //
        // POST: /Location/Edit/5

        [HttpPost]
        public ActionResult Edit(Location location)
        {
            if (ModelState.IsValid)
            {
                db.Entry(location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", location.CountryId);
            return View(location);
        }

        //
        // GET: /Location/Delete/5
 
        public ActionResult Delete(int id)
        {
            Location location = db.Locations.Find(id);
            return View(location);
        }

        //
        // POST: /Location/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult LocationDropdown(int? id,int? countryid,string controlname)
        {
            IList<LocationDTO> locations;
            if (!countryid.HasValue)
            {
                locations = db.Locations.ToList().Select(x => new LocationDTO
                                                             {
                                                                 Id = x.LocationId,
                                                                 Name = x.LocationName + " - " + x.CostCenterCode
                                                             }).ToList();
            }
            else
            {
                locations = db.Locations.Where(x=>x.CountryId == countryid).ToList().Select(x => new LocationDTO
                {
                    Id = x.LocationId,
                    Name = x.LocationName + " - " + x.CostCenterCode
                }).ToList();
            }
            var defaultvalue = locations.FirstOrDefault();
           // ViewBag.LocationID = new SelectList(locations, "Id", "Name",id.HasValue ? id.ToString() : defaultvalue != null ? defaultvalue.Id.ToString() : string.Empty);
            var model = new LocationDropdownViewModel(controlname);
            model.CountryId = countryid;
            model.SelectedId = id;
            return PartialView("LocationDropdown", model);
        }


        public JsonResult LocationDropdownList(int? id, int? countryid)
        {
            IList<LocationDTO> locations;
            if (!countryid.HasValue)
            {
                locations = db.Locations.ToList().Select(x => new LocationDTO
                {
                    Id = x.LocationId,
                    Name = x.LocationName + " - " + x.CostCenterCode
                }).ToList();
            }
            else
            {
                locations = db.Locations.Where(x => x.CountryId == countryid).ToList().Select(x => new LocationDTO
                {
                    Id = x.LocationId,
                    Name = x.LocationName + " - " + x.CostCenterCode
                }).ToList();
            }
            var defaultvalue = locations.FirstOrDefault();
            return
                Json(
                    new SelectList(locations, "Id", "Name",
                                   id.HasValue
                                       ? id.ToString()
                                       : defaultvalue != null ? defaultvalue.Id.ToString() : string.Empty),
                    JsonRequestBehavior.AllowGet);

        }

        public ActionResult CreateDialog()
        {
            return PartialView("CreateDialog");
        }

        [HttpPost]
        public ActionResult CreateDialog(Location location)
        {
            if (ModelState.IsValid)
            {
                db.Locations.Add(location);
                db.SaveChanges();
                
            }
            return LocationDropdown(null,null,string.Empty);
            
        }
    }
}