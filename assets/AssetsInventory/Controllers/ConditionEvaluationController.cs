﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{
    public class ConditionEvaluationController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /ConditionEvaluation/

        public ViewResult Index()
        {
            return View(db.ConditionEvaluations.ToList());
        }

        //
        // GET: /ConditionEvaluation/Details/5

        public ViewResult Details(int id)
        {
            ConditionEvaluation conditionevaluation = db.ConditionEvaluations.Find(id);
            return View(conditionevaluation);
        }

        //
        // GET: /ConditionEvaluation/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /ConditionEvaluation/Create

        [HttpPost]
        public ActionResult Create(ConditionEvaluation conditionevaluation)
        {
            if (ModelState.IsValid)
            {
                db.ConditionEvaluations.Add(conditionevaluation);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(conditionevaluation);
        }
        
        //
        // GET: /ConditionEvaluation/Edit/5
 
        public ActionResult Edit(int id)
        {
            ConditionEvaluation conditionevaluation = db.ConditionEvaluations.Find(id);
            return View(conditionevaluation);
        }

        //
        // POST: /ConditionEvaluation/Edit/5

        [HttpPost]
        public ActionResult Edit(ConditionEvaluation conditionevaluation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(conditionevaluation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(conditionevaluation);
        }

        //
        // GET: /ConditionEvaluation/Delete/5
 
        public ActionResult Delete(int id)
        {
            ConditionEvaluation conditionevaluation = db.ConditionEvaluations.Find(id);
            return View(conditionevaluation);
        }

        //
        // POST: /ConditionEvaluation/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            ConditionEvaluation conditionevaluation = db.ConditionEvaluations.Find(id);
            db.ConditionEvaluations.Remove(conditionevaluation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult ConditionEvaluationDropdown(int? id)
        {
            ViewBag.ConditionEvaluationID = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription",id.HasValue ? id.ToString() : "1");
            var model = new ConditionEvaluationDropdownViewModel();
            return PartialView("ConditionEvaluationDropdown", model);
        }
    }
}