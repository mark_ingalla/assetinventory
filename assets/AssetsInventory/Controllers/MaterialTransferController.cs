﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AssetResource;
using AssetsInventory.DTO;
using AssetsInventory.Lookups;
using AssetsInventory.Models;
using AssetsInventory.Service;
using AssetsInventory.Utilities;
using AssetsInventory.ViewModels;
using Caution = AssetsInventory.Models.Caution;

namespace AssetsInventory.Controllers
{ 

    public class MaterialTransferController : BaseController
    {
        private AssetContext db = new AssetContext();
        private AssetWorkflowService _workflow = new AssetWorkflowService();

        //
        // GET: /MaterialTransfer/

        public ViewResult Index()
        {
            //var materialtransfers = db.MaterialTransfers
            //    //.Include(m => m.FromCountry)
            //    //.Include(m => m.ToCountry)
            //    .Include(m => m.TransferReason)
            //    .Include(m => m.TransferType);
            var materialtransfers =
                db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType).ToList();
            var query = from mt in materialtransfers
                join fromcountry in db.Countries on mt.FromCountryId equals fromcountry.CountryId
                join tocountry in db.Countries on mt.ToCountryId equals tocountry.CountryId
                select new MaterialTransferDTO
                           {
                               MaterialTransferId = mt.MaterialTransferId,
                               MaterialTransferNumber = mt.MaterialTransferNumber,
                               TransferTypeName = mt.TransferType.TransferTypeName,
                               FromCountry = fromcountry.CountryName,
                               ToCountry = tocountry.CountryName,
                               DateAdded = mt.DateAdded,
                               Status = mt.Status
                           };
            
            return View(query.ToList());
        }

        [HttpPost]
        public ActionResult Index(string pagesize)
        {

            TempData["pagesize"] = pagesize;
            return RedirectToAction("Index");
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            base.OnActionExecuted(filterContext);
            if (filterContext.HttpContext.Request.HttpMethod == "GET" &&
                filterContext.RouteData.Values["action"].ToString() == "Index")
            {
                ViewData["pagesize"] = TempData["pagesize"] ?? 15;
            }
        }
        public JsonResult List()
        {
            var materialtransfers =
                db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType).Include(m=>m.Originator).ToList();
            var query = from mt in materialtransfers
                        join fromcountry in db.Countries on mt.FromCountryId equals fromcountry.CountryId
                        join tocountry in db.Countries on mt.ToCountryId equals tocountry.CountryId
                        join users in db.Users on mt.OriginatorId equals users.UserId
                        select new MaterialTransferDTO
                        {
                            MaterialTransferId = mt.MaterialTransferId,
                            MaterialTransferNumber = mt.MaterialTransferNumber,
                            TransferTypeName = mt.TransferType.TransferTypeName,
                            FromCountry = fromcountry.CountryName,
                            ToCountry = tocountry.CountryName,
                            DateAdded = mt.DateAdded,
                            Status = mt.Status == Enum.GetName(typeof(TransferStatusEnum),TransferStatusEnum.Transit) ? "In Transit" : mt.Status,
                            DateApproved =  mt.ApprovalDate,
                            Initiator = users.FirstName + " " + users.LastName,
                            AFENumber = mt.AFENumber
                            //Initiator = mt.Originator.UserName
                        };

            return Json(query,JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /MaterialTransfer/Details/5

        public ViewResult Details(int id)
        {
            MaterialTransfer materialtransfer = db.MaterialTransfers.Find(id);
            return View(materialtransfer);
        }

        public ViewResult Print(int id)
        {
            var materialtransfers =
                db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType)
                .Include(m => m.CurrentLocation).Include(m=>m.NewLocation).Include(m=>m.Originator)
                .ToList();
            var query = from mt in materialtransfers
                        join fromcountry in db.Countries on mt.FromCountryId equals fromcountry.CountryId
                        join tocountry in db.Countries on mt.ToCountryId equals tocountry.CountryId
                        join fromloc in db.Locations on mt.CurrentLocationId equals fromloc.LocationId
                        join toloc in db.Locations on mt.NewLocationId equals toloc.LocationId
                        join originator in db.Users on mt.OriginatorId equals originator.UserId
                        join receiver in db.Users on mt.OriginatorId equals receiver.UserId
                        where mt.MaterialTransferId == id
                        select new MaterialTransferPrintPageViewModel
                                   {
                                       AfeReference = mt.AFENumber,
                                       AttnReceiver = string.Format("{0} {1}", receiver.FirstName, receiver.LastName),
                                       FromCountry = fromcountry.CountryName,
                                       ToCountry = tocountry.CountryName,
                                       Cautions = GetCautions(mt, db.Cautions),
                                       TransferReasons = GetTransferReason(mt, db.TransferReasons),
                                       TransferTypes = GetTransferType(mt, db.TransferTypes),
                                       Assets = (from p in mt.TransferAssets.ToList()
                                                 join q in db.Assets on p.AssetId equals q.AssetId
                                                 join cat in db.Categories on q.CategoryId equals cat.CategoryId
                                                 join subcat in db.SubCategories on q.SubCategoryId equals subcat.SubCategoryId
                                                 join manufacturer in db.Manufacturers on q.ManufacturerId equals manufacturer.ManufacturerId
                                                 join modeltype in db.ModelTypes on q.ModelTypeID equals modeltype.ModelTypeId
                                                 join condition in db.ConditionEvaluations on q.ConditionEvaluationID equals condition.ConditionEvaluationId
                                                 select new AssetDTO
                                                            {
                                                                AssetId = p.AssetId ?? 0,
                                                                AssetUniqueId = q.AssetUniqueId,
                                                                Category = cat.CategoryName,
                                                                SubCategory = subcat.SubCategoryName,
                                                                SerialNumber = q.SerialNumber,
                                                                Manufacturer = manufacturer.ManufacturerName,
                                                                ModelType = modeltype.ModelTypeName,
                                                                Condition = condition.ConditionEvalDescription,
                                                                Status = q.Status
                                                            }),
                                       MaterialTransferNumber = mt.MaterialTransferNumber,
                                       MtfDate = mt.DateAdded.HasValue ? mt.DateAdded.ToString() : string.Empty,
                                       FromLocation = fromloc.LocationName,
                                       ToLocation = toloc.LocationName,
                                       ReceiverRef = mt.ReceiverRef,
                                       ShippingComments = mt.TransferComment,
                                       ShipToAddress = mt.ShipToAddress,
                                       Originator = string.Format("{0} {1}", originator.FirstName, originator.LastName),
                                       Status = mt.Status,
                                       Approvals = from approvers in mt.MaterialTransferApprovals
                                                   join approverusers in db.Users on approvers.ApprovingUser.UserId equals approverusers.UserId
                                                   select new MaterialTransferApprovalDTO
                                                              {
                                                                  ApprovalDateTime = approvers.ApprovalDateTime,
                                                                  ApprovalStatus = approvers.ApprovalStatus,
                                                                  ApprovingUser = new UserDTO
                                                                                      {
                                                                                          FirstName = approverusers.FirstName,
                                                                                          LastName = approverusers.LastName,
                                                                                          Position = approvers.ApprovalDateTime.HasValue ? approvers.ApprovalDateTime.ToString() : string.Empty
                                                                                      }
                                                              }
                                       //Approvers = from approvers in mt.MaterialTransferApprovals
                                       //            join approverusers in db.Users on approvers.ApprovingUser.UserId equals approverusers.UserId
                                       //            select new UserDTO
                                       //                       {
                                       //                           FirstName = approverusers.FirstName,
                                       //                           LastName = approverusers.LastName,
                                       //                           Position = approvers.ApprovalDateTime.HasValue ? approvers.ApprovalDateTime.ToString() : string.Empty
                                       //                       }

                                   };
                    
            return View(query.FirstOrDefault());
            
        }

        //
        // GET: /MaterialTransfer/Create

        public ActionResult Create()
        {
            ViewBag.TransferReasonId = new SelectList(db.TransferReasons, "TransferReasonId", "TransferReasonName",1);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName",1);
            ViewBag.FromCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName");
            ViewBag.CurrentLocationId = new SelectList(db.Locations, "LocationId", "LocationName",1);
            ViewBag.ToCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName");
            ViewBag.NewLocationId = new SelectList(db.Locations, "LocationId", "LocationName",1);
            ViewBag.ReceiverId = new SelectList(GetUsers(), "UserId", "FullName",1);
            ViewBag.Status = new SelectList(TransferStatusLookup.StatusLookup(),0);
            ViewBag.CautionIds = GetCautions(new MaterialTransfer(), db.Cautions);
            return View();
        } 

        //
        // POST: /MaterialTransfer/Create

        [HttpPost]
        public ActionResult Create(MaterialTransfer materialtransfer, IEnumerable<int> cautionIds, IEnumerable<int> countrymgrs)
        {
            var transferreason = db.TransferReasons.Find(materialtransfer.TransferReasonId);
            if (materialtransfer.TransferReasonId == 1 && materialtransfer.CurrentLocationId == materialtransfer.NewLocationId)
                ModelState.AddModelError("TransferReasonId", "Invalid Locations for Country-Country");
            //fail safe since "to country is" disabled
            if (materialtransfer.TransferTypeId == 1)
                materialtransfer.ToCountryId = materialtransfer.FromCountryId;
            var approvers = db.ApprovingUsers.ToList().Where(x => x.TransferTypeId == materialtransfer.TransferTypeId
                && x.CountryId == materialtransfer.FromCountryId && x.TransferReasons.Contains(transferreason)).Count();
            if (approvers <= 0)
                ModelState.AddModelError("",Common.NoApprovingUserErrorMessage);

            if (ModelState.IsValid)
            {
                //fill data
                
                    var fromcountrycode = db.Countries.Find(materialtransfer.FromCountryId).CountryCode;
                    var currentuser = db.Users.FirstOrDefault(x => x.UserName == this.User.Identity.Name);
                    materialtransfer.MaterialTransferNumber = string.Format("{0}-{1}", fromcountrycode,
                                                                            GetUniqueNumber(materialtransfer.FromCountryId));
                    materialtransfer.OriginatorId = currentuser.UserId;
                
                //get cautions
           
                if (materialtransfer.Cautions == null) materialtransfer.Cautions = new List<Caution>();

                if (cautionIds != null) SetCautions(materialtransfer,cautionIds.ToArray());
               

                //get approving users
                //var reason = db.TransferReasons.FirstOrDefault(x => x.TransferReasonName == "Country-Country");
                //var transfertype = db.TransferTypes.FirstOrDefault(x => x.TransferTypeName == "InCountry");
                
                if (materialtransfer.MaterialTransferApprovals == null)
                    materialtransfer.MaterialTransferApprovals = new List<MaterialTransferApproval>();
                //if (materialtransfer.TransferTypeId == transfertype.TransferTypeId)
                //{
                var approvingusers = db.ApprovingUsers.ToList().Where(x => x.CountryId == materialtransfer.FromCountryId 
                        && x.TransferTypeId == materialtransfer.TransferTypeId
                        && x.TransferReasons.Contains(transferreason));
                    foreach (var approvingUser in
                        approvingusers.Where(approvingUser => !materialtransfer.MaterialTransferApprovals.Any(x => x.ApprovingUserId == approvingUser.ApprovingUserId)))
                    {
                        materialtransfer.MaterialTransferApprovals.Add(new MaterialTransferApproval
                        {
                            ApprovingUserId =
                                approvingUser.ApprovingUserId
                        });
                    }
                //}
                //else//country manager block
                //{
                //    foreach (var countrymgr in countrymgrs)
                //    {
                //        var approvingcountrymgr =
                //            db.ApprovingUsers.FirstOrDefault(x => x.TransferTypeId == materialtransfer.TransferTypeId
                //                                                  && x.CountryId == materialtransfer.FromCountryId &&
                //                                                  x.UserId == countrymgr);
                //        materialtransfer.MaterialTransferApprovals.Add(new MaterialTransferApproval
                //                                                           {
                //                                                               ApprovingUserId =  approvingcountrymgr.ApprovingUserId

                //                                                           });
                //    }
                //}//end ountry manager block

                //default status
                materialtransfer.Status = Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Draft);
                db.MaterialTransfers.Add(materialtransfer);
                
                db.SaveChanges();

                //send email

                //try
                //{
                //    SendEmailApprovers(materialtransfer.MaterialTransferId);
                //}
                //catch (Exception)
                //{

                //    return View("EmailError");
                //}
                

                //return RedirectToAction("Index");  
                return RedirectToAction("Edit", new {id = materialtransfer.MaterialTransferId});
            }

            ViewBag.TransferReasonId = new SelectList(db.TransferReasons, "TransferReasonId", "TransferReasonName", materialtransfer.TransferReasonId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", materialtransfer.TransferTypeId);
            ViewBag.FromCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", materialtransfer.FromCountryId);
            ViewBag.CurrentLocationId = new SelectList(db.Locations, "LocationId", "LocationName",materialtransfer.CurrentLocationId);
            ViewBag.ToCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", materialtransfer.ToCountryId);
            ViewBag.NewLocationId = new SelectList(db.Locations, "LocationId", "LocationName",materialtransfer.NewLocationId);
            ViewBag.ReceiverId = new SelectList(GetUsers(), "UserId", "FullName", materialtransfer.ReceiverId);
            ViewBag.Status = new SelectList(TransferStatusLookup.StatusLookup(),materialtransfer.Status);
            ViewBag.CautionIds = db.Cautions.ToList().Select(x => new SelectListItem
            {
                Text = x.CautionName,
                Value = x.CautionId.ToString()
            });
            return View(materialtransfer);
        }

       
        private void SendEmailApprovers(int id)
        {
            var materialtransfer = db.MaterialTransfers.Find(id);
            foreach (var approval in materialtransfer.MaterialTransferApprovals)
            {
                var url = Url.AbsoluteAction("MaterialTransferApproval", "MaterialTransfer", new { id = id, userid = approval.ApprovingUser.UserId });
               // var sb = new StringBuilder();
               // var materialtransferapproval = db.MaterialTransferApprovals.Find(id);
             //   sb.Append("Click this <a href='" + url + "'>Link</a>");
                var user = db.Users.Find(approval.ApprovingUser.UserId);
                if (string.IsNullOrWhiteSpace(user.Email)) continue;
                var notifier = new MaterialTransferForApprovalEmail
                                   {
                                       AppUrl = "Click this <a href='" + url + "'>Link</a>",
                                       Culture =
                                           new CultureInfo(user.Language != null
                                                               ? user.Language.LanguageShortName
                                                               : "en"),
                                       MTRNumber = materialtransfer.MaterialTransferNumber,
                                       UserEmail = user.Email
                                   };
                                   notifier.Send();
                //var notifier = new EmailNotifier
                //{
                //    RecepientEmail = user.Email,
                //    Subject = "Material Transfer for Approval",
                //    Body = sb.ToString(),
                //    IsBodyHtml = true
                //};

                //try
                //{
                //    notifier.SendEmail();
                //}
                //catch (Exception)
                //{
                    
                //    throw;
                //}
                
            }

            
        }


        private void SendEmailNotificationUsers(int id)
        {
            var materialtransfer = db.MaterialTransfers.Find(id);
            var notificationusers =
                db.NotificationUsers.Where(
                    x =>
                    x.TransferTypeId == materialtransfer.TransferTypeId && x.CountryId == materialtransfer.FromCountryId);
            foreach (var approval in notificationusers)
            {
                //var url = Url.AbsoluteAction("MaterialTransferApproval", "MaterialTransfer", new { id = id, userid = approval.UserId });
   
                var user = db.Users.Find(approval.UserId);
                if (string.IsNullOrWhiteSpace(user.Email)) continue;
                var notifier = new MaterialTransferNotificationEmail()
                {
                    //AppUrl = "Click this <a href='" + url + "'>Link</a>",
                    Culture =
                        new CultureInfo(user.Language != null
                                            ? user.Language.LanguageShortName
                                            : "en"),
                    MTRNumber = materialtransfer.MaterialTransferNumber,
                    UserEmail = user.Email
                };
                notifier.Send();
                //var notifier = new EmailNotifier
                //{
                //    RecepientEmail = user.Email,
                //    Subject = "Material Transfer for Approval",
                //    Body = sb.ToString(),
                //    IsBodyHtml = true
                //};

                //try
                //{
                //    notifier.SendEmail();
                //}
                //catch (Exception)
                //{

                //    throw;
                //}

            }


        }
        //
        // GET: /MaterialTransfer/Edit/5
 
        public ActionResult Edit(int id)
        {
            MaterialTransfer materialtransfer = db.MaterialTransfers.Find(id);
            ViewBag.TransferReasonId = new SelectList(db.TransferReasons, "TransferReasonId", "TransferReasonName", materialtransfer.TransferReasonId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", materialtransfer.TransferTypeId);
            ViewBag.FromCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", materialtransfer.FromCountryId);
            ViewBag.CurrentLocationId = new SelectList(db.Locations, "LocationId", "LocationName", materialtransfer.CurrentLocationId);
            ViewBag.ToCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", materialtransfer.ToCountryId);
            ViewBag.NewLocationId = new SelectList(db.Locations, "LocationId", "LocationName", materialtransfer.NewLocationId);
            ViewBag.ReceiverId = new SelectList(GetUsers(), "UserId", "FullName",materialtransfer.ReceiverId);
            ViewBag.Status = new SelectList(TransferStatusLookup.StatusLookup(),materialtransfer.Status);
            ViewBag.ConditionEvaluations = db.ConditionEvaluations;


            ViewBag.CautionIds = GetCautions(materialtransfer,db.Cautions);
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            var model = new EditMaterialTransferPageViewModel(materialtransfer,currentuser.UserId);

            //model.IsShipReady = model.IsOwned && _workflow.IsReadyForShipping(model.MaterialTransferId);
            
            //model.IsOwned = materialtransfer.OriginatorId == currentuser.UserId;


            var originator = db.Users.Find(materialtransfer.OriginatorId);
            model.OriginatorName = string.Format("{0} {1}", originator.FirstName, originator.LastName);

            return View(model);
        }

        private IEnumerable<SelectListItem> GetCautions(MaterialTransfer materialTransfer,IEnumerable<Caution> cautions)
        {
            if (materialTransfer.Cautions != null)
            return cautions.Select(caution => new SelectListItem
                                                  {
                                                      Text = caution.CautionName, Value = caution.CautionId.ToString(), Selected = materialTransfer.Cautions.Contains(caution)
                                                  }).ToList();
            return cautions.Select(caution => new SelectListItem
            {
                Text = caution.CautionName,
                Value = caution.CautionId.ToString()
            }).ToList();
        }

        private IEnumerable<SelectListItem> GetTransferType(MaterialTransfer materialTransfer, IEnumerable<TransferType> transferTypes)
        {
            if (materialTransfer.Cautions != null)
                return transferTypes.Select(caution => new SelectListItem
                {
                    Text = caution.TransferTypeName,
                    Value = caution.TransferTypeId.ToString(),
                    Selected = materialTransfer.TransferTypeId == caution.TransferTypeId
                }).ToList();
            return transferTypes.Select(caution => new SelectListItem
            {
                Text = caution.TransferTypeName,
                Value = caution.TransferTypeId.ToString()
            }).ToList();
        }

        private IEnumerable<SelectListItem> GetTransferReason(MaterialTransfer materialTransfer, IEnumerable<TransferReason> transferTypes)
        {
            if (materialTransfer.Cautions != null)
                return transferTypes.Select(caution => new SelectListItem
                {
                    Text = caution.TransferReasonName,
                    Value = caution.TransferReasonId.ToString(),
                    Selected = materialTransfer.TransferTypeId == caution.TransferReasonId
                }).ToList();
            return transferTypes.Select(caution => new SelectListItem
            {
                Text = caution.TransferReasonName,
                Value = caution.TransferReasonId.ToString()
            }).ToList();
        }

        private IList<Caution> GetCautionById(int[] cautions)
        {
            var selectedcautions = new List<Caution>();
            foreach (var cautionid in cautions)
            {
                var dbcaution = db.Cautions.Single(x => x.CautionId == cautionid);
                selectedcautions.Add(dbcaution);
            }
            return selectedcautions;
        }

        private IList<TransferType> GetTransferById(int[] transfertypes)
        {
            var selectedcautions = new List<TransferType>();
            foreach (var cautionid in transfertypes)
            {
                var dbcaution = db.TransferTypes.Single(x => x.TransferTypeId == cautionid);
                selectedcautions.Add(dbcaution);
            }
            return selectedcautions;
        }

        private void SetCautions(MaterialTransfer materialTransfer, int[] cautionIds)
        {
            var selectedTags = GetCautionById(cautionIds);

            var cautions = materialTransfer.Cautions.ToList();
            foreach (var caution in cautions)
                if (!selectedTags.Contains(caution))
                    materialTransfer.Cautions.Remove(caution);

            foreach (var tag in selectedTags)
                if (!cautions.Contains(tag))
                    materialTransfer.Cautions.Add(tag);
        }

        
        //
        // POST: /MaterialTransfer/Edit/5

        [HttpPost]
        public ActionResult Edit(string submit, EditMaterialTransferPageViewModel model, FormCollection formCollection, IEnumerable<int> cautionIds)
        {
            var materialtransfer = db.MaterialTransfers.Find(model.MaterialTransferId);
         
            if (ModelState.IsValid)
            {
         
                if (model.IsShipReady)
                {
                    model.UpdateShippingModel(materialtransfer);
                    

                }
                else if (User.IsInRole("Admin") || (!User.IsInRole("Admin") 
                    && (materialtransfer.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Draft))))
                {
                    model.UpdateModel(materialtransfer);
                }

                if (cautionIds != null)
                    SetCautions(materialtransfer,cautionIds.ToArray());

                foreach (var child in materialtransfer.TransferAssets.ToList())
                {
                    
                    db.MaterialTransferAssets.Remove(child);
                }
                try
                {

                    var uniqueassetid = formCollection["MTA_AssetUniqueId"].Split(',');
                    //var mtaAssetid = formCollection["MTA_AssetId"].Split(',');
                    var nonassetdesc = formCollection["NonAssetDescription"].Split(',');
                    var nonassetqty = formCollection["NonAssetQuantity"].Split(',');
                    var conditions = formCollection["Condition"].Split(',');
                    var chkisshipped = formCollection["IsShipped"].Split(',');
                    var txtetadays = formCollection["ETADays"].Split(',');
                    //if (materialtransfer.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Approved)
                    //&& model.IsOwned)
                    //{
                        
                    //}

                    for (var i = 0; i < uniqueassetid.Length; i++)
                    {
                        var materialtransferasset = new MaterialTransferAsset();
                        var assetnumber = uniqueassetid[i];
                        if (!string.IsNullOrWhiteSpace(assetnumber) && assetnumber != "NA")
                        {
                            var assetid = db.Assets.FirstOrDefault(x => x.AssetUniqueId == assetnumber);
                            if (assetid != null)
                            {
          
                                    materialtransferasset.AssetId = assetid.AssetId;
                                    materialtransferasset.ConditionEvaluationId = int.Parse(conditions[i]);
                                    materialtransferasset.ETADays = int.Parse(txtetadays[i]);
                                    materialtransferasset.IsShipped = bool.Parse(chkisshipped.Count() != txtetadays.Count() ? chkisshipped[i * 2] : chkisshipped[i]);
                                //}
                            }

                            
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(nonassetdesc[i]))
                            {
                                materialtransferasset.NonAssetDescription = nonassetdesc[i];
                                materialtransferasset.NonAssetQuantity = int.Parse(nonassetqty[i]);
                            }
                            
                        }
                        materialtransfer.TransferAssets.Add(materialtransferasset);
                    }
                }
                catch (Exception)
                {

                }


                db.SaveChanges();
                //try to update the status
                

                //_workflow.MovetoTransit(model.MaterialTransferId);

                if (materialtransfer.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.TransferRequested))
                {
                    foreach (var asset in
                        from assets in materialtransfer.TransferAssets
                        where assets.AssetId.HasValue
                        select db.Assets.Find(assets.AssetId))
                    {
                        asset.Status = Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.TransferRequested);
                        db.SaveChanges();
                    }
                }
                TempData["SuccessMsg"] = "Material Transfer Saved!";
                //return RedirectToAction("Index");
                
                if (submit == "Send for Approval")
                {
                    var approvalResult = SendApprovalNotification(model.MaterialTransferId);
                    if (approvalResult.Content != "success")
                        TempData["ErrorMsg"] = "Send for Approval failed!";
                    TempData["SuccessMsg"] = "Approval Sent.";

                }
                if (submit == "Ship")
                {
                    _workflow.MovetoTransit(model.MaterialTransferId);
                    TempData["SuccessMsg"] = "Material Transfer Shipped!";
                }
                return RedirectToAction("Edit",new { id = model.MaterialTransferId});



            }
            ViewBag.TransferReasonId = new SelectList(db.TransferReasons, "TransferReasonId", "TransferReasonName", model.TransferReasonId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", model.TransferTypeId);
            ViewBag.FromCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", model.FromCountryId);
            ViewBag.CurrentLocationId = new SelectList(db.Locations, "LocationId", "LocationName", model.CurrentLocationId);
            ViewBag.ToCountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", model.ToCountryId);
            ViewBag.NewLocationId = new SelectList(db.Locations, "LocationId", "LocationName", model.NewLocationId);
            ViewBag.ReceiverId = new SelectList(GetUsers(), "UserId", "FullName", model.ReceiverId);
            ViewBag.Status = new SelectList(TransferStatusLookup.StatusLookup(), model.Status);
            ViewBag.CautionIds = GetCautions(materialtransfer, db.Cautions);
            ViewBag.ConditionEvaluations = db.ConditionEvaluations;
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            model.IsOwned = materialtransfer.OriginatorId == currentuser.UserId;

            model.Cautions = materialtransfer.Cautions.Select(x => new CautionDTO
            {
                CautionId = x.CautionId,
                CautionName = x.CautionName
            }).ToList();
            model.Assets = materialtransfer.TransferAssets.Select(x => new MaterialTransferAssetDTO
            {
                MTA_AssetId = x.AssetId,
                MTA_AssetUniqueId = x.Asset != null ? x.Asset.AssetUniqueId : string.Empty,
                MaterialTransferAssetId = x.MaterialTransferAssetId,
                MaterialTransferId = x.MaterialTransferId,
                NonAssetDescription = x.NonAssetDescription,
                NonAssetQuantity = x.NonAssetQuantity,
                IsShipped = x.IsShipped,
                ETADays = x.ETADays,
                ConditionEvaluationId = x.ConditionEvaluationId ?? 0
            }).ToList();
            return View(model);
        }

        //
        // GET: /MaterialTransfer/Delete/5
 
        public ActionResult Delete(int id)
        {
            MaterialTransfer materialtransfer = db.MaterialTransfers.Find(id);
            return View(materialtransfer);
        }

        //
        // POST: /MaterialTransfer/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {       
            
            MaterialTransfer materialtransfer = db.MaterialTransfers.Find(id);
            //var statusvalue = Enum.Parse(typeof (TransferStatusEnum), materialtransfer.Status);
            if (materialtransfer.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Draft))
            {
                foreach (var approval in materialtransfer.MaterialTransferApprovals.ToList())
                {
                    db.MaterialTransferApprovals.Remove(approval);
                }
                db.MaterialTransfers.Remove(materialtransfer);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        [HttpPost, ActionName("Cancel")]
        public ActionResult CancelConfirmed(int id)
        {
            var mtf = db.MaterialTransfers.Find(id);
            mtf.Status = Enum.GetName(typeof (TransferStatusEnum), TransferStatusEnum.Cancelled);
            db.SaveChanges();
            _workflow.CancelRejected(id);
            return RedirectToAction("Edit","Materialtransfer",new {id=id});
        }

        [HttpPost, ActionName("Ship")]
        public ActionResult ShipConfirmed(int id)
        {
            _workflow.MovetoTransit(id);
            return RedirectToAction("Edit", "Materialtransfer", new { id = id });
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private IList<UserDTO> GetUsers()
        {
            return db.Users.Select(x => new UserDTO
                                            {
                                                 UserId = x.UserId,
                                                 FullName = x.FirstName + " " + x.LastName
                                            }).ToList();
        }

        private string GetUniqueNumber()
        {
            var ci = new CultureInfo("en-us");
            var materialtransfers = db.MaterialTransfers.Count();
            var materialtransfercount = materialtransfers + 1;

            return materialtransfercount.ToString("D7", ci);
        }

        private string GetUniqueNumber(int country)
        {
            var ci = new CultureInfo("en-us");
            var materialtransfers = db.MaterialTransfers.ToList().LastOrDefault(x=>x.FromCountryId == country);
            if (materialtransfers == null)
                return "0000001";
            var materialtransfercount = int.Parse(materialtransfers.MaterialTransferNumber.Substring(4,7)) + 1;

            return materialtransfercount.ToString("D7", ci);
        }

        public ActionResult RenderMaterialTransferAsset(int id,bool? isship,int? conditionid)
        {
            ViewData["shipmode"] = isship ?? false;
            ViewBag.Condition = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId",
                                               "ConditionEvalDescription", conditionid.HasValue ? conditionid : 0);
            return PartialView("MaterialTransferAssetControl",new MaterialTransferAssetDTO{ RowId = id});
        }

        [HttpPost]
        public ActionResult RenderMaterialTransferAssets(int id,List<string> assets, bool? isship)
        {
            
            ViewData["shipmode"] = isship ?? false;
            ViewBag.Condition = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId",
                                               "ConditionEvalDescription");
            var vm = new MaterialTransferAssetListViewModel
                         {
                             Assets = assets.Select(x => new MaterialTransferAssetDTO
                                                             {
                                                                 MTA_AssetUniqueId = x
                                                             }).ToList(),
                                                             StartCount = id
                         };
            return PartialView("MaterialTransferAssetControlList", vm);
        }

        //public ActionResult RenderMateialTransferApprovalControl(int transfertypeid , int countryid)
        //{
        //    var approvingusers = db.ApprovingUsers.Where(x => x.CountryId == countryid
        //        && x.TransferTypeId == transfertypeid).ToList();
        //    var model = approvingusers.Select(x => new MaterialTransferApprovalDTO()
        //                                             {
        //                                                 ApprovingUser = new UserDTO
        //                                                                     {
        //                                                                         FullName = x.User != null ? string.Format("{0} {1}",x.User.FirstName,x.User.LastName) : string.Empty,
        //                                                                         Position = x.User != null ? x.User.Position : string.Empty
        //                                                                     }
        //                                             });
        //    //if (transfertypeid == 1)
        //    return PartialView("MateialTransferApprovalControl", model);
        //    //var countrymgrs = approvingusers.Select(x => new MaterialTransferCountryManagerDTO
        //    //                                                 {
        //    //                                                     UserId = x.UserId,
        //    //                                                     FullName =
        //    //                                                         x.User != null
        //    //                                                             ? string.Format("{0} {1}", x.User.FirstName,
        //    //                                                                             x.User.LastName)
        //    //                                                             : string.Empty
        //    //                                                 });
        //    //ViewBag.CountryMgr = new SelectList(countrymgrs, "UserId", "FullName");
        //    //return PartialView("MaterialTransferCountryApprovalControl", model);
        //}


        public ActionResult RenderEditMaterialTransferApprovalControl(int materialtransferid)
        {
            var materialtransfer = db.MaterialTransfers.Find(materialtransferid);

            var transferreason = db.TransferReasons.Find(materialtransfer.TransferReasonId);
            //var approvingusers = db.ApprovingUsers.ToList().Where(x => x.CountryId == materialtransfer.FromCountryId
            //    && x.TransferTypeId == materialtransfer.TransferTypeId && x.TransferReasons.Contains(transferreason));

            var approvingusers = materialtransfer.MaterialTransferApprovals.ToList();

            var notifiedusers = db.NotificationUsers.ToList().Where(x => x.CountryId == materialtransfer.FromCountryId
                && x.TransferTypeId == materialtransfer.TransferTypeId && x.TransferReasons.Contains(transferreason));

            var model = new MaterialTransferUserViewModel
            {
                Approval = approvingusers.Select(x => new MaterialTransferApprovalDTO
                {
                    ApprovalDateTime = x.ApprovalDateTime,
                    ApprovalStatus = x.ApprovalStatus,
                    ApprovingUser = new UserDTO
                    {
                        FullName = x.ApprovingUser != null && x.ApprovingUser.User != null ? string.Format("{0} {1}", x.ApprovingUser.User.FirstName, x.ApprovingUser.User.LastName) : string.Empty,
                        Position = x.ApprovingUser != null && x.ApprovingUser.User != null ? x.ApprovingUser.User.Position : string.Empty
                    }
                }),
                NotifyUser = notifiedusers.Select(y => new MaterialTransferNotifyUserDTO
                {
                    NotifiedUser = new UserDTO
                    {
                        FullName = y.User != null ? string.Format("{0} {1}", y.User.FirstName, y.User.LastName) : string.Empty,
                        Position = y.User != null ? y.User.Position : string.Empty
                    }
                })
            };


            return PartialView("MaterialTransferUsers", model);
        }

        public ActionResult RenderMaterialTransferApprovalControl(int transfertypeid,int transferreasonid, int countryid)
        {
            var transferreason = db.TransferReasons.Find(transferreasonid);
            var approvingusers = db.ApprovingUsers.ToList().Where(x => x.CountryId == countryid
                && x.TransferTypeId == transfertypeid && x.TransferReasons.Contains(transferreason));



            var notifiedusers = db.NotificationUsers.ToList().Where(x => x.CountryId == countryid
                && x.TransferTypeId == transfertypeid && x.TransferReasons.Contains(transferreason));

            var model = new MaterialTransferUserViewModel
                            {
                                Approval = approvingusers.Select(x=>new MaterialTransferApprovalDTO
                                                                        {
                                                                             ApprovingUser = new UserDTO
                                                                             {
                                                                                 FullName = x.User != null ? string.Format("{0} {1}",x.User.FirstName,x.User.LastName) : string.Empty,
                                                                                 Position = x.User != null ? x.User.Position : string.Empty
                                                                             }
                                                                        }),
                            NotifyUser =  notifiedusers.Select(y=>new MaterialTransferNotifyUserDTO
                            {
                                   NotifiedUser = new UserDTO
                                                      {
                                                          FullName = y.User != null ? string.Format("{0} {1}", y.User.FirstName, y.User.LastName) : string.Empty,
                                                          Position = y.User != null ? y.User.Position : string.Empty
                                                      }                                      
                            })
                            };
            

            return PartialView("MaterialTransferUsers", model);
        }

        public ActionResult MaterialTransferApproval(int id , int userid)
        {
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name && x.UserId == userid);
            if (currentuser == null) return View("Unauthorized");
            var model = new MaterialTransferApprovalPageViewModel();
            ViewBag.Status = new SelectList(TransferStatusLookup.ApproveStatusLookup());
            return View(model);
        }

        [HttpPost]
        public ActionResult MaterialTransferApproval(int id,int userid,MaterialTransferApprovalPageViewModel model)
        {
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name && x.UserId == userid);
            if (currentuser == null) return View("Unauthorized");

            var materialtransferapproval = db.MaterialTransferApprovals.FirstOrDefault(x=>x.MaterialTransferId == id && x.ApprovingUser.UserId == userid);
            if (materialtransferapproval == null) return View("Unauthorized");
            materialtransferapproval.MaterialTransfer.TransferComment += (model.Comments + string.Format("  [{0}]",currentuser.UserName));
            materialtransferapproval.ApprovalStatus = model.Status;
            if (model.Status == Enum.GetName(typeof(ApproveTransferStatusEnum),ApproveTransferStatusEnum.ApproveTransfer))
                materialtransferapproval.ApprovalDateTime = DateTime.Now;


            if (model.Status == Enum.GetName(typeof(ApproveTransferStatusEnum), ApproveTransferStatusEnum.DisapproveTransfer))
            {
                var disapproved = Enum.GetName(typeof (TransferStatusEnum), TransferStatusEnum.Disapproved);
                materialtransferapproval.MaterialTransfer.ApprovalStatus = disapproved ;
                materialtransferapproval.MaterialTransfer.Status = disapproved;
                materialtransferapproval.ApprovalDateTime = DateTime.Now;
                db.SaveChanges();

                _workflow.CancelRejected(materialtransferapproval.MaterialTransferId);
                //send email

                var mtf = db.MaterialTransfers.Find(materialtransferapproval.MaterialTransferId);
                var receiver = db.Users.Include(x=>x.Language).FirstOrDefault(x=>x.UserId == mtf.OriginatorId);
                var email = new MaterialTransferDisapproveEmail
                                {
                                    MTRNumber = mtf.MaterialTransferNumber,
                                    UserEmail = receiver.Email,
                                    Culture =
                                        new CultureInfo(receiver.Language == null
                                                            ? "en"
                                                            : receiver.Language.LanguageShortName)
                                                            
                                };
                email.Send();
                
            }
            db.SaveChanges();
            //try to move to approved status
            _workflow.MovetoApproved(materialtransferapproval.MaterialTransferId);
            return RedirectToAction("Edit", new {id = materialtransferapproval.MaterialTransferId});
        }

        public ContentResult SendApprovalNotification(int id)
        {
            var materialtransfer = db.MaterialTransfers.Find(id);
            //check assets
            if (materialtransfer.TransferAssets.Count <= 0) throw new Exception("No Assets");

            SendEmailApprovers(id);
            SendEmailNotificationUsers(id);
            
            materialtransfer.Status = Enum.GetName(typeof (TransferStatusEnum), TransferStatusEnum.TransferRequested);
            db.SaveChanges();
            return Content("success");
           
        }

        public ActionResult SendForApproval(int id,int userid)
        {
            var url = Url.AbsoluteAction("MaterialTransferApproval", "MaterialTransfer", new { id = id, userid = userid });
            var sb = new StringBuilder();
            var materialtransferapproval = db.MaterialTransferApprovals.Find(id);
            var materialtransfer = db.MaterialTransfers.Find(materialtransferapproval.MaterialTransferId);
            sb.Append("Click this <a href='" + url + "'>Link</a>");
            var user = db.Users.Find(userid);
            var notifier = new MaterialTransferForApprovalEmail()
            {
                AppUrl = "Click this <a href='" + url + "'>Link</a>",
                Culture =
                    new CultureInfo(user.Language != null
                                        ? user.Language.LanguageShortName
                                        : "en"),
                MTRNumber = materialtransfer.MaterialTransferNumber,
                UserEmail = user.Email
            };
            notifier.Send();
            //var notifier = new EmailNotifier
            //                   {
            //                       RecepientEmail = user.Email,
            //                       Subject = "Material Transfer for Approval",
            //                       Body = sb.ToString(),
            //                       IsBodyHtml = true
            //                   };
            //try
            //{
            //    notifier.SendEmail();
            //}
            //catch (Exception)
            //{

            //    return View("EmailError");
            //}
            
            ViewBag.Id = materialtransferapproval.MaterialTransferId;
            return View();
            //return RedirectToAction("Edit",new { id = materialtransferapproval.MaterialTransferId});
        }

        #region Receiver

        //public ActionResult MaterialTransferinTransit()
        //{
        //    var materialtransfers =
        //        db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType)
        //        .Where(x=>x.Status == Enum.GetName(typeof(TransferStatusEnum),TransferStatusEnum.Transit))
        //        .ToList();
        //    var query = from mt in materialtransfers
        //                join fromcountry in db.Countries on mt.FromCountryId equals fromcountry.CountryId
        //                join tocountry in db.Countries on mt.ToCountryId equals tocountry.CountryId
        //                select new MaterialTransferDTO
        //                {
        //                    MaterialTransferId = mt.MaterialTransferId,
        //                    MaterialTransferNumber = mt.MaterialTransferNumber,
        //                    TransferTypeName = mt.TransferType.TransferTypeName,
        //                    FromCountry = fromcountry.CountryName,
        //                    ToCountry = tocountry.CountryName,
        //                    DateAdded = mt.DateAdded,
        //                    Status = mt.Status
        //                };

        //    return PartialView("MaterialTransferTransitIndex", query);
        //}

        [ActionName("Receive")]
        public ActionResult ReceiveMaterialTransfer(int id)
        {
            var materialtransfer = db.MaterialTransfers.Find(id);
            var model = new MaterialTransferReceivePageViewModel(materialtransfer);
           // ViewBag.Status = new SelectList(TransferStatusLookup.ReceiveStatusLookup());
            ViewBag.AssetStatus = new SelectList(AssetStatusLookup.ReceiveStatusLookup());
            ViewBag.ConditionEvaluation = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription");
            return View(model);
        }

        [ActionName("Receive")]
        [HttpPost]
        public ActionResult ReceiveMaterialTransfer(MaterialTransferReceivePageViewModel model,FormCollection formCollection)
        {
            var materialtransfer = db.MaterialTransfers.Find(model.MaterialTransferId);
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            if (ModelState.IsValid)
            {

                #region Old Code

                //if (!string.IsNullOrWhiteSpace(model.Comments))
                //{
                //    materialtransfer.TransferComment += model.Comments + string.Format("  [{0}]", currentuser.UserName);
                //    db.SaveChanges();
                //}

                //if (model.Status == Enum.GetName(typeof(TransferStatusEnum),TransferStatusEnum.Discrepancy))
                //    _workflow.MovetoDiscrepancy(model.MaterialTransferId);
                ////else if (model.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Cancelled))
                ////    _workflow.CancelRejected(model.MaterialTransferId);
                //else if (model.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Approved)
                //    || model.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Received))
                //    _workflow.MovetoReceiverApproved(model.MaterialTransferId);
                //else
                //{
                //    materialtransfer.Status = model.Status;
                //    db.SaveChanges();
                //}
                #endregion

                var assetids = formCollection["item.MTA_AssetUniqueId"].Split(',');
                var assetstatus = formCollection["item.Status"].Split(',');
                var assetcondition = formCollection["item.ConditionEvaluationId"].Split(',');
                var assetcomment = formCollection["item.Comment"].Split(',');

                for (var i = 0; i < assetids.Count(); i++)
                {
                    var j = i;
                    var uniqueasetid = assetids[j];
                    var asset = db.Assets.FirstOrDefault(x => x.AssetUniqueId == uniqueasetid);
                    if (asset != null)
                    {
                        asset.Status = assetstatus[j];
                        asset.ConditionEvaluationID = int.Parse(assetcondition[j]);
                        asset.Comment += Environment.NewLine + " " + assetcomment[j] + " [" + User.Identity.Name + "]";
                        db.SaveChanges();
                    }
                }

                //if any is not received
                if (assetstatus.Any(x => x != Enum.GetName(typeof(AssetReceiveStatusEnum), AssetReceiveStatusEnum.Received)))
                {
                    _workflow.MovetoDiscrepancy(model.MaterialTransferId);
                }
                else
                {
                    _workflow.MovetoReceiverApproved(model.MaterialTransferId);
                }

                return RedirectToAction("Index", "Home");
            }

            //ViewBag.Status = new SelectList(TransferStatusLookup.StatusLookup());
            ViewBag.AssetStatus = new SelectList(AssetStatusLookup.ReceiveStatusLookup());
            ViewBag.ConditionEvaluation = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription");
            return View(model);
        }




        #endregion


    }
}