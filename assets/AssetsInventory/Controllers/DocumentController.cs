﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class DocumentController : BaseAdminController
    {
        private AssetContext db = new AssetContext();


        #region Auto-Generated

        //
        // GET: /Document/

        public ViewResult Index()
        {
            var documents = db.Documents.Include(d => d.Asset);
            return View(documents.ToList());
        }

        //
        // GET: /Document/Details/5

        public ViewResult Details(int id)
        {
            Document document = db.Documents.Find(id);
            return View(document);
        }

        //
        // GET: /Document/Create

        public ActionResult Create()
        {
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId");
            return View();
        } 

        //
        // POST: /Document/Create

        [HttpPost]
        public ActionResult Create(Document document)
        {
            if (ModelState.IsValid)
            {
                db.Documents.Add(document);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId", document.AssetId);
            return View(document);
        }
        
        //
        // GET: /Document/Edit/5
 
        public ActionResult Edit(int id)
        {
            Document document = db.Documents.Find(id);
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "DocumentTypeName",document.DocumentTypeId);
            return View(document);
        }

        //
        // POST: /Document/Edit/5

        [HttpPost]
        public ActionResult Edit(Document document)
        {
            if (ModelState.IsValid)
            {
                db.Entry(document).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", "Asset", new { id = document.AssetId });  
            }
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "DocumentTypeName", document.DocumentTypeId);
            return View(document);
        }

        //
        // GET: /Document/Delete/5
 
        public ActionResult Delete(int id)
        {
            Document document = db.Documents.Find(id);
            return View(document);
        }

        //
        // POST: /Document/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Document document = db.Documents.Find(id);
            var assetid = document.AssetId;
            db.Documents.Remove(document);
            db.SaveChanges();
            return RedirectToAction("Edit", "Asset", new { id = assetid });  
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        public ActionResult AssetCreate(int id)
        {
            var model = new Document {AssetId = id};
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "DocumentTypeName");
            return View(model);
        }


        [HttpPost]
        public ActionResult AssetCreate(int id, Document document, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/asset-data"), fileName);
                    file.SaveAs(path);

                    document.DocumentName = fileName;
                    
                    db.Documents.Add(document);
                    db.SaveChanges();
                    return RedirectToAction("Edit", "Asset", new { id = document.AssetId });
                }
               
            }
            

            return RedirectToAction("Edit", "Asset", new {id = id});
        }

        public void DownloadFile(int id)
        {
            var document = db.Documents.Find(id);
            if (!String.IsNullOrEmpty(document.DocumentName))
            {
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + document.DocumentName);
            }
            
            var filePath = HttpContext.Server.MapPath("/Content/asset-data/" + document.DocumentName);
                HttpContext.Response.TransmitFile(filePath);
        }

    }
}