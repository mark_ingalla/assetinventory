﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class DimensionUOMController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /DimensionUOM/

        public ViewResult Index()
        {
            return View(db.DimensionUOMs.ToList());
        }

        //
        // GET: /DimensionUOM/Details/5

        public ViewResult Details(int id)
        {
            DimensionUOM dimensionuom = db.DimensionUOMs.Find(id);
            return View(dimensionuom);
        }

        //
        // GET: /DimensionUOM/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /DimensionUOM/Create

        [HttpPost]
        public ActionResult Create(DimensionUOM dimensionuom)
        {
            if (ModelState.IsValid)
            {
                db.DimensionUOMs.Add(dimensionuom);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(dimensionuom);
        }
        
        //
        // GET: /DimensionUOM/Edit/5
 
        public ActionResult Edit(int id)
        {
            DimensionUOM dimensionuom = db.DimensionUOMs.Find(id);
            return View(dimensionuom);
        }

        //
        // POST: /DimensionUOM/Edit/5

        [HttpPost]
        public ActionResult Edit(DimensionUOM dimensionuom)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dimensionuom).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dimensionuom);
        }

        //
        // GET: /DimensionUOM/Delete/5
 
        public ActionResult Delete(int id)
        {
            DimensionUOM dimensionuom = db.DimensionUOMs.Find(id);
            return View(dimensionuom);
        }

        //
        // POST: /DimensionUOM/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            DimensionUOM dimensionuom = db.DimensionUOMs.Find(id);
            db.DimensionUOMs.Remove(dimensionuom);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}