﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{
    public class NotificationUserController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /ApprovingUser/

        public ViewResult Index()
        {
            var notificationUsers = db.NotificationUsers.Include(a => a.User).Include(a => a.TransferType).Include(a => a.Country);
            return View(notificationUsers.ToList());
        }

        public JsonResult List()
        {
            var data = db.NotificationUsers.Include(a => a.User).Include(a => a.TransferType).Include(a => a.Country);

            var model = data.ToList().Select(x => new ApprovingUserDTO
            {
                Id = x.NotificationUserId,
                Country = x.Country.CountryName,
                Name = x.User.FirstName + " " + x.User.LastName,
                TransferType = x.TransferType.TransferTypeName,
                UserName = x.User.UserName
            });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /ApprovingUser/Details/5

        public ViewResult Details(int id)
        {
            NotificationUser notificationUser = db.NotificationUsers.Find(id);
            return View(notificationUser);
        }

        //
        // GET: /NotificationUser/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users.Select(x => new UserDTO { UserId = x.UserId, FullName = x.FirstName + " " + x.LastName }), "UserId", "FullName", 1);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", 1);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", 1);
            ViewBag.TransferReasonIds = GetReasons(new NotificationUser(), db.TransferReasons);
            return View();
        }

        private IEnumerable<SelectListItem> GetReasons(NotificationUser NotificationUser, IEnumerable<TransferReason> reasons)
        {
            if (NotificationUser.TransferReasons != null)
                return reasons.Select(reason => new SelectListItem
                {
                    Text = reason.TransferReasonName,
                    Value = reason.TransferReasonId.ToString(),
                    Selected = NotificationUser.TransferReasons.Contains(reason)
                }).ToList();
            return reasons.Select(reason => new SelectListItem
            {
                Text = reason.TransferReasonName,
                Value = reason.TransferReasonId.ToString()
            }).ToList();
        }
        //
        // POST: /NotificationUser/Create

        [HttpPost]
        public ActionResult Create(NotificationUser notificationUser, IEnumerable<int> transferReasonIds)
        {
            if (ModelState.IsValid)
            {
                if (notificationUser.TransferReasons == null) notificationUser.TransferReasons = new List<TransferReason>();

                if (transferReasonIds != null) SetReasons(notificationUser, transferReasonIds.ToArray());
                db.NotificationUsers.Add(notificationUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users.Select(x => new UserDTO { UserId = x.UserId, FullName = x.FirstName + " " + x.LastName }), "UserId", "FullName", notificationUser.UserId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", notificationUser.TransferTypeId);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", notificationUser.CountryId);
            ViewBag.TransferReasonIds = GetReasons(notificationUser, db.TransferReasons);
            return View(notificationUser);
        }

        private void SetReasons(NotificationUser  notificationUser, int[] cautionIds)
        {
            var selectedTags = GetReasonById(cautionIds);

            var cautions = notificationUser.TransferReasons.ToList();
            foreach (var caution in cautions)
                if (!selectedTags.Contains(caution))
                    notificationUser.TransferReasons.Remove(caution);

            foreach (var tag in selectedTags)
                if (!cautions.Contains(tag))
                    notificationUser.TransferReasons.Add(tag);
        }

        private IList<TransferReason> GetReasonById(int[] reasons)
        {
            var selectedreasons = new List<TransferReason>();
            foreach (var reasonid in reasons)
            {
                var dbcaution = db.TransferReasons.Single(x => x.TransferReasonId == reasonid);
                selectedreasons.Add(dbcaution);
            }
            return selectedreasons;
        }
        //
        // GET: /ApprovingUser/Edit/5

        public ActionResult Edit(int id)
        {
            NotificationUser approvinguser = db.NotificationUsers.Find(id);
            ViewBag.UserId = new SelectList(db.Users.Select(x => new UserDTO { UserId = x.UserId, FullName = x.FirstName + " " + x.LastName }), "UserId", "FullName", approvinguser.UserId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", approvinguser.TransferTypeId);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", approvinguser.CountryId);
            ViewBag.TransferReasonIds = GetReasons(approvinguser, db.TransferReasons);
            return View(approvinguser);
        }

        //
        // POST: /ApprovingUser/Edit/5

        [HttpPost]
        public ActionResult Edit(NotificationUser approvinguser, IEnumerable<int> transferReasonIds)
        {
            var approvinguserentity = db.NotificationUsers.Find(approvinguser.NotificationUserId);
            if (ModelState.IsValid)
            {

                if (approvinguser.TransferReasons == null) approvinguser.TransferReasons = new List<TransferReason>();

                approvinguserentity.TransferTypeId = approvinguser.TransferTypeId;
                approvinguserentity.UserId = approvinguser.UserId;
                approvinguserentity.CountryId = approvinguser.CountryId;


                if (approvinguserentity.TransferReasons == null) approvinguserentity.TransferReasons = new List<TransferReason>();
                if (transferReasonIds != null)
                    SetReasons(approvinguserentity, transferReasonIds.ToArray());

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users.Select(x => new UserDTO { UserId = x.UserId, FullName = x.FirstName + " " + x.LastName }), "UserId", "FullName", approvinguser.UserId);
            ViewBag.TransferTypeId = new SelectList(db.TransferTypes, "TransferTypeId", "TransferTypeName", approvinguser.TransferTypeId);
            ViewBag.CountryId = new SelectList(db.Countries.OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Where(x=>x.IsShown), "CountryId", "CountryName", approvinguser.CountryId);
            ViewBag.TransferReasonIds = GetReasons(approvinguser, db.TransferReasons);
            return View(approvinguser);
        }

        //
        // GET: /ApprovingUser/Delete/5

        public ActionResult Delete(int id)
        {
            NotificationUser approvinguser = db.NotificationUsers.Find(id);
            return View(approvinguser);
        }

        //
        // POST: /ApprovingUser/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            NotificationUser approvinguser = db.NotificationUsers.Find(id);
            db.NotificationUsers.Remove(approvinguser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
