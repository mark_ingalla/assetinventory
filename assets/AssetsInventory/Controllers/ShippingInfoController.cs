﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class ShippingInfoController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /ShippingInfo/

        public ViewResult Index()
        {
            return View(db.ShippingInfos.ToList());
        }

        //
        // GET: /ShippingInfo/Details/5

        public ViewResult Details(int id)
        {
            ShippingInfo shippinginfo = db.ShippingInfos.Find(id);
            return View(shippinginfo);
        }

        //
        // GET: /ShippingInfo/Create

        public ActionResult Create()
        {
            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription");
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription");
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription");
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription");
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription");
            return View();
        } 

        //
        // POST: /ShippingInfo/Create

        [HttpPost]
        public ActionResult Create(ShippingInfo shippinginfo)
        {
            if (ModelState.IsValid)
            {
                db.ShippingInfos.Add(shippinginfo);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription",shippinginfo.WeightUOMId);
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription",shippinginfo.DryWeightUOMId);
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription",shippinginfo.HeightUOMId);
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription",shippinginfo.WidthUOMId);
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription",shippinginfo.DiameterUOMId);
            return View(shippinginfo);
        }
        
        //
        // GET: /ShippingInfo/Edit/5
 
        public ActionResult Edit(int id)
        {
            ViewData["AssetId"] = TempData["AssetId"];
            ShippingInfo shippinginfo = db.ShippingInfos.Find(id);
            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", shippinginfo.WeightUOMId);
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", shippinginfo.DryWeightUOMId);
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.HeightUOMId);
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.WidthUOMId);
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.DiameterUOMId);
            return View(shippinginfo);
        }

        //
        // POST: /ShippingInfo/Edit/5

        [HttpPost]
        public ActionResult Edit(int id,ShippingInfo shippinginfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shippinginfo).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("Edit", "Asset", new {id = id });
            }
            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", shippinginfo.WeightUOMId);
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", shippinginfo.DryWeightUOMId);
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.HeightUOMId);
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.WidthUOMId);
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.DiameterUOMId);
            return View(shippinginfo);
        }

        //
        // GET: /ShippingInfo/Delete/5
 
        public ActionResult Delete(int id)
        {
            ShippingInfo shippinginfo = db.ShippingInfos.Find(id);
            return View(shippinginfo);
        }

        //
        // POST: /ShippingInfo/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            ShippingInfo shippinginfo = db.ShippingInfos.Find(id);
            db.ShippingInfos.Remove(shippinginfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}