﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Utilities;

namespace AssetsInventory.Controllers
{
    [Authorize]
    [SetCulture]
    [Elmah.Contrib.Mvc.ElmahHandleError]
    public class BaseController : Controller
    {
    }

    //[AjaxAuthorize(Roles="Admin")]
    [Authorize]
    [SetCulture]
    [Elmah.Contrib.Mvc.ElmahHandleError]
    public class BaseAdminController : Controller
    {
        
        public BaseAdminController()
        {
            
        }

        public ActionResult DeleteHandler()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(string pagesize)
        {
            
            TempData["pagesize"] = pagesize;
            return RedirectToAction("Index");
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            base.OnActionExecuted(filterContext);
            if (filterContext.HttpContext.Request.HttpMethod == "GET" &&
                filterContext.RouteData.Values["action"].ToString() == "Index")
            {
                ViewData["pagesize"] = TempData["pagesize"] ?? 15;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            //handle error for delete referential
            if (filterContext.RouteData.Values["action"].ToString() == "Delete")
            {
                filterContext.ExceptionHandled = true;

                var returnurl = filterContext.HttpContext.Request.RawUrl;
                //TempData["ErrorMsg"] = filterContext.Exception.InnerException.InnerException.Message;
                TempData["ErrorMsg"] = "Cannot Delete this Record Since It is being referenced by another Record";
                TempData["DeleteUrl"] = returnurl;
                //RedirectToAction("DeleteHandler");
                filterContext.Result = View("DeleteHandler");
            }
            if (filterContext.RouteData.Values["action"].ToString() == "Edit" ||
                filterContext.RouteData.Values["action"].ToString() == "Create")
            {
                filterContext.ExceptionHandled = true;

                var returnurl = filterContext.HttpContext.Request.RawUrl;
                //TempData["ErrorMsg"] = filterContext.Exception.InnerException.InnerException.Message;
                TempData["ErrorMsg"] = "Cannot Add/Update this Record. Duplicate record!";
                TempData["DeleteUrl"] = returnurl;
                //RedirectToAction("DeleteHandler");
                filterContext.Result = Redirect(returnurl);
            }
            
            
        }
    }
}