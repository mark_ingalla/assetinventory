﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class PurchaseCurrencyController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /PurchaseCurrency/

        public ViewResult Index()
        {
            return View(db.Currencies.ToList());
        }

        //
        // GET: /PurchaseCurrency/Details/5

        public ViewResult Details(int id)
        {
            PurchaseCurrency purchasecurrency = db.Currencies.Find(id);
            return View(purchasecurrency);
        }

        //
        // GET: /PurchaseCurrency/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /PurchaseCurrency/Create

        [HttpPost]
        public ActionResult Create(PurchaseCurrency purchasecurrency)
        {
            if (ModelState.IsValid)
            {
                db.Currencies.Add(purchasecurrency);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(purchasecurrency);
        }
        
        //
        // GET: /PurchaseCurrency/Edit/5
 
        public ActionResult Edit(int id)
        {
            PurchaseCurrency purchasecurrency = db.Currencies.Find(id);
            return View(purchasecurrency);
        }

        //
        // POST: /PurchaseCurrency/Edit/5

        [HttpPost]
        public ActionResult Edit(PurchaseCurrency purchasecurrency)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchasecurrency).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(purchasecurrency);
        }

        //
        // GET: /PurchaseCurrency/Delete/5
 
        public ActionResult Delete(int id)
        {
            PurchaseCurrency purchasecurrency = db.Currencies.Find(id);
            return View(purchasecurrency);
        }

        //
        // POST: /PurchaseCurrency/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            PurchaseCurrency purchasecurrency = db.Currencies.Find(id);
            db.Currencies.Remove(purchasecurrency);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}