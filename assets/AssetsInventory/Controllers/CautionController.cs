﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{
    public class CautionController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /Caution/

        public ViewResult Index()
        {
            return View(db.Cautions.ToList());
        }

        //
        // GET: /Caution/Details/5

        public ViewResult Details(int id)
        {
            Caution caution = db.Cautions.Find(id);
            return View(caution);
        }

        //
        // GET: /Caution/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Caution/Create

        [HttpPost]
        public ActionResult Create(Caution caution)
        {
            if (ModelState.IsValid)
            {
                db.Cautions.Add(caution);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(caution);
        }
        
        //
        // GET: /Caution/Edit/5
 
        public ActionResult Edit(int id)
        {
            Caution caution = db.Cautions.Find(id);
            return View(caution);
        }

        //
        // POST: /Caution/Edit/5

        [HttpPost]
        public ActionResult Edit(Caution caution)
        {
            if (ModelState.IsValid)
            {
                db.Entry(caution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(caution);
        }

        //
        // GET: /Caution/Delete/5
 
        public ActionResult Delete(int id)
        {
            Caution caution = db.Cautions.Find(id);
            return View(caution);
        }

        //
        // POST: /Caution/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Caution caution = db.Cautions.Find(id);
            db.Cautions.Remove(caution);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}