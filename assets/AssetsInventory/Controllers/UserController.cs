﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Lookups;
using AssetsInventory.Models;
using AssetsInventory.Service;
using AssetsInventory.Utilities;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{ 
    public class UserController : BaseAdminController
    {
        private AssetContext db = new AssetContext();
        private readonly Dictionary<int,string > _usertype = UserTypeEnum.User.ToDictionary();

        //
        // GET: /User/

        public ViewResult Index()
        {
            return View(db.Users.ToList());
        }

        public JsonResult List()
        {
            var data = db.Users.ToList().Select(x => new UserDTO
                                                         {
                                                              FullName = x.FirstName + " " + x.LastName,
                                                              FirstName = x.FirstName,
                                                              LastName = x.LastName,
                                                              Username = x.UserName,
                                                              UserType = Enum.GetName(typeof(UserTypeEnum),x.UserTypeId),
                                                              UserId = x.UserId,
                                                              Position = x.Position
                                                         });
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        //
        // GET: /User/Details/5

        public ViewResult Details(int id)
        {
            User user = db.Users.Find(id);
            return View(user);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            ViewBag.LanguageId = new SelectList(db.Languages, "LanguageId", "LanguageName",1);
            
            ViewBag.UserTypeId = new SelectList(_usertype, "Key", "Value",0);
            return View();
        } 

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User user)
        {
            ModelState.Remove("UserName");
            if (ModelState.IsValid)
            {
                user.UserName = user.Email;
                //user.Password = Extensions.GetRandomPassword(8);
                //default as regular user
                var hashpass = EncryptService.Encrypt(user.Password,true);
                user.Password = hashpass;
                user.UserTypeId = user.UserTypeId;
                db.Users.Add(user);
                db.SaveChanges();

                //send email
                var culture = db.Languages.Find(user.LanguageId);
                var createuseremail = new CreatedUserEmail
                                          {
                                              AppUrl = Url.AbsoluteAction("LogOn", "Account"),
                                              Password = EncryptService.Decrypt(user.Password,true),
                                              UserEmail = user.Email,
                                              Culture = new CultureInfo(culture.LanguageShortName)
                                          };
                try
                {
                    createuseremail.Send();
                }
                catch (Exception)
                {

                    return View("EmailError");
                }
                

                return RedirectToAction("Index");  
            }
            ViewBag.LanguageId = new SelectList(db.Languages, "LanguageId", "LanguageName",user.LanguageId);
            ViewBag.UserTypeId = new SelectList(_usertype, "Key", "Value",user.UserTypeId);
            return View(user);
        }
        
        //
        // GET: /User/Edit/5
 
        public ActionResult Edit(int id)
        {
            User user = db.Users.Find(id);
            var model = new UserEditPageViewModel(user);
            ViewBag.LanguageId = new SelectList(db.Languages, "LanguageId", "LanguageName", user.LanguageId);
            ViewBag.UserTypeId = new SelectList(_usertype, "Key", "Value", user.UserTypeId);
            return View(model);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(UserEditPageViewModel model)
        {

            if (ModelState.IsValid)
            {
               
                var user = db.Users.Find(model.UserId);
                if (OneAdmin(model.UserId, model.UserTypeId))
                {
                    model.UpdateModel(user);
                    user.UserName = user.Email;
                    //default as regular user
                    user.UserTypeId = user.UserTypeId;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                TempData["ErrorMsg"] = "There can never be less than 1 admin";
            }
            ViewBag.LanguageId = new SelectList(db.Languages, "LanguageId", "LanguageName", model.LanguageId);
            ViewBag.UserTypeId = new SelectList(_usertype, "Key", "Value", model.UserTypeId);
            return View(model);
        }

        private bool OneAdmin(int userid,int usertypeid)
        {
            var adminusers = db.Users.Where(x => x.UserTypeId == 1);

            if (adminusers.Count() == 1 && adminusers.First().UserId == userid && usertypeid != 1)
            {
                return false;
            }
            return true;
        }
        //
        // GET: /User/Delete/5
 
        public ActionResult Delete(int id)
        {
            User user = db.Users.Find(id);
            return View(user);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpPost]
        public ContentResult Resend(int id)
        {
            var user = db.Users.Find(id);
            //send email
            var culture = db.Languages.Find(user.LanguageId ?? 1);
            var createuseremail = new CreatedUserEmail
            {
                AppUrl = Url.AbsoluteAction("LogOn", "Account"),
                Password = user.Password,
                UserEmail = user.Email,
                Culture = new CultureInfo(culture.LanguageShortName)
            };
            createuseremail.Send();
            return Content("Send OK");
        }

        public ContentResult GetRandomPassword()
        {
            var pass = Extensions.GetRandomPassword(8);
            return Content(pass);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}