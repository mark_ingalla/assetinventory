﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{

    public class CategoryController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /Category/

        public ViewResult Index()
        {
            return View(db.Categories.ToList());
        }

        public JsonResult List()
        {
            var data = db.Categories.ToList().Select(x => new CategoryDTO
                                                              {
                                                                  CategoryId = x.CategoryId,
                                                                  CategoryName = x.CategoryName,
                                                                  Prefix = x.Prefix 
                                                              });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Category/Details/5

        public ViewResult Details(int id)
        {
            Category category = db.Categories.Find(id);
            return View(category);
        }

        //
        // GET: /Category/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Category/Create

        [HttpPost]
        public ActionResult Create(Category category)
        {
            if (ModelState.IsValid)
            {
                db.Categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(category);
        }
        
        //
        // GET: /Category/Edit/5
 
        public ActionResult Edit(int id)
        {
            Category category = db.Categories.Find(id);
            return View(category);
        }

        //
        // POST: /Category/Edit/5

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        //
        // GET: /Category/Delete/5
 
        public ActionResult Delete(int id)
        {
            Category category = db.Categories.Find(id);
            return View(category);
        }

        //
        // POST: /Category/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Category category = db.Categories.Find(id);
            db.Categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult CategoryDropdown(int? categoryid)
        {
            var data = db.Categories.ToList().Select(x=> new CategoryDTO
                                                             {
                                                                 CategoryId = x.CategoryId,
                                                                 CategoryName = x.CategoryName + " - " + x.CategoryId
                                                             });
            ViewBag.CategoryId = new SelectList(data.OrderBy(x=>x.CategoryName), "CategoryId", "CategoryName",categoryid.HasValue ? categoryid.ToString() : string.Empty);
            var model = new CategoryDropdownViewModel();
            return PartialView("CategoryDropdown", model);
        }
    }
}