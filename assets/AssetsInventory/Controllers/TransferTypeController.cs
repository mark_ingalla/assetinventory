﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class TransferTypeController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /TransferType/

        public ViewResult Index()
        {
            return View(db.TransferTypes.ToList());
        }

        //
        // GET: /TransferType/Details/5

        public ViewResult Details(int id)
        {
            TransferType transfertype = db.TransferTypes.Find(id);
            return View(transfertype);
        }

        //
        // GET: /TransferType/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /TransferType/Create

        [HttpPost]
        public ActionResult Create(TransferType transfertype)
        {
            if (ModelState.IsValid)
            {
                db.TransferTypes.Add(transfertype);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(transfertype);
        }
        
        //
        // GET: /TransferType/Edit/5
 
        public ActionResult Edit(int id)
        {
            TransferType transfertype = db.TransferTypes.Find(id);
            return View(transfertype);
        }

        //
        // POST: /TransferType/Edit/5

        [HttpPost]
        public ActionResult Edit(TransferType transfertype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transfertype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(transfertype);
        }

        //
        // GET: /TransferType/Delete/5
 
        public ActionResult Delete(int id)
        {
            TransferType transfertype = db.TransferTypes.Find(id);
            return View(transfertype);
        }

        //
        // POST: /TransferType/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            TransferType transfertype = db.TransferTypes.Find(id);
            db.TransferTypes.Remove(transfertype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}