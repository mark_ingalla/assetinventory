﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{
    public class CountryController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /Country/

        public ViewResult Index()
        {
            return View(db.Countries.ToList());
        }

        public JsonResult List()
        {
            var data = db.Countries.ToList().OrderBy(x=>x.SortCode).ThenBy(x=>x.CountryName).Select(x=>new CountryDTO
                                                           {
                                                               Id = x.CountryId,
                                                               Code = x.CountryCode,
                                                               Name = x.CountryName,
                                                               SortCode = x.SortCode
                                                           });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Country/Details/5

        public ViewResult Details(int id)
        {
            Country country = db.Countries.Find(id);
            return View(country);
        }

        //
        // GET: /Country/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Country/Create

        [HttpPost]
        public ActionResult Create(Country country)
        {
            if (ModelState.IsValid)
            {
                db.Countries.Add(country);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(country);
        }
        
        //
        // GET: /Country/Edit/5
 
        public ActionResult Edit(int id)
        {
            Country country = db.Countries.Find(id);
            return View(country);
        }

        //
        // POST: /Country/Edit/5

        [HttpPost]
        public ActionResult Edit(Country country)
        {
            if (ModelState.IsValid)
            {
                db.Entry(country).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(country);
        }

        //
        // GET: /Country/Delete/5
 
        public ActionResult Delete(int id)
        {
            Country country = db.Countries.Find(id);
            return View(country);
        }

        //
        // POST: /Country/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Country country = db.Countries.Find(id);
            db.Countries.Remove(country);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}