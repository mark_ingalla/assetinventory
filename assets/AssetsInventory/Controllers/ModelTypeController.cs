﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{ 
    public class ModelTypeController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /ModelType/

        public ViewResult Index()
        {
            var modeltypes = db.ModelTypes.Include(m => m.Manufacturer);
            return View(modeltypes.ToList());
        }

        public JsonResult List()
        {
            var data = db.ModelTypes.Include(m => m.Manufacturer).ToList().Select(x => new ModelTypeDTO
                                                                                           {
                                                                                               Id = x.ModelTypeId,
                                                                                               Name = x.ModelTypeName,
                                                                                                Desc = x.ModelTypeDescription,
                                                                                                Manufacturer = x.Manufacturer.ManufacturerName
                                                                                           });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /ModelType/Details/5

        public ViewResult Details(int id)
        {
            ModelType modeltype = db.ModelTypes.Find(id);
            return View(modeltype);
        }

        //
        // GET: /ModelType/Create

        public ActionResult Create()
        {
            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName");
            return View();
        } 

        //
        // POST: /ModelType/Create

        [HttpPost]
        public ActionResult Create(ModelType modeltype)
        {
            if (ModelState.IsValid)
            {
                db.ModelTypes.Add(modeltype);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", modeltype.ManufacturerId);
            return View(modeltype);
        }
        
        //
        // GET: /ModelType/Edit/5
 
        public ActionResult Edit(int id)
        {
            ModelType modeltype = db.ModelTypes.Find(id);
            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", modeltype.ManufacturerId);
            return View(modeltype);
        }

        //
        // POST: /ModelType/Edit/5

        [HttpPost]
        public ActionResult Edit(ModelType modeltype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(modeltype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", modeltype.ManufacturerId);
            return View(modeltype);
        }

        //
        // GET: /ModelType/Delete/5
 
        public ActionResult Delete(int id)
        {
            ModelType modeltype = db.ModelTypes.Find(id);
            return View(modeltype);
        }

        //
        // POST: /ModelType/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            ModelType modeltype = db.ModelTypes.Find(id);
            db.ModelTypes.Remove(modeltype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CreateManufacturer(int id)
        {
            var model = new ModelType()
                            {
                                ManufacturerId = id
                            };
            var manufacturer = db.Manufacturers.Find(id);
            ViewBag.Manufacturer = manufacturer.ManufacturerName;
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateManufacturer(ModelType modeltype)
        {
            if (ModelState.IsValid)
            {
                db.ModelTypes.Add(modeltype);
                db.SaveChanges();
                return RedirectToAction("Index","Manufacturer");
            }
            var manufacturer = db.Manufacturers.Find(modeltype.ManufacturerId);
            ViewBag.Manufacturer = manufacturer.ManufacturerName;
            
            return View(modeltype);
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// partial view for modeltype dropdown
        /// </summary>
        /// <param name="id">manufacturer id</param>
        /// <param name="modeltypeid">modeltypeid id</param>
        /// <returns></returns>
        public ActionResult ModelTypeDropdown(int id,int? modeltypeid)
        {
            var data = db.ModelTypes.Where(x => x.ManufacturerId == id);
            ViewBag.ModelTypeID = new SelectList(data, "ModelTypeId", "ModelTypeName",modeltypeid.HasValue ? modeltypeid.ToString() : string.Empty);
            var model = new ModelTypeDropdownViewModel();
            return PartialView("ModelTypeDropdown", model);
        }
    }
}