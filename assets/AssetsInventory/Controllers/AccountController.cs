﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using AssetsInventory.Lookups;
using AssetsInventory.Models;
using AssetsInventory.Service;
using AssetsInventory.Utilities;

namespace AssetsInventory.Controllers
{
    [SetCulture]
    [Elmah.Contrib.Mvc.ElmahHandleError]
    public class AccountController : Controller
    {
        private AssetContext db = new AssetContext();
        //
        // GET: /Account/LogOn

        public ActionResult LogOn()
        {
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (PasswordMatch(model.UserName,model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                //MembershipCreateStatus createStatus;
                //Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);
                try
                {
                    var newuser = new User
                    {
                        UserName = model.Email,
                        Password = model.Password,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Position = "Administrator",
                        LanguageId = 1,
                        UserTypeId = (int)UserTypeEnum.Admin,//might be enum 1-admin,2-regular user
                        DateAdded = DateTime.Now.Date
                    };
                    db.Users.Add(newuser);
                    db.SaveChanges();

                    FormsAuthentication.SetAuthCookie(model.Email, false /* createPersistentCookie */);

                    return RedirectToAction("Index", "Home");
                }
                catch (Exception ex)
                {

                    ModelState.AddModelError("", ex.Message);
                }
                
                
                    
                
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    //MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    //changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                    var hashpass = EncryptService.Encrypt(model.NewPassword,true);
                    var oldpass = EncryptService.Encrypt(model.OldPassword,true);
                    var user = db.Users.Single(x => x.UserName == User.Identity.Name 
                        && (x.Password == model.OldPassword || x.Password == oldpass));
                    user.Password = hashpass;
                    //db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    changePasswordSucceeded = true;
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
         {
             var user = db.Users.FirstOrDefault(x => x.UserName == model.UserName);
             if (user == null) return Redirect("/");
             var culture = db.Languages.Find(user.LanguageId ?? 1);
             var createuseremail = new CreatedUserEmail
             {
                 AppUrl = Url.AbsoluteAction("LogOn", "Account"),
                 Password = user.Password,
                 UserEmail = user.Email,
                 Culture = new CultureInfo(culture.LanguageShortName)
             };
            try
            {
                createuseremail.Subject = "Password Recovery";
                createuseremail.Send();
                TempData["email"] = user.Email;
            }
            catch (Exception)
            {

                return RedirectToAction("ForgotPassword");
            }
            return View("EmailSent");

         }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        private bool PasswordMatch(string username,string password)
        {
            var hashpass = EncryptService.Encrypt(password,true);
            var user = db.Users.Where(x => x.UserName == username 
                && (x.Password == password || x.Password == hashpass)).SingleOrDefault();
            return user != null;
        }



        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
