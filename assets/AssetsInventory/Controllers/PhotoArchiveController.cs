﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class PhotoArchiveController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /PhotoArchive/

        public ViewResult Index()
        {
            var photoarchives = db.PhotoArchives.Include(p => p.Asset);
            return View(photoarchives.ToList());
        }

        //
        // GET: /PhotoArchive/Details/5

        public ViewResult Details(int id)
        {
            PhotoArchive photoarchive = db.PhotoArchives.Find(id);
            return View(photoarchive);
        }

        //
        // GET: /PhotoArchive/Create

        public ActionResult Create()
        {
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId");
            return View();
        } 

        //
        // POST: /PhotoArchive/Create

        [HttpPost]
        public ActionResult Create(PhotoArchive photoarchive)
        {
            if (ModelState.IsValid)
            {
                db.PhotoArchives.Add(photoarchive);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId", photoarchive.AssetId);
            return View(photoarchive);
        }
        
        //
        // GET: /PhotoArchive/Edit/5
 
        public ActionResult Edit(int id)
        {
            PhotoArchive photoarchive = db.PhotoArchives.Find(id);
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId", photoarchive.AssetId);
            return View(photoarchive);
        }

        //
        // POST: /PhotoArchive/Edit/5

        [HttpPost]
        public ActionResult Edit(PhotoArchive photoarchive)
        {
            if (ModelState.IsValid)
            {
                db.Entry(photoarchive).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", "Asset", new { id = photoarchive.AssetId });  
            }
            ViewBag.AssetId = new SelectList(db.Assets, "AssetId", "AssetUniqueId", photoarchive.AssetId);
            return View(photoarchive);
        }

        //
        // GET: /PhotoArchive/Delete/5
 
        public ActionResult Delete(int id)
        {
            PhotoArchive photoarchive = db.PhotoArchives.Find(id);
            return View(photoarchive);
        }

        //
        // POST: /PhotoArchive/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            PhotoArchive photoarchive = db.PhotoArchives.Find(id);
            var assetid = photoarchive.AssetId;
            db.PhotoArchives.Remove(photoarchive);
            db.SaveChanges();
            return RedirectToAction("Edit", "Asset", new { id = assetid });  
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        public ActionResult AssetCreate(int id)
        {
            var model = new PhotoArchive{ AssetId = id };
            return View(model);
        }


        [HttpPost]
        public ActionResult AssetCreate(int id, PhotoArchive photoArchive, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    fileName = string.Format("[assetid{0}]{1}", id, fileName);
                    var path = Path.Combine(Server.MapPath("~/Content/asset-photo"),  fileName);
                    file.SaveAs(path);

                    photoArchive.PhotoFileName = fileName;
                    photoArchive.DateUploaded = DateTime.Now.Date;
                    db.PhotoArchives.Add(photoArchive);
                    db.SaveChanges();
                    return RedirectToAction("Edit", "Asset", new { id = photoArchive.AssetId });
                }

            }


            return RedirectToAction("Edit", "Asset", new { id = id });
        }

        public void DownloadFile(int id)
        {
            var photoarchive = db.PhotoArchives.Find(id);
            if (!String.IsNullOrEmpty(photoarchive.PhotoFileName))
            {
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + photoarchive.PhotoFileName);
            }

            var filePath = HttpContext.Server.MapPath("/Content/asset-photo/" + photoarchive.PhotoFileName);
            HttpContext.Response.TransmitFile(filePath);
        }
    }
}