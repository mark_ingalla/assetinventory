﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Lookups;
using AssetsInventory.Models;
using AssetsInventory.Queries;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{ 
    public class AssetController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /Asset/

        public ViewResult Index()
        {
            
            var assets = db.Assets.Include(a => a.Category).Include(a => a.SubCategory)
                .Include(a => a.Manufacturer).Include(a => a.ModelType).Include(a => a.Location)
                .Include(a => a.ConditionEvaluation);
            
            return View(assets.ToList());

        }

        



        public JsonResult List()
        {
            var assets = db.Assets.Include(a => a.Category).Include(a => a.SubCategory)
                .Include(a => a.Manufacturer).Include(a => a.ModelType).Include(a => a.Location)
                .Include(a => a.ConditionEvaluation);

            var data = assets.Select(x => new AssetDTO
                                                       {
                                                            AssetId=x.AssetId,
                                                            AssetUniqueId = x.AssetUniqueId,
                                                            Category = x.Category != null ? x.Category.CategoryName : string.Empty,
                                                            Location =  x.Location != null ?  x.Location.LocationName : string.Empty,
                                                            Manufacturer = x.Manufacturer != null ? x.Manufacturer.ManufacturerName : string.Empty,
                                                            ModelType = x.ModelType != null ? x.ModelType.ModelTypeName : string.Empty,
                                                            SerialNumber = x.SerialNumber,
                                                            Status = x.Status,
                                                            SubCategory = x.SubCategory !=null ? x.SubCategory.SubCategoryName: string.Empty
                                                       }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenderAssetSearch()
        {
            return PartialView("AssetSearch");
        }

        public JsonResult SearchList(string text,int? location)
        {
            if (string.IsNullOrWhiteSpace(text)) return Json(string.Empty,JsonRequestBehavior.AllowGet);




            var query = new AssetQuery().SearchAssets(text,location ?? 0);

            var data = query.Select(x => new AssetDTO
            {
                AssetId = x.AssetId,
                AssetUniqueId = x.AssetUniqueId,
                Category = x.Category != null ? x.Category.CategoryName : string.Empty,
                Location = x.Location != null ? x.Location.LocationName : string.Empty,
                Manufacturer = x.Manufacturer != null ? x.Manufacturer.ManufacturerName : string.Empty,
                ModelType = x.ModelType != null ? x.ModelType.ModelTypeName : string.Empty,
                SerialNumber = x.SerialNumber,
                Status = x.Status,
                SubCategory = x.SubCategory != null ? x.SubCategory.SubCategoryName : string.Empty
            }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);

           
        }


        //
        // GET: /Asset/Details/5

        public ViewResult Details(int id)
        {
            Asset asset = db.Assets.Find(id);
            return View(asset);
        }

        public ViewResult Print(int id)
        {
            Asset asset = db.Assets.Find(id);
            
            //var asset = db.Assets.Include(m => m.ShippingInfo).FirstOrDefault(m => m.AssetId == id));)))

            var model = new AssetPrintPageViewModel(asset);
            return View(model);
        }

        //
        // GET: /Asset/Create
        [Authorize(Roles="Admin")]
        public ActionResult Create()
        {
            //ViewBag.Status = new SelectList(AssetStatusLookup.StatusLookup());
            return View();
        } 

        //
        // POST: /Asset/Create

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(Asset asset)
        {
            var category = db.Categories.Find(asset.CategoryId);
            if (asset.IsPart && string.IsNullOrWhiteSpace(asset.SerialNumber))
                ModelState.AddModelError("SerialNumber", "SerialNumber is Required");

            if (ModelState.IsValid && asset.CategoryId.HasValue && asset.SubCategoryId.HasValue)
            {
                if (!asset.IsPart)
                {
                    //asset
                    asset.AssetUniqueId = string.Format("{0}-{1}", category.Prefix, AssetUniqueId());
                }
                else
                {
                    //spare
                    var manufacture = db.Manufacturers.Find(asset.ManufacturerId);
                    asset.AssetUniqueId = string.Format("{0}-{1}-{2}", category.Prefix, manufacture.ManufacturerCode,asset.SerialNumber);
                }
                //default status
                asset.AssetPurchasePrice = asset.AssetPurchasePrice ?? 0;
                asset.NetbookValue = asset.NetbookValue ?? 0;
                asset.FairMarketPrice = asset.FairMarketPrice ?? 0;
                asset.Status = Enum.GetName(typeof (AssetStatusEnum), AssetStatusEnum.InPlace);
                db.Assets.Add(asset);
                db.SaveChanges();
                //return RedirectToAction("Index");  
                return RedirectToAction("Edit", "Asset", new { id = asset.AssetId});
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", asset.CategoryId);
            ViewBag.SubCategoryId = new SelectList(db.SubCategories, "SubCategoryId", "SubCategoryName", asset.SubCategoryId);
            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", asset.ManufacturerId);
            ViewBag.ModelTypeID = new SelectList(db.ModelTypes, "ModelTypeId", "ModelTypeName", asset.ModelTypeID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationId", "LocationName", asset.LocationID);
            ViewBag.PurchaseInfoID = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "AFENumber", asset.PurchaseInfoID);
            ViewBag.ShippingInfoID = new SelectList(db.ShippingInfos, "ShippingInfoId", "ShippingInfoId", asset.ShippingInfoID);
            ViewBag.ConditionEvaluationID = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription", asset.ConditionEvaluationID);
            
            return View(asset);
        }
        
        //
        // GET: /Asset/Edit/5
 
        //public ActionResult Edit(int id)
        //{
        //    Asset asset = db.Assets.Find(id);
        //    ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", asset.CategoryId);
        //    ViewBag.SubCategoryId = new SelectList(db.SubCategories, "SubCategoryId", "SubCategoryName", asset.SubCategoryId);
        //    ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", asset.ManufacturerId);
        //    ViewBag.ModelTypeID = new SelectList(db.ModelTypes, "ModelTypeId", "ModelTypeName", asset.ModelTypeID);
        //    ViewBag.LocationID = new SelectList(db.Locations, "LocationId", "LocationName", asset.LocationID);
        //    ViewBag.PurchaseInfoID = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "AFENumber", asset.PurchaseInfoID);
        //    ViewBag.ShippingInfoID = new SelectList(db.ShippingInfos, "ShippingInfoId", "ShippingInfoId", asset.ShippingInfoID);
        //    ViewBag.ConditionEvaluationID = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription", asset.ConditionEvaluationID);

            
        //    return View(asset);
        //}

        [ActionName("Edit")]
        public ActionResult EditAsset(int id)
        {

            var asset = db.Assets.Find(id);
            var model = new EditAssetPageViewModel(asset);

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", asset.CategoryId);
            ViewBag.SubCategoryId = new SelectList(db.SubCategories, "SubCategoryId", "SubCategoryName", asset.SubCategoryId);
            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", asset.ManufacturerId);
            ViewBag.ModelTypeID = new SelectList(db.ModelTypes, "ModelTypeId", "ModelTypeName", asset.ModelTypeID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationId", "LocationName", asset.LocationID);

            ViewBag.ConditionEvaluationID = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription", asset.ConditionEvaluationID);


            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", asset.ShippingInfo != null && asset.ShippingInfo.WeightUOMId.HasValue ? asset.ShippingInfo.WeightUOMId.ToString() : string.Empty);
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", asset.ShippingInfo != null && asset.ShippingInfo.DryWeightUOMId.HasValue ? asset.ShippingInfo.DryWeightUOMId.ToString() : string.Empty);
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo != null && asset.ShippingInfo.HeightUOMId.HasValue ? asset.ShippingInfo.HeightUOMId.ToString() : string.Empty);
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo != null && asset.ShippingInfo.WidthUOMId.HasValue ? asset.ShippingInfo.WidthUOMId.ToString() : string.Empty);
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo != null && asset.ShippingInfo.DiameterUOMId.HasValue ? asset.ShippingInfo.DiameterUOMId.ToString() : string.Empty);

            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol", asset.PurchaseInfo != null && asset.PurchaseInfo.PurchaseCurrencyId.HasValue ? asset.PurchaseInfo.PurchaseCurrencyId.ToString() : string.Empty);
            ViewBag.Status = new SelectList(AssetStatusLookup.StatusLookup(),asset.Status);
          
            return View("edit-asset",model);

        }

        [ActionName("Edit")]
        [HttpPost]
        //[Authorize(Roles = "Admin")]
        public ActionResult EditAsset(EditAssetPageViewModel model,FormCollection formCollection )
        {
            var asset = db.Assets.Find(model.AssetId);
            if (model.IsPart && string.IsNullOrWhiteSpace(model.SerialNumber))
                ModelState.AddModelError("SerialNumber", "SerialNumber is Required");

            if (ModelState.IsValid)
            {
                if (User.IsInRole("Admin"))
                    model.UpdateModel(asset);
                else
                    model.UserUpdateModel(asset);

                db.SaveChanges();
                return RedirectToAction("Edit",new {id = model.AssetId});
            }


            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", model.CategoryId);
            ViewBag.SubCategoryId = new SelectList(db.SubCategories, "SubCategoryId", "SubCategoryName", model.SubCategoryId);
            ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", model.ManufacturerId);
            ViewBag.ModelTypeID = new SelectList(db.ModelTypes, "ModelTypeId", "ModelTypeName", model.ModelTypeID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationId", "LocationName", model.LocationID);

            ViewBag.ConditionEvaluationID = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription", model.ConditionEvaluationID);

            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", model.WeightUOMId);
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", model.DryWeightUOMId);
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", model.HeightUOMId);
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", model.WidthUOMId);
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", model.DiameterUOMId);

            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol",
                                                        model.PurchaseCurrencyId);
            ViewBag.Status = new SelectList(AssetStatusLookup.StatusLookup(), asset.Status);

            model.Documents = asset.Documents.Select(x => new DocumentDTO
            {
                DocumentDescription = x.DocumentDescription,
                DocumentId = x.DocumentId,
                DocumentName = x.DocumentName
            }).ToList();

            model.Photos = asset.PhotoArchives.Select(x => new PhotoAssetDTO
            {
                DateUploaded = x.DateUploaded,
                PhotoArchiveId = x.PhotoArchiveId,
                PhotoFileName = x.PhotoFileName
            }).ToList();
            return View("edit-asset", model);

        }

        //
        // POST: /Asset/Edit/5

        //[HttpPost]
        //public ActionResult Edit(Asset asset)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(asset).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", asset.CategoryId);
        //    ViewBag.SubCategoryId = new SelectList(db.SubCategories, "SubCategoryId", "SubCategoryName", asset.SubCategoryId);
        //    ViewBag.ManufacturerId = new SelectList(db.Manufacturers, "ManufacturerId", "ManufacturerName", asset.ManufacturerId);
        //    ViewBag.ModelTypeID = new SelectList(db.ModelTypes, "ModelTypeId", "ModelTypeName", asset.ModelTypeID);
        //    ViewBag.LocationID = new SelectList(db.Locations, "LocationId", "LocationName", asset.LocationID);
        //    ViewBag.PurchaseInfoID = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "AFENumber", asset.PurchaseInfoID);
        //    ViewBag.ShippingInfoID = new SelectList(db.ShippingInfos, "ShippingInfoId", "ShippingInfoId", asset.ShippingInfoID);
        //    ViewBag.ConditionEvaluationID = new SelectList(db.ConditionEvaluations, "ConditionEvaluationId", "ConditionEvalDescription", asset.ConditionEvaluationID);

        //    ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", asset.ShippingInfo.WeightUOMId);
        //    ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", asset.ShippingInfo.DryWeightUOMId);
        //    ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo.HeightUOMId);
        //    ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo.WidthUOMId);
        //    ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo.DiameterUOMId);
        //    return View(asset);
        //}

        //
        // GET: /Asset/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            Asset asset = db.Assets.Find(id);
            return View(asset);
        }

        //
        // POST: /Asset/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Asset asset = db.Assets.Find(id);
            db.Assets.Remove(asset);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private string AssetUniqueId()
        {
            var ci = new CultureInfo("en-us");
            var assets = db.Assets.Where(x => !x.IsPart).Count();
            var assetcount = assets + 1;

            return assetcount.ToString("D5", ci);


        }



        public ActionResult AddShippingInfo(int id)
        {
            var asset = db.Assets.Find(id);
            ViewBag.AssetId = id;
            TempData["AssetId"] = id;
            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", asset.ShippingInfo != null && asset.ShippingInfo.WeightUOMId.HasValue ? asset.ShippingInfo.WeightUOMId.ToString() : string.Empty);
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", asset.ShippingInfo != null && asset.ShippingInfo.DryWeightUOMId.HasValue ? asset.ShippingInfo.DryWeightUOMId.ToString() : string.Empty);
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo != null && asset.ShippingInfo.HeightUOMId.HasValue ? asset.ShippingInfo.HeightUOMId.ToString() : string.Empty);
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo != null && asset.ShippingInfo.WidthUOMId.HasValue ? asset.ShippingInfo.WidthUOMId.ToString() : string.Empty);
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", asset.ShippingInfo != null && asset.ShippingInfo.DiameterUOMId.HasValue ? asset.ShippingInfo.DiameterUOMId.ToString() : string.Empty);

            if (asset.ShippingInfoID.HasValue)
                //   return RedirectToAction("EditShippingInfo", new {id = asset.ShippingInfoID, assetid = id});
                return RedirectToAction("Edit", "ShippingInfo", new {id = asset.ShippingInfoID});

           
            return View();
        }



        [HttpPost]
        public ActionResult AddShippingInfo(int id,ShippingInfo shippinginfo)
        {
            if (ModelState.IsValid)
            {
                var asset = db.Assets.Find(id);

                if (asset.ShippingInfoID == null || asset.ShippingInfoID == 0)
                    db.ShippingInfos.Add(shippinginfo);
                //else
                //    db.Entry(shippinginfo).State = EntityState.Modified;
                
                db.SaveChanges();
                if (asset.ShippingInfoID == null || asset.ShippingInfoID == 0)
                {
                    asset.ShippingInfoID = shippinginfo.ShippingInfoId;
                    db.SaveChanges();
                }

                return RedirectToAction("Edit",new {id = asset.AssetId});

            }

            ViewBag.WeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", shippinginfo.WeightUOMId);
            ViewBag.DryWeightUOMId = new SelectList(db.WeightUOMs, "WeightUOMId", "WeightUOMDescription", shippinginfo.DryWeightUOMId);
            ViewBag.HeightUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.HeightUOMId);
            ViewBag.WidthUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.WidthUOMId);
            ViewBag.DiameterUOMId = new SelectList(db.DimensionUOMs, "DimensionUOMId", "DimesionDescription", shippinginfo.DiameterUOMId);
            return View(shippinginfo);
        }

        public ActionResult AddPurchaseInfo(int id)
        {
            var asset = db.Assets.Find(id);
            ViewBag.AssetId = id;
            TempData["AssetId"] = id;
            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol", asset.PurchaseInfo != null && asset.PurchaseInfo.PurchaseCurrencyId.HasValue ? asset.PurchaseInfo.PurchaseCurrencyId.ToString() : string.Empty);
            if (asset.PurchaseInfoID.HasValue)
                return RedirectToAction("Edit", "PurchaseInfo", new { id = asset.PurchaseInfoID });


            return View();
        }



        [HttpPost]
        public ActionResult AddPurchaseInfo(int id, PurchaseInfo purchaseinfo)
        {
            if (ModelState.IsValid)
            {
                var asset = db.Assets.Find(id);

                if (asset.PurchaseInfoID == null || asset.PurchaseInfoID == 0)
                    db.PurchaseInfos.Add(purchaseinfo);
                //else
                //    db.Entry(shippinginfo).State = EntityState.Modified;

                db.SaveChanges();
                if (asset.PurchaseInfoID == null || asset.PurchaseInfoID == 0)
                {
                    asset.PurchaseInfoID = purchaseinfo.PurchaseInfoId;
                    db.SaveChanges();
                }

                return RedirectToAction("Edit", new { id = asset.AssetId });

            }

            ViewBag.PurchaseCurrencyId = new SelectList(db.Currencies, "PurchaseCurrencyID", "PurhcaseCurrencySymbol",purchaseinfo.PurchaseCurrencyId);
            return View(purchaseinfo);
        }


        public JsonResult GetAssets(string term,int? id)
        {
            #region Old code for country
            var locationids = db.Locations.Where(x => x.CountryId == id).Select(x => x.LocationId).ToArray();

            var transferssets = from p in db.Assets.ToList()
                                join q in db.MaterialTransferAssets on p.AssetId equals q.AssetId
                                join r in db.MaterialTransfers.ToList() on q.MaterialTransferId equals r.MaterialTransferId
                                where p.Status != Enum.GetName(typeof (AssetStatusEnum), AssetStatusEnum.InPlace)
                                && p.Status != Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.Received)
                                || ((p.Status == Enum.GetName(typeof (AssetStatusEnum), AssetStatusEnum.InPlace)
                                || p.Status == Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.Received)) 
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Cancelled)
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Discrepancy)
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Received)
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Disapproved))
                                select p;
            var data = db.Assets.Include(x => x.Location)
                .Include(x => x.Category)
                .Include(x => x.SubCategory)
                .Include(x => x.Manufacturer)
                .Where(x => x.AssetUniqueId.StartsWith(term)).ToList();



            var jsondata = from p in data
                           //join transferassets in db.MaterialTransferAssets.ToList() 
                           //on p.AssetId equals transferassets.AssetId into joinedtransferassets 
                           //from transferassets in joinedtransferassets.DefaultIfEmpty()
                           where locationids.Contains(p.LocationID ?? 0)
                           && !transferssets.Contains(p)
                           //&& p.Status != Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.InPlace)
                           select p;
           
            #endregion

            #region non country query
            
            //var data = db.Assets.Include(x => x.Location)
            //    .Include(x => x.Category)
            //    .Include(x => x.SubCategory)
            //    .Include(x => x.Manufacturer)
            //    .Where(x => x.AssetUniqueId.StartsWith(term)).ToList();
            //var jsondata = from p in data
            //               where p.LocationID == id
            //               select p;
           
            #endregion

            return Json(jsondata.Select(x => new AssetAutoDropDTO
            {
                AssetId = x.AssetId,
                AssetUniqueId = x.AssetUniqueId,
                Category = x.Category != null ? x.Category.CategoryName : string.Empty,
                Subcategory = x.SubCategory != null ? x.SubCategory.SubCategoryName : string.Empty,
                Manufacturer = x.Manufacturer != null ? x.Manufacturer.ManufacturerName : string.Empty,
                SerialNumber = x.SerialNumber
            }).ToArray(), JsonRequestBehavior.AllowGet);
            //return Json(jsondata.Select(x => new AutoDropdownDTO { id= x.AssetId, value = x.AssetUniqueId }).ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetMaterialTransfers(int id)
        {
            //var asset = db.Assets.Include(x=>x.TransferAssets).FirstOrDefault(x=>x.AssetId == id);
            //var data = asset.TransferAssets.OrderByDescending(x=>x.DateAdded).Select(x => new MaterialTransferDTO
            //                                                {
            //                                                    MaterialTransferId = x.MaterialTransferId,
            //                                                    MaterialTransferNumber = x.MaterialTransfer.MaterialTransferNumber,
            //                                                    DateApproved = x.MaterialTransfer.ApprovalDate,
            //                                                    Status = x.MaterialTransfer.Status
            //                                                }).ToList();
            var data = db.MaterialTransfers.Include(x => x.TransferAssets).Where(x => x.TransferAssets.Any(y => y.AssetId == id)).Select(x => new MaterialTransferDTO
            {
                MaterialTransferId = x.MaterialTransferId,
                MaterialTransferNumber = x.MaterialTransferNumber,
                DateApproved = x.ApprovalDate,
                Status = x.Status
            }).Distinct();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
    }

    public class AssetAutoDropDTO
    {
        public int AssetId { get; set; }
        public string AssetUniqueId { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string SerialNumber { get; set; }
        public string Manufacturer { get; set; }
    }
}