﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class WeightUOMController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /WeightUOM/

        public ViewResult Index()
        {
            return View(db.WeightUOMs.ToList());
        }

        //
        // GET: /WeightUOM/Details/5

        public ViewResult Details(int id)
        {
            WeightUOM weightuom = db.WeightUOMs.Find(id);
            return View(weightuom);
        }

        //
        // GET: /WeightUOM/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /WeightUOM/Create

        [HttpPost]
        public ActionResult Create(WeightUOM weightuom)
        {
            if (ModelState.IsValid)
            {
                db.WeightUOMs.Add(weightuom);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(weightuom);
        }
        
        //
        // GET: /WeightUOM/Edit/5
 
        public ActionResult Edit(int id)
        {
            WeightUOM weightuom = db.WeightUOMs.Find(id);
            return View(weightuom);
        }

        //
        // POST: /WeightUOM/Edit/5

        [HttpPost]
        public ActionResult Edit(WeightUOM weightuom)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weightuom).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(weightuom);
        }

        //
        // GET: /WeightUOM/Delete/5
 
        public ActionResult Delete(int id)
        {
            WeightUOM weightuom = db.WeightUOMs.Find(id);
            return View(weightuom);
        }

        //
        // POST: /WeightUOM/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            WeightUOM weightuom = db.WeightUOMs.Find(id);
            db.WeightUOMs.Remove(weightuom);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}