﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Models;
using AssetsInventory.ViewModels;

namespace AssetsInventory.Controllers
{ 
    public class SubCategoryController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /SubCategory/

        public ViewResult Index()
        {
            var subcategories = db.SubCategories.Include(s => s.Category);
            return View(subcategories.ToList());
        }

        public JsonResult List()
        {
            var data = db.SubCategories.Include(s => s.Category).ToList().Select(x => new SubCategoryDTO
                                                                                          {
                                                                                              CategoryName =
                                                                                                  x.Category.
                                                                                                  CategoryName,
                                                                                              SubCategoryId =
                                                                                                  x.SubCategoryId,
                                                                                              SubCategoryName =
                                                                                                  x.SubCategoryName
                                                                                          });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /SubCategory/Details/5

        public ViewResult Details(int id)
        {
            SubCategory subcategory = db.SubCategories.Find(id);
            return View(subcategory);
        }

        //
        // GET: /SubCategory/Create

        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories.OrderBy(x=>x.CategoryName), "CategoryId", "CategoryName");
            return View();
        } 

        //
        // POST: /SubCategory/Create

        [HttpPost]
        public ActionResult Create(SubCategory subcategory)
        {
            if (ModelState.IsValid)
            {
                db.SubCategories.Add(subcategory);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.CategoryId = new SelectList(db.Categories.OrderBy(x => x.CategoryName), "CategoryId", "CategoryName", subcategory.CategoryId);
            return View(subcategory);
        }
        
        //
        // GET: /SubCategory/Edit/5
 
        public ActionResult Edit(int id)
        {
            SubCategory subcategory = db.SubCategories.Find(id);
            ViewBag.CategoryId = new SelectList(db.Categories.OrderBy(x => x.CategoryName), "CategoryId", "CategoryName", subcategory.CategoryId);
            return View(subcategory);
        }

        //
        // POST: /SubCategory/Edit/5

        [HttpPost]
        public ActionResult Edit(SubCategory subcategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subcategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories.OrderBy(x => x.CategoryName), "CategoryId", "CategoryName", subcategory.CategoryId);
            return View(subcategory);
        }

        //
        // GET: /SubCategory/Delete/5
 
        public ActionResult Delete(int id)
        {
            SubCategory subcategory = db.SubCategories.Find(id);
            return View(subcategory);
        }

        //
        // POST: /SubCategory/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            SubCategory subcategory = db.SubCategories.Find(id);
            db.SubCategories.Remove(subcategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        public ActionResult AddSubCategory(int id)
        {
            ViewBag.CategoryId = new SelectList(db.Categories.OrderBy(x => x.CategoryName), "CategoryId", "CategoryName", id);
            return View();
        }

        [HttpPost]
        public ActionResult AddSubCategory(SubCategory subcategory)
        {
            if (ModelState.IsValid)
            {
                db.SubCategories.Add(subcategory);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("AddSubCategory", new {id = subcategory.CategoryId});
            }

            ViewBag.CategoryId = new SelectList(db.Categories.OrderBy(x => x.CategoryName), "CategoryId", "CategoryName", subcategory.CategoryId);
            return View(subcategory);
        }

        /// <summary>
        /// partial view for ajax sub category dropdown
        /// </summary>
        /// <param name="id">category id</param>
        /// <param name="subcatid">sub category id</param>
        /// <returns></returns>
        public ActionResult SubCategoryDropdown(int id,int? subcatid)
        {
            var data = db.SubCategories.Where(x => x.CategoryId == id);
            ViewBag.SubCategoryId = new SelectList(data, "SubCategoryId", "SubCategoryName",subcatid.HasValue ? subcatid.ToString() : string.Empty);
            var model = new SubCategoryDropdownViewModel();
            return PartialView("SubCategoryDropdown",model);
        }

        public JsonResult GetTemplate(int id)
        {
            var data = db.SubCategories.Find(id);
            return Json(data.DescriptionTemplate, JsonRequestBehavior.AllowGet);
        }

    }
}