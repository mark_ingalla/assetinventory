﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class TransferReasonController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /TransferReason/

        public ViewResult Index()
        {
            return View(db.TransferReasons.ToList());
        }

        //
        // GET: /TransferReason/Details/5

        public ViewResult Details(int id)
        {
            TransferReason transferreason = db.TransferReasons.Find(id);
            return View(transferreason);
        }

        //
        // GET: /TransferReason/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /TransferReason/Create

        [HttpPost]
        public ActionResult Create(TransferReason transferreason)
        {
            if (ModelState.IsValid)
            {
                db.TransferReasons.Add(transferreason);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(transferreason);
        }
        
        //
        // GET: /TransferReason/Edit/5
 
        public ActionResult Edit(int id)
        {
            TransferReason transferreason = db.TransferReasons.Find(id);
            return View(transferreason);
        }

        //
        // POST: /TransferReason/Edit/5

        [HttpPost]
        public ActionResult Edit(TransferReason transferreason)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transferreason).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(transferreason);
        }

        //
        // GET: /TransferReason/Delete/5
 
        public ActionResult Delete(int id)
        {
            TransferReason transferreason = db.TransferReasons.Find(id);
            return View(transferreason);
        }

        //
        // POST: /TransferReason/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            TransferReason transferreason = db.TransferReasons.Find(id);
            db.TransferReasons.Remove(transferreason);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}