﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.Models;

namespace AssetsInventory.Controllers
{ 
    public class DocumentTypeController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        //
        // GET: /DocumentType/

        public ViewResult Index()
        {
            return View(db.DocumentTypes.ToList());
        }

        //
        // GET: /DocumentType/Details/5

        public ViewResult Details(int id)
        {
            DocumentType documenttype = db.DocumentTypes.Find(id);
            return View(documenttype);
        }

        //
        // GET: /DocumentType/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /DocumentType/Create

        [HttpPost]
        public ActionResult Create(DocumentType documenttype)
        {
            if (ModelState.IsValid)
            {
                db.DocumentTypes.Add(documenttype);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(documenttype);
        }
        
        //
        // GET: /DocumentType/Edit/5
 
        public ActionResult Edit(int id)
        {
            DocumentType documenttype = db.DocumentTypes.Find(id);
            return View(documenttype);
        }

        //
        // POST: /DocumentType/Edit/5

        [HttpPost]
        public ActionResult Edit(DocumentType documenttype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(documenttype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(documenttype);
        }

        //
        // GET: /DocumentType/Delete/5
 
        public ActionResult Delete(int id)
        {
            DocumentType documenttype = db.DocumentTypes.Find(id);
            return View(documenttype);
        }

        //
        // POST: /DocumentType/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            DocumentType documenttype = db.DocumentTypes.Find(id);
            db.DocumentTypes.Remove(documenttype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}