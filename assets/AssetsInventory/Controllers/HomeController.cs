﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetsInventory.DTO;
using AssetsInventory.Lookups;
using AssetsInventory.Models;
using AssetsInventory.Utilities;
using AssetsInventory.ViewModels;
using Language = AssetsInventory.DTO.Language;


namespace AssetsInventory.Controllers
{
    //[SetCulture]
    //[Elmah.Contrib.Mvc.ElmahHandleError]
    public class HomeController : BaseAdminController
    {
        private AssetContext db = new AssetContext();

        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        [HttpPost]
        public ActionResult Index(string pagesize)
        {

            TempData["pagesize"] = pagesize;
            return RedirectToAction("Index");
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            base.OnActionExecuted(filterContext);
            if (filterContext.HttpContext.Request.HttpMethod == "GET" &&
                (filterContext.RouteData.Values["action"].ToString() == "Index"
                || filterContext.RouteData.Values["action"].ToString() == "MaterialTransferinTransit"))
            {
                ViewData["pagesize"] = TempData["pagesize"] ?? 15;
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult RelatedFields()
        {
            return View();
        }

        public ActionResult SetCulture(string id)
        {

            HttpCookie userCookie = Request.Cookies["Culture"];

            userCookie.Value = id;
            userCookie.Expires = DateTime.Now.AddYears(100);
            Response.SetCookie(userCookie);

            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult RenderCultureUserControl()
        {
          
                var language = Request.Cookies != null && Request.Cookies["Culture"] != null ? Request.Cookies["Culture"].Value : "en";

                var languagelist = new List<Language>
                                       {
                                           new Language {Code = "en", Name = "English"},
                                           new Language {Code = "fr", Name = "Francais"},
                                           new Language {Code = "es", Name = "Espanyol"},
                                           new Language {Code = "pt-Br", Name = "Portuguese"},
                                       };
                ViewBag.LanguageList = new SelectList(languagelist, "Code", "Name",language);
            
            return PartialView("CultureUserControl");
        }

        public ActionResult RenderLogOnPartial()
        {
            var user = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            var model = new LogonPartialPageViewModel()
                            {
                                HasNoUser = db.Users.Count() == 0,
                                Position = user.Position,
                                DisplayName = user != null ? string.Format("{0} {1}",user.FirstName,user.LastName) : string.Empty
                            };
            return PartialView("_LogOnPartial", model);
        }

        public ActionResult MaterialTransferinTransit()
        {

            var currentuser = db.Users.FirstOrDefault(x => x.UserName == this.User.Identity.Name);
        

            ViewBag.UserId = currentuser.UserId;

            var materialtransfers =
                db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType).Include(m=>m.Originator).ToList()
                .Where(x => x.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Transit)
                && x.ReceiverId == currentuser.UserId)
                .Union(db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType).Include(m=>m.Originator).ToList()
                .Where(x => x.MaterialTransferApprovals.Any(y=>y.ApprovingUser.UserId == currentuser.UserId
                && string.IsNullOrWhiteSpace(y.ApprovalStatus)) && x.Status == Enum.GetName(typeof(TransferStatusEnum),TransferStatusEnum.TransferRequested)))
                .ToList();
            var query = from mt in materialtransfers
                        join fromcountry in db.Countries on mt.FromCountryId equals fromcountry.CountryId
                        join tocountry in db.Countries on mt.ToCountryId equals tocountry.CountryId
                        //join users in db.Users on mt.OriginatorId equals users.UserId
                        select new MaterialTransferDTO
                        {
                            MaterialTransferId = mt.MaterialTransferId,
                            MaterialTransferNumber = mt.MaterialTransferNumber,
                            TransferTypeName = mt.TransferType.TransferTypeName,
                            FromCountry = fromcountry.CountryName,
                            ToCountry = tocountry.CountryName,
                            DateAdded = mt.DateAdded,
                            Status = mt.Status == Enum.GetName(typeof(TransferStatusEnum),TransferStatusEnum.Transit) ? "In Transit" : mt.Status,
                            //DateApproved = mt.ApprovalDate,
                            //Initiator = users.FirstName + " " + users.LastName
                            //Initiator = mt.Originator.UserName
                        };

            return PartialView("MaterialTransferTransitIndex", query);
        }

        public JsonResult MaterialTransferActionList()
        {
            var currentuser = db.Users.FirstOrDefault(x => x.UserName == this.User.Identity.Name);

            ViewBag.UserId = currentuser.UserId;

            var materialtransfers =
                db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType).Include(m => m.Originator).ToList()
                .Where(x => x.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Transit)
                && x.ReceiverId == currentuser.UserId)
                .Union(db.MaterialTransfers.Include(m => m.TransferReason).Include(m => m.TransferType).Include(m => m.Originator).ToList()
                .Where(x => x.MaterialTransferApprovals.Any(y => y.ApprovingUser.UserId == currentuser.UserId
                && string.IsNullOrWhiteSpace(y.ApprovalStatus)) && x.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.TransferRequested)))
                .ToList();
            var query = from mt in materialtransfers
                        join fromcountry in db.Countries on mt.FromCountryId equals fromcountry.CountryId
                        join tocountry in db.Countries on mt.ToCountryId equals tocountry.CountryId
                        //join approvals in db.MaterialTransferApprovals on mt.MaterialTransferId equals approvals.MaterialTransferId
                        //into materialapprovals from approvals in materialapprovals.DefaultIfEmpty()
                        //where approvals.ApprovingUserId == currentuser.UserId
                        //join users in db.Users on mt.OriginatorId equals users.UserId
                        select new MaterialTransferDTO
                        {
                            MaterialTransferId = mt.MaterialTransferId,
                            MaterialTransferNumber = mt.MaterialTransferNumber,
                            TransferTypeName = mt.TransferType.TransferTypeName,
                            FromCountry = fromcountry.CountryName,
                            ToCountry = tocountry.CountryName,
                            DateAdded = mt.DateAdded,
                            //Status = approvals != null ? approvals.ApprovalStatus : string.Empty
                            Status = mt.Status == Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Transit) ? "In Transit" : mt.Status,
                            //DateApproved = mt.ApprovalDate,
                            //Initiator = users.FirstName + " " + users.LastName
                            //Initiator = mt.Originator.UserName
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenderSystemValuesDisplay()
        {
            var basemodel = (BaseModel) ViewData.Model;
            var model = new SystemDisplayPageViewModel(basemodel);
            return PartialView("SystemValues", model);
        }

        public ViewResult UploadLogo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadLogo(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (attachments != null && attachments.Count() > 0)
            {
                var savepath = Server.MapPath("~/Content/logo/");
                if (!Directory.Exists(savepath))
                    Directory.CreateDirectory(savepath);

                

                foreach (var file in attachments)
                {
                    if (file.ContentLength > 0)
                    {
                        //var fileName = Path.GetFileName(file.FileName);
                        
                        var path = Path.Combine(savepath, "logo.jpg");
                        file.SaveAs(path);
                        
                    }
                }



                TempData["SuccessMsg"] = "Logo updated!";

            }
            return View();
        }


    }
}
