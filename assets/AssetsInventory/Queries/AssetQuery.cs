﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using AssetsInventory.Lookups;
using AssetsInventory.Models;

namespace AssetsInventory.Queries
{
    public class AssetQuery
    {
        private AssetContext db = new AssetContext();

        public IList<Asset> SearchAssets(string text,int id)
        {
            var locationids = db.Locations.Where(x => x.CountryId == id).Select(x => x.LocationId).ToArray();

            var transferssets = from p in db.Assets.ToList()
                                join q in db.MaterialTransferAssets on p.AssetId equals q.AssetId
                                join r in db.MaterialTransfers.ToList() on q.MaterialTransferId equals r.MaterialTransferId
                                where p.Status != Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.InPlace)
                                && p.Status != Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.Received)
                                || ((p.Status == Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.InPlace)
                                || p.Status == Enum.GetName(typeof(AssetStatusEnum), AssetStatusEnum.Received))
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Cancelled)
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Discrepancy)
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Received)
                                && r.Status != Enum.GetName(typeof(TransferStatusEnum), TransferStatusEnum.Disapproved))
                                select p;

            var data = db.Assets.Include(a => a.Category).Include(a => a.SubCategory)
               .Include(a => a.Manufacturer).Include(a => a.ModelType).Include(a => a.Location)
               .Include(a => a.ConditionEvaluation)              
               .ToList()
               .Where(p => 
                    p.ArrangementNumber.ToLower().Contains(text)
                    || p.AssetUniqueId.ToLower().Contains(text)
                    || p.Category.CategoryName.ToLower().Contains(text)
                    || p.ConditionEvaluation.ConditionEvalDescription.ToLower().Contains(text)
                    || p.Location.LocationName.ToLower().Contains(text)
                    || p.Manufacturer.ManufacturerName.ToLower().Contains(text)
                    || p.ModelType.ModelTypeName.ToLower().Contains(text)
                    || p.SerialNumber.ToLower().Contains(text)
                    || p.Status.ToLower().Contains(text)
             //       || p.Description.ToLower().Contains(text)
             //       || p.Comment.ToLower().Contains(text)
                    || p.SubCategory.SubCategoryName.Contains(text))
               ;

            var query = from p in data
                           where locationids.Contains(p.LocationID ?? 0)
                           && !transferssets.Contains(p)
                           select p;

            return query.ToList();
        }
    }
}