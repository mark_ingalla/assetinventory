﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using AssetsInventory.Lookups;
using AssetsInventory.Models;

namespace AssetsInventory.Utilities
{
    public class AssetUser : IPrincipal
    {

        private AssetContext db = new AssetContext();
        public string UserName { get; set; }

        public AssetUser(string name)
        {
            UserName = name;
            
        }

        #region Implementation of IPrincipal

        public bool IsInRole(string role)
        {
            var usertype = (UserTypeEnum) Enum.Parse(typeof (UserTypeEnum), role);
            
            var user = db.Users.FirstOrDefault(x => x.UserName == UserName && x.UserTypeId == (int)usertype);

            return user != null;

        }

        public IIdentity Identity
        {
            get
            {
                var identity = new AssetIdentity(UserName);
                
                return identity;
            }
        }

        #endregion
    }

    public class AssetIdentity : IIdentity
    {
        private AssetContext db = new AssetContext();
        
        public AssetIdentity(string username)
        {
            Name = username;
            var user = db.Users.FirstOrDefault(x => x.UserName == username);
            IsAuthenticated = user != null;
            AuthenticationType = "custom";

        }

        public string Name { get; private set; }

        public string AuthenticationType { get; private set; }

        public bool IsAuthenticated { get; private set; }
    }

   
}