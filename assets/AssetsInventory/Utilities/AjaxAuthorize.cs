﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssetsInventory.Utilities
{
    public class AjaxAuthorizeAttribute : AuthorizeAttribute
    {
        override public void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            // Only do something if we are about to give a HttpUnauthorizedResult and we are in AJAX mode. 
            if (filterContext.Result is  HttpUnauthorizedResult && filterContext.HttpContext.Request.IsAjaxRequest())
            {
                // TODO: fix the URL building: 
                // 1- Use some class to build URLs just in case LoginUrl actually has some query already. 
                HttpRequestBase request = filterContext.HttpContext.Request;
                string returnUrl = request.Path;
                bool queryStringPresent = request.QueryString.Count > 0;
                if (queryStringPresent || request.Form.Count > 0)
                    returnUrl += '?' + request.QueryString.ToString();
                if (queryStringPresent)
                    returnUrl += '&';
                returnUrl += request.Form;
                //String url = System.Web.Security.FormsAuthentication.LoginUrl +
                //             "?X-Requested-With=XMLHttpRequest&ReturnUrl=" +
                //             HttpUtility.UrlEncode(returnUrl);
                String url = returnUrl;
                filterContext.Result = new RedirectResult(url);
            }
        }
    }
}