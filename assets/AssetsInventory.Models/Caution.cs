﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Caution : BaseModel
    {
        public int CautionId { get; set; }
        [Required][StringLength(450)]
        public string CautionName { get; set; }

        public virtual ICollection<MaterialTransfer> MaterialTransfers { get; set; }
    }
}