﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Category : BaseModel
    {

        public int CategoryId { get; set; }
        [Required]
        public string CategoryName { get; set; }
        [Required]
        [StringLength(450)]
        public string Prefix { get; set; }

        //public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<SubCategory> SubCategories { get; set; }
    }
}