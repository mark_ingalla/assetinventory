﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class MaterialTransferAsset : BaseModel
    {
        public int MaterialTransferAssetId { get; set; }
        public int MaterialTransferId { get; set; }
        public int? AssetId { get; set; }
        public string NonAssetDescription { get; set; }
        public int NonAssetQuantity { get; set; }

        public bool IsShipped { get; set; }
        public int ETADays { get; set; }
        public int? ConditionEvaluationId { get; set; }

        public virtual ConditionEvaluation ConditionEvaluation { get; set; }
        public virtual MaterialTransfer MaterialTransfer { get; set; }
        public virtual Asset Asset { get; set; }

    }
}