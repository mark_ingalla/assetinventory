﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class ChangeRequest
    {
        public int Id { get; set; }
        public int ChangeType { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int AdminId { get; set; }
        public DateTime? DateRequested { get; set; }
        public int RequestorId { get; set; }
        public string Module { get; set; }
        public string Status { get; set; }
        public string Link { get; set; }

        public virtual ICollection<ChangeRequestAttachment> Attachments { get; set; }
        public virtual User Admin { get; set; }
        public virtual User Requestor { get; set; }


    }

    public class ChangeRequestAttachment
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }



    public enum ChangeTypeEnum
    {
        New,
        Change
    }

  
}