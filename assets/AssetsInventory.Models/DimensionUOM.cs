﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class DimensionUOM : BaseModel
    {
        public int DimensionUOMId { get; set; }
        [Required]
        public string DimesionDescription { get; set; }
    }
}