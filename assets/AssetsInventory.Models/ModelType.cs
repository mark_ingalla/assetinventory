﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class ModelType : BaseModel
    {
        public int ModelTypeId { get; set; }
        [Required]
        public string ModelTypeName { get; set; }
        [Required]
        public string ModelTypeDescription { get; set; }
        [Required]
        public int? ManufacturerId { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }
        
        
    }
}