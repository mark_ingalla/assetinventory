﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class ConditionEvaluation : BaseModel
    {
        public int ConditionEvaluationId { get; set; }
        [Required]
        [StringLength(450)]
        public string ConditionEvalDescription { get; set; }
    }
}