﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Asset : BaseModel
    {
        public int AssetId { get; set; }
        public string AssetUniqueId { get; set; }
        public string Description { get; set; }
        [Required]
        public int? CategoryId { get; set; }
        [Required]
        public int? SubCategoryId { get; set; }
        [Required]
        public int? ManufacturerId { get; set; }
        [Required]
        public int? ModelTypeID { get; set; }

        public string SerialNumber { get; set; }
        public string ArrangementNumber { get; set; }
        [Required]
        public int? LocationID { get; set; }
        public int? PurchaseInfoID { get; set; }
        public int MaintenanceRequestID { get; set; }
        public int? ShippingInfoID { get; set; }
        public string MaterialTransferNumber { get; set; }
        [Required]
        public int? ConditionEvaluationID { get; set; }
        //public int? PhotoArchiveID { get; set; }
        //public string Photo { get; set; }
        public decimal? AssetPurchasePrice { get; set; }
        public decimal? NetbookValue { get; set; }
        public decimal? FairMarketPrice { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public bool IsPart { get; set; }


        public virtual Category Category { get; set; }
        public virtual SubCategory SubCategory { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual ModelType ModelType { get; set; }
        public virtual Location Location { get; set; }
        public virtual PurchaseInfo PurchaseInfo { get; set; }
        public virtual ShippingInfo ShippingInfo { get; set; }
        public virtual ConditionEvaluation ConditionEvaluation { get; set; }

        public virtual ICollection<Document> Documents { get; set; }
        public virtual ICollection<PhotoArchive> PhotoArchives { get; set; }
        public virtual ICollection<MaterialTransferAsset> TransferAssets { get; set; }
        //public virtual ICollection<PurchaseInfo> PurchaseInfos { get; set; }
        //public virtual ICollection<ShippingInfo> ShippingInfos { get; set; }

        
    }
}