﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class CountryManager
    {
        public int CountryManagerId { get; set; }
        public int MaterialTransferId { get; set; }
        public int UserId { get; set; }

        public virtual MaterialTransfer MaterialTransfer { get; set; }
        public virtual User User { get; set; }
    }
}