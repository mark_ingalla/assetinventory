﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class TransferType : BaseModel
    {
        public int TransferTypeId { get; set; }
        [Required]
        [StringLength(450)]
        public string TransferTypeName { get; set; }
    }
}