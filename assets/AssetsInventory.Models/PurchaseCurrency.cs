﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class PurchaseCurrency : BaseModel
    {
        public int PurchaseCurrencyID { get; set; }
        [Required]
        public string PurhcaseCurrencySymbol { get; set; }
    }
}