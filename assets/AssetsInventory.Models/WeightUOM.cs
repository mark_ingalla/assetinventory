﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class WeightUOM : BaseModel
    {
        public int WeightUOMId { get; set; }
        [Required]
        public string WeightUOMDescription { get; set; }
    }
}