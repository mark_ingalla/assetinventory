﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class ShippingInfo : BaseModel
    {
        public int ShippingInfoId { get; set; }
        public double? OperatingWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public double? DryWeight { get; set; }
        public int? DryWeightUOMId { get; set; }
        public double? Height { get; set; }
        public int? HeightUOMId { get; set; }
        public double? Width { get; set; }
        public int? WidthUOMId { get; set; }
        public double? Diameter { get; set; }
        public int? DiameterUOMId { get; set; }


        public virtual WeightUOM OperatingWeightUOM { get; set; }
        public virtual WeightUOM DryWeightUOM { get; set; }

        public virtual DimensionUOM HeightUOM { get; set; }
        public virtual DimensionUOM WidthUOM { get; set; }
        public virtual DimensionUOM DiameterUOM { get; set; }

    }
}