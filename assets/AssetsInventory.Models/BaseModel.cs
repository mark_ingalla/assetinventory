﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{

    public interface IBaseModel
    {
         DateTime? DateModified { get; set; }
         DateTime? DateAdded { get; set; }
         int? AddedById { get; set; }
         int? ModifiedById { get; set; }
    }
    public class BaseModel : IBaseModel
    {
        public virtual DateTime? DateModified { get; set; }
        public virtual DateTime? DateAdded { get; set; }
        public virtual int? AddedById { get; set; }
        public virtual int? ModifiedById { get; set; }
    }
}