﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Location : BaseModel
    {
        public int LocationId { get; set; }
        [Required]
        public string LocationName { get; set; }
        [Required]
        public int? CountryId { get; set; }
        [Required]
        [StringLength(450)]
        public string CostCenterCode { get; set; }

        public virtual Country Country { get; set; }
    }
}