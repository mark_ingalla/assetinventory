﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Language : BaseModel
    {
        public int LanguageId { get; set; }
        [Required]
        public string LanguageName { get; set; }
        [Required]
        public string LanguageShortName { get; set; }
    }
}