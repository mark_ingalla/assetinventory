﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class SubCategory : BaseModel
    {

        public int SubCategoryId { get; set; }
        [Required]
        public int? CategoryId { get; set; }
        [Required]
        public string SubCategoryName { get; set; }
        public string MarkinginInstructions { get; set; }
        public string CertOrDocRequirement { get; set; }
        public string DescriptionTemplate { get; set; }
        public virtual Category Category { get; set; }


        //public virtual ICollection<Asset> Assets { get; set; }
    }
}