﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AssetsInventory.Models
{
    public class User : BaseModel
    {
        public int UserId { get; set; }
        [Required]
        [StringLength(450)]
        public string UserName { get; set; }
        //[RegularExpression("^[0-9a-zA-Z]+$")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [StringLength(450)]
        [Email]
        public string Email { get; set; }
        [Required]
        public int UserTypeId { get; set; }
        [Required]
        public int? LanguageId { get; set; }
        [Required]
        public string Position { get; set; }

        public virtual Language Language { get; set; }
        public virtual ICollection<MaterialTransfer> Transfers { get; set; }
        public virtual ICollection<ApprovingUser> ApprovingUsers { get; set; }
        public virtual ICollection<MaterialTransfer> CountryManagerTransfers { get; set; }
        public virtual ICollection<CountryManager> CountryManagers { get; set; }

    }
}