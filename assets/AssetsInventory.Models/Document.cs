﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Document : BaseModel
    {
        public int DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentDescription { get; set; }
        public int? DocumentTypeId { get; set; }
        public int AssetId { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual DocumentType DocumentType { get; set; }

       
    }
}