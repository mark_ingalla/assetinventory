﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class DocumentType : BaseModel
    {
        public int DocumentTypeId { get; set; }
        [Required]
        [StringLength(450)]
        public string DocumentTypeName { get; set; }
    }
}