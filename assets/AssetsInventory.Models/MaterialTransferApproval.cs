﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class MaterialTransferApproval : BaseModel
    {
        public int MaterialTransferApprovalId { get; set; }
        public int MaterialTransferId { get; set; }
        public int ApprovingUserId { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime? ApprovalDateTime { get; set; }

        public virtual MaterialTransfer MaterialTransfer { get; set; }
        public virtual ApprovingUser ApprovingUser { get; set; }
    }
}