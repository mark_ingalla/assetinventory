﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class MaterialTransfer : BaseModel
    {
        public int MaterialTransferId { get; set; }
        [Required]
        public int TransferTypeId { get; set; }
        [Required]
        public int TransferReasonId { get; set; }
       // public int? CautionId { get; set; }
         [StringLength(450)]
        public string MaterialTransferNumber { get; set; }
        public string ManifestNumber { get; set; }
        [Required]
        public int FromCountryId { get; set; }
        [Required]
        public int? CurrentLocationId { get; set; }
        public string AFENumber { get; set; }
        public string Transporter { get; set; }
        [Required]
        public int ToCountryId { get; set; }
        [Required]
        public int? NewLocationId { get; set; }
        public string RepairWorkOrder { get; set; }
        public string TruckNumber { get; set; }
        public string AttnReceiver { get; set; }
        public int ReceiverId { get; set; }
        public string ReceiverRef { get; set; }
        public string ShipToAddress { get; set; }
        public string TransferComment { get; set; }
        public string Status { get; set; }

        public int OriginatorId { get; set; }
        
        public string ManifestoInfo { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? ShippingDate { get; set; }

        public TransferReason TransferReason { get; set; }
        public TransferType TransferType { get; set; }
        public virtual Country FromCountry { get; set; }
        public virtual Country ToCountry { get; set; }
       // public Caution Caution { get; set; }
        public virtual ICollection<Caution> Cautions { get; set; }
        

        public virtual User Receiver { get; set; }
        public virtual User Originator { get; set; }
        
        public virtual Location CurrentLocation { get; set; }
        public virtual Location NewLocation { get; set; }

        
        //public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<MaterialTransferAsset> TransferAssets { get; set; }
        public virtual ICollection<MaterialTransferApproval> MaterialTransferApprovals { get; set; }

        public virtual ICollection<CountryManager> CountryManagers { get; set; }
        


    }
}