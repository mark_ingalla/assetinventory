﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Manufacturer : BaseModel
    {
        public int ManufacturerId { get; set; }
        [Required]
        public string ManufacturerName { get; set; }
        [Required][StringLength(3,MinimumLength = 3)]
        public string ManufacturerCode { get; set; }
        public string OtherManufacturerRef { get; set; }

        public virtual ICollection<ModelType> ModelTypes { get; set; }
    }
}