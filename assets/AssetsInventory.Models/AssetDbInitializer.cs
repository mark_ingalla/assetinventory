﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace AssetsInventory.Models
{
    //DO NOT SWITCH/UNCOMMENT unless you are building up your database. WARNING: This will delete all your data
    //Switch strategy if you dont have the schema yet
    //public class AssetDbInitializer : DropCreateDatabaseAlways<AssetContext>
    public class AssetDbInitializer : CreateDatabaseIfNotExists<AssetContext>   
    {
  
        

        protected override void Seed(AssetContext context)
        {
            context.Database.ExecuteSqlCommand("ALTER TABLE Locations ADD CONSTRAINT uc_LocationCostCenterCode UNIQUE(CostCenterCode)");
            context.Database.ExecuteSqlCommand("ALTER TABLE MaterialTransfers ADD CONSTRAINT uc_MaterialTransferNumber UNIQUE(MaterialTransferNumber)");
            context.Database.ExecuteSqlCommand("ALTER TABLE Users ADD CONSTRAINT uc_UserUserName UNIQUE(UserName)");
            context.Database.ExecuteSqlCommand("ALTER TABLE Users ADD CONSTRAINT uc_UserEmail UNIQUE(Email)");

            context.Database.ExecuteSqlCommand("ALTER TABLE Cautions ADD CONSTRAINT uc_CautionName UNIQUE(CautionName)");
            context.Database.ExecuteSqlCommand("ALTER TABLE TransferReasons ADD CONSTRAINT uc_TransferReasonName UNIQUE(TransferReasonName)");
            context.Database.ExecuteSqlCommand("ALTER TABLE TransferTypes ADD CONSTRAINT uc_TransferTypeName UNIQUE(TransferTypeName)");
            context.Database.ExecuteSqlCommand("ALTER TABLE DocumentTypes ADD CONSTRAINT uc_DocumentTypeName UNIQUE(DocumentTypeName)");
            context.Database.ExecuteSqlCommand("ALTER TABLE ConditionEvaluations ADD CONSTRAINT uc_ConditionEvalDescription UNIQUE(ConditionEvalDescription)");
            context.Database.ExecuteSqlCommand("ALTER TABLE Categories ADD CONSTRAINT uc_CategoryPrefix UNIQUE(Prefix)");
            context.Database.ExecuteSqlCommand("ALTER TABLE Manufacturers ADD CONSTRAINT uc_ManufacturerCode UNIQUE(ManufacturerCode)");

        }
    }
}