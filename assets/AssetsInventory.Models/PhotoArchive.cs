﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class PhotoArchive : BaseModel
    {
        public int PhotoArchiveId { get; set; }
        public DateTime DateUploaded { get; set; }
        public string PhotoFileName { get; set; }

        public int AssetId { get; set; }

        public virtual Asset Asset { get; set; }
    }
}