﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class Country : BaseModel
    {
        public int CountryId { get; set; }
        [Required]
        public string CountryName { get; set; }
        [Required]
        public string CountryCode { get; set; }
  
        public string ISOCode { get; set; }
        [Required]
        public int SortCode { get; set; }

        public bool IsShown { get; set; }
    }
}