﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class PurchaseInfo : BaseModel
    {
        public int PurchaseInfoId { get; set; }
        public string AFENumber { get; set; }
        public string MaterialReqNumber { get; set; }
        public string PONumber { get; set; }
        public string Vendor { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal? PurchasePrice { get; set; }
        public int? PurchaseCurrencyId { get; set; }
        public DateTime? PurchaseDate { get; set; }


        public virtual PurchaseCurrency PurchaseCurrency { get; set; }


    }
}