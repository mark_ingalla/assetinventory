﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class ApprovingUser : BaseModel
    {
        public int ApprovingUserId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int TransferTypeId { get; set; }
        [Required]
        public int CountryId { get; set; }

        public virtual User User { get; set; }
        public virtual TransferType TransferType { get; set; }
        public virtual Country Country { get; set; }

        public virtual ICollection<TransferReason> TransferReasons { get; set; }
        public virtual ICollection<MaterialTransferApproval> MaterialTransferApprovals { get; set; }
    }
}