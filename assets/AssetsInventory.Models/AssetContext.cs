﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class AssetContext : DbContext
    {

     //   public DbSet<Asset> Assets { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<ModelType> ModelTypes { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<ConditionEvaluation> ConditionEvaluations { get; set; }
        public DbSet<PurchaseInfo> PurchaseInfos { get; set; }
       
        public DbSet<PhotoArchive> PhotoArchives { get; set; }
        public DbSet<PurchaseCurrency> Currencies { get; set; }
        public DbSet<WeightUOM> WeightUOMs { get; set; }
        public DbSet<DimensionUOM> DimensionUOMs { get; set; }
        public DbSet<ShippingInfo> ShippingInfos { get; set; }

        public DbSet<Asset> Assets { get; set; }
        public DbSet<Country> Countries { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<ApprovingUser> ApprovingUsers { get; set; }

        public DbSet<MaterialTransfer> MaterialTransfers { get; set; }
        public DbSet<MaterialTransferAsset> MaterialTransferAssets { get; set; }
        public DbSet<MaterialTransferApproval> MaterialTransferApprovals { get; set; }
        public DbSet<TransferReason> TransferReasons { get; set; }
        public DbSet<TransferType> TransferTypes { get; set; }
        public DbSet<Caution> Cautions { get; set; }

        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<ChangeRequest> ChangeRequests { get; set; }
        public DbSet<ChangeRequestAttachment> ChangeRequestAttachments { get; set; }

        public DbSet<NotificationUser> NotificationUsers { get; set; }


        public class Initializer:IDatabaseInitializer<AssetContext>
        {
            public void InitializeDatabase(AssetContext context)
            {
                if (!context.Database.Exists() || !context.Database.CompatibleWithModel(false))
                {
                    context.Database.Delete();
                    
                    context.Database.Create();

                    context.Database.ExecuteSqlCommand("ALTER TABLE Locations ADD CONSTRAINT uc_LocationCostCenterCode UNIQUE(CostCenterCode)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE MaterialTransfers ADD CONSTRAINT uc_MaterialTransferNumber UNIQUE(MaterialTransferNumber)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE Users ADD CONSTRAINT uc_UserUserName UNIQUE(UserName)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE Users ADD CONSTRAINT uc_UserEmail UNIQUE(Email)");

                    context.Database.ExecuteSqlCommand("ALTER TABLE Cautions ADD CONSTRAINT uc_CautionName UNIQUE(CautionName)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE TransferReasons ADD CONSTRAINT uc_TransferReasonName UNIQUE(TransferReasonName)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE TransferTypes ADD CONSTRAINT uc_TransferTypeName UNIQUE(TransferTypeName)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE DocumentTypes ADD CONSTRAINT uc_DocumentTypeName UNIQUE(DocumentTypeName)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE ConditionEvaluations ADD CONSTRAINT uc_ConditionEvalDescription UNIQUE(ConditionEvalDescription)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE Categories ADD CONSTRAINT uc_CategoryPrefix UNIQUE(Prefix)");
                    context.Database.ExecuteSqlCommand("ALTER TABLE Manufacturers ADD CONSTRAINT uc_ManufacturerCode UNIQUE(ManufacturerCode)");
                    
                }
              
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //comment this part to generate schema
            modelBuilder.Conventions.Remove<IncludeMetadataConvention>();
            //end comment
            //modelBuilder.Entity<ModelType>()
            //   .HasRequired(x => x.Manufacturer)
            //   .WithMany()
            //   .HasForeignKey(x => x.ManufacturerId).WillCascadeOnDelete(false);

            modelBuilder.Entity<MaterialTransferApproval>()
                .HasRequired(x=>x.MaterialTransfer)
                .WithMany()
                .HasForeignKey(x=>x.MaterialTransferId).WillCascadeOnDelete(false);

            modelBuilder.Entity<MaterialTransferApproval>()
                .HasRequired(x => x.ApprovingUser)
                .WithMany()
                .HasForeignKey(x => x.ApprovingUserId).WillCascadeOnDelete(false);

            modelBuilder.Entity<ModelType>()
               .HasRequired(x => x.Manufacturer)
               .WithMany()
               .HasForeignKey(x => x.ManufacturerId).WillCascadeOnDelete(false);

            modelBuilder.Entity<SubCategory>()
               .HasRequired(x => x.Category)
               .WithMany()
               .HasForeignKey(x => x.CategoryId).WillCascadeOnDelete(false);

        }

        public override int SaveChanges()
        {
            if (HttpContext.Current.User != null)
            {
                var currentuser = Users.FirstOrDefault(x => x.UserName == HttpContext.Current.User.Identity.Name);
                foreach (var entry in ChangeTracker.Entries<IBaseModel>())
                {
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.Entity.DateAdded = DateTime.Now;
                            if (currentuser != null)
                                entry.Entity.AddedById = currentuser.UserId;
                            break;

                        case EntityState.Modified:
                           
                            
                            entry.Entity.DateModified = DateTime.Now;
                            if (currentuser != null)
                                entry.Entity.ModifiedById = currentuser.UserId;
                            break;

                        default:
                            break;

                    }
                }
            }
            return base.SaveChanges();
        }

        

        

        

        

    }
}