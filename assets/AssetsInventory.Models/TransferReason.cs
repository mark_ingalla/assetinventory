﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssetsInventory.Models
{
    public class TransferReason :BaseModel
    {
        public int TransferReasonId { get; set; }
        [Required]
        [StringLength(450)]
        public string TransferReasonName { get; set; }

        public virtual ICollection<ApprovingUser> ApprovingUsers { get; set; }
        public virtual ICollection<NotificationUser> NotificationUsers { get; set; }
    }
}