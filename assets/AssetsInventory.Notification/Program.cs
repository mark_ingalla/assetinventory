﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using AssetsInventory.Models;


//using AssetsInventory.Models;

namespace AssetsInventory.Notification
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Database.SetInitializer(new AssetDbInitializer());
            using (var context = new AssetContext())
            {

                context.Database.Initialize(true);
            }

            var service = new ETAService();
            service.Run();
        }


    }
}
