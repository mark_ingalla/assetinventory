﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssetsInventory.Notification
{
    public class AssetETADTO
    {

        public string Receiver { get; set; }
        public string Originator { get; set; }
        public DateTime? ShippingDate { get; set; }
        public DateTime? ETA { get; set; }
        public int ETADays { get; set; }
        public string ItemNumber { get; set; }

    }
}
