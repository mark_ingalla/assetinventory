﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace AssetsInventory.Notification
{
    public class PastETAEmailService
    {
        public const string NotifierEmail = "no-reply@assetinventory.com";
        public string RecepientEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }


        public void SendEmail()
        {
            var fromemail = new MailAddress(NotifierEmail, NotifierEmail);


            var newmail = new MailMessage(fromemail, new MailAddress(RecepientEmail))
            {
                Subject = "Item Past ETA",
                Body = Body,
                IsBodyHtml = true

            };


            var smtp = new SmtpClient()
            {
                // Credentials = new NetworkCredential("marxmvc@gmail.com","passwordon"),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            try
            {
                smtp.Send(newmail);
            }
            catch (Exception)
            {

                throw;
            }


        }
    }
}
