﻿using System;
using System.Text;
using AssetsInventory.Models;
using System.Linq;

namespace AssetsInventory.Notification
{
    public class ETAService
    {
        private AssetContext db = new AssetContext();

        public void Run()
        {
            var body = new StringBuilder();
            body.Append("<p>Hello [Username],</p>");
            body.Append("<p><strong>Item Details</strong></p>");
            body.Append("<p><strong>Item #:</strong> [ItemNumber]<br />");
            body.Append("<strong>Shipping Date :</strong> [ShippingDate]<br /></p>");
            body.Append("<strong>ETA :</strong> [ETA]<br /></p>");
            body.Append("<p>This item is past its ETA</p>");

            

            var rundate = DateTime.Now;
            var data = from mt in db.MaterialTransfers.ToList()
                       join mtasset in db.MaterialTransferAssets on mt.MaterialTransferId equals
                           mtasset.MaterialTransferId
                        join asset in db.Assets on mtasset.AssetId equals asset.AssetId
                       join originator in db.Users on mt.OriginatorId equals originator.UserId
                       join receiver in db.Users on mt.ReceiverId equals receiver.UserId
                       where mt.Status == "Transit" && mt.ShippingDate.HasValue
                       select new AssetETADTO
                                  {
                                      ItemNumber =asset.AssetUniqueId,
                                      ETADays = mtasset.ETADays,
                                      ETA = mt.ShippingDate.Value.AddDays(mtasset.ETADays),
                                      Originator = originator.Email,
                                      Receiver = receiver.Email,
                                      ShippingDate =  mt.ShippingDate.Value 
                                  };
            foreach (var assetEtadto in data)
            {
                if (rundate.Subtract(assetEtadto.ETA.Value).Days > 1)
                {
                    // eta 1 day past rundate
                    body.Replace("[ItemNumber]", assetEtadto.ItemNumber);
                    body.Replace("[ETA]", assetEtadto.ETA.Value.ToString());
                    body.Replace("[ShippingDate]", assetEtadto.ShippingDate.Value.ToString());


                    var originatorbody = body.Replace("[Username]", assetEtadto.Originator);
                    var originatoremailservice = new PastETAEmailService
                                           {
                                               RecepientEmail = assetEtadto.Originator,
                                               Body = originatorbody.ToString()
                                           };
                    originatoremailservice.SendEmail();
                    var receiverbody = body.Replace("[Username]", assetEtadto.Receiver);
                    var receiveremailservice = new PastETAEmailService
                    {
                        RecepientEmail = assetEtadto.Receiver,
                        Body = receiverbody.ToString()
                    };
                    receiveremailservice.SendEmail();
                }
            }
        }

    }
}
